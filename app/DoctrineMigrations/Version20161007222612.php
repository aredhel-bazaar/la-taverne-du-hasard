<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161007222612 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE game (id INT AUTO_INCREMENT NOT NULL, fk_user_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, type VARCHAR(5) NOT NULL, image_cover VARCHAR(250) NOT NULL, description LONGTEXT NOT NULL, min_players INT NOT NULL, max_players INT NOT NULL, duration TIME NOT NULL, website VARCHAR(250) NOT NULL, status INT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_232B318C5741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE subscriber (id INT AUTO_INCREMENT NOT NULL, fk_event_id INT DEFAULT NULL, fk_user_id INT DEFAULT NULL, date_creation DATETIME NOT NULL, INDEX IDX_AD005B6943DFAB55 (fk_event_id), INDEX IDX_AD005B695741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE game ADD CONSTRAINT FK_232B318C5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE subscriber ADD CONSTRAINT FK_AD005B6943DFAB55 FOREIGN KEY (fk_event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE subscriber ADD CONSTRAINT FK_AD005B695741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE game');
        $this->addSql('DROP TABLE subscriber');
    }
}
