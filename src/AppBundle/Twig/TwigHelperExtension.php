<?php

namespace AppBundle\Twig;

use AppBundle\Service\Slugger;

class TwigHelperExtension extends \Twig_Extension
{
    /**
     * @var
     */
    private $locale;
    /**
     * @var \IntlDateFormatter
     */
    private $dateFormatter;
    /**
     * @var Slugger
     */
    private $slugger;
    
    
    
    public function __construct($locale, Slugger $slugger)
    {
        $this->locale        = $locale;
        $this->dateFormatter = \IntlDateFormatter::create(
            $this->locale,
            \IntlDateFormatter::LONG,
            \IntlDateFormatter::LONG
        );
        $this->slugger = $slugger;
    }
    
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('aging',     array($this, 'getAging')),
            new \Twig_SimpleFilter('strftime',  array($this, 'strftime')),
            new \Twig_SimpleFilter('intval',    array($this, 'intval')),
            new \Twig_SimpleFilter('slug',    array($this, 'slugify'))
        );
    }
    
    
    
    /**
     * @param \DateTime      $dateTimeOld
     * @param \DateTime|null $dateTimeNew
     *
     * @return string
     */
    public function getAging(\DateTime $dateTimeOld, \DateTime $dateTimeNew=null)
    {
        if( is_null($dateTimeNew) ) {
            $dateTimeNew = new \DateTime();
        }
    
        $DateInterval = $dateTimeOld->diff($dateTimeNew, true);
    
        $str = [];
        if( $DateInterval->y != 0 ) {
            if( !isset($show) ) {
                $show = [
                    'y' => TRUE,
                    'm' => FALSE,
                    'd' => FALSE,
                    'h' => FALSE,
                    'i' => FALSE,
                    's' => FALSE
                ];
            }
        
            $str[] = $DateInterval->y." an".($DateInterval->y > 1 ? "s":"");
        }
    
        if( $DateInterval->m != 0 ) {
            if( !isset($show) ) {
                $show = [
                    'y' => FALSE,
                    'm' => TRUE,
                    'd' => FALSE,
                    'h' => FALSE,
                    'i' => FALSE,
                    's' => FALSE
                ];
            }
        
            if( $show['m'] ) {
                $str[] = $DateInterval->m." mois";
            }
        }
    
        if( $DateInterval->d != 0 ) {
            if( !isset($show) ) {
                $show = [
                    'y' => FALSE,
                    'm' => FALSE,
                    'd' => TRUE,
                    'h' => FALSE,
                    'i' => FALSE,
                    's' => FALSE
                ];
            }
        
            if( $show['d'] ) {
                $str[] = $DateInterval->d."j";
            }
        }
    
        if( $DateInterval->h != 0 ) {
            if( !isset($show) ) {
                $show = [
                    'y' => FALSE,
                    'm' => FALSE,
                    'd' => FALSE,
                    'h' => TRUE,
                    'i' => FALSE,
                    's' => FALSE
                ];
            }
        
            if( $show['h'] ) {
                $str[] = $DateInterval->h."hr".($DateInterval->h > 1 ? "s":"");
            }
        }
    
        if( $DateInterval->i != 0 ) {
            if( !isset($show) ) {
                $show = [
                    'y' => FALSE,
                    'm' => FALSE,
                    'd' => FALSE,
                    'h' => FALSE,
                    'i' => TRUE,
                    's' => FALSE
                ];
            }
        
            if( $show['i'] ) {
                $str[] = $DateInterval->i."min";
            }
        }
    
        if( $DateInterval->s != 0 ) {
            if( !isset($show) ) {
                $show = [
                    'y' => FALSE,
                    'm' => FALSE,
                    'd' => FALSE,
                    'h' => FALSE,
                    'i' => FALSE,
                    's' => TRUE
                ];
            }
        
            if( $show['s'] ) {
                $str[] = $DateInterval->s."s";
            }
        }
    
        return implode(' ', $str);
    }
    
    
    
    /**
     * @param \DateTime $dateTime
     * @param string    $format
     *
     * @return string
     */
    public function strftime(\DateTime $dateTime, string $format) {
        return $this->dateFormatter->formatObject($dateTime, $format);
    }
    
    
    
    /**
     * @param string $value
     *
     * @return int
     */
    public function intval(string $value) {
        return intval($value);
    }
    
    
    
    /**
     * @param string $value
     *
     * @return string
     */
    public function slugify(string $value) {
        return $this->slugger->slugify($value);
    }
    
    
    
    /**
     * @return string
     */
    public function getName()
    {
        return 'twig_helper_extension';
    }
}
