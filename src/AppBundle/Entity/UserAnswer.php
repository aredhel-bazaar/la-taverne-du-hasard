<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserAnswerRepository")
 * @ORM\Table(name="lk_user_answer", indexes={})
 * @ORM\Cache()
 * @ORM\HasLifecycleCallbacks()
 */
class UserAnswer
{
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     * @ORM\Id
     */
    private $fkUser;
    
    /**
     * @var Answer
     *
     * @ORM\ManyToOne(targetEntity="Answer")
     * @ORM\JoinColumn(name="fk_answer_id", referencedColumnName="id")
     * @ORM\Id
     */
    private $fkAnswer;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime", nullable=true)
     */
    private $modificationDate;
    
    
    
    /**
     * @return User
     */
    public function getFkUser(): User
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param User $fkUser
     *
     * @return UserAnswer
     */
    public function setFkUser(User $fkUser): UserAnswer
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return Answer
     */
    public function getFkAnswer(): Answer
    {
        return $this->fkAnswer;
    }
    
    
    
    /**
     * @param Answer $fkAnswer
     *
     * @return UserAnswer
     */
    public function setFkAnswer(Answer $fkAnswer): UserAnswer
    {
        $this->fkAnswer = $fkAnswer;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return UserAnswer
     */
    public function setCreationDate(\DateTime $creationDate): UserAnswer
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return UserAnswer
     */
    public function setModificationDate(\DateTime $modificationDate): UserAnswer
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->creationDate = new DateTime();
    }
    
    
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->modificationDate = new DateTime();
    }
}
