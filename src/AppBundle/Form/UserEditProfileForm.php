<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserEditProfileForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $options['data'];
        
        $builder
            ->add('publicName', TextType::class, [
                'label' => 'user.label.public_name',
                'required'  => true
            ])
            ->add('presentation', CKEditorType::class, [
                'label'         => 'user.label.presentation',
                'config_name'   => 'minimal_config',
                'required'      => false
            ])
            ->add('avatar', HiddenType::class, [
                'attr' => [
                    'class' => 'js-avatar-path'
                ],
                'required'  => false
            ])
            ->add('firstName', TextType::class, [
                'label'    => 'user.label.first_name',
                'disabled' => !empty($user->getFirstName()),
                'required'  => true
            ] + (empty($user->getFirstName()) ? [] :
                [
                    'attr'     => [
                        'icon' => (!empty($user->getFirstName()) ? 'lock' : '')
                    ]
                ])
            )
            ->add('lastName', TextType::class, [
                'label'    => 'user.label.last_name',
                'disabled' => !empty($user->getLastName()),
                'required'  => true
            ] + (empty($user->getLastName()) ? [] :
                [
                    'attr'     => [
                        'icon' => (!empty($user->getLastName()) ? 'lock' : '')
                    ]
                ])
            )
            ->add('email', TextType::class, [
                'label'         => 'user.label.email',
                'disabled'  => true,
                'attr'      => [
                    'icon' => 'lock'
                ],
                'required'  => true
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => User::class,
            'validation_groups'  => ['default']
        ));
    }
}
