<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AnswerRepository")
 * @ORM\Table(name="answer", indexes={})
 * @ORM\Cache()
 * @ORM\HasLifecycleCallbacks()
 */
class Answer
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Question
     *
     * @ORM\ManyToOne(targetEntity="Question", inversedBy="answers")
     * @ORM\JoinColumn(name="fk_question_id", referencedColumnName="id")
     *
     */
    private $fkQuestion;
    
    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=150)
     */
    private $label;
    
    /**
     * @var int
     *
     * @ORM\Column(name="recordOrder", type="integer", length=3)
     * @ORM\OrderBy({"order"="ASC"})
     */
    private $recordOrder;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Answer
     */
    public function setId(int $id): Answer
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return Question
     */
    public function getfkQuestion()
    {
        return $this->fkQuestion;
    }
    
    
    
    /**
     * @param Question $question
     *
     * @return Answer
     */
    public function setFkQuestion(Question $question): Answer
    {
        $this->fkQuestion = $question;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    
    
    /**
     * @param string $label
     *
     * @return Answer
     */
    public function setLabel(string $label): Answer
    {
        $this->label = $label;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getRecordOrder()
    {
        return $this->recordOrder;
    }
    
    
    
    /**
     * @param int $recordOrder
     *
     * @return Answer
     */
    public function setRecordOrder(int $recordOrder): Answer
    {
        $this->recordOrder = $recordOrder;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Answer
     */
    public function setCreationDate(\DateTime $creationDate): Answer
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Answer
     */
    public function setModificationDate(\DateTime $modificationDate): Answer
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->modificationDate = new \DateTime();
        $this->creationDate = new \DateTime();
    }
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->modificationDate = new \DateTime();
    }
}