(function() {
    "use strict";

    /**
     * Global variables
     */
    var dropzoneContainer,
        userIdx = 0;

    var avatar_base_path = "/media/upload/avatar/";
    dropzoneContainer = $('.js-dropzone');

    if( dropzoneContainer.length ) {
        dropzoneContainer.dropzone({
            url:                Routing.generate('user_upload_avatar'),
            maxFiles:           1,
            acceptedFiles:      'image/*',
            addRemoveLinks:     false,
            thumbnailWidth:     '200',
            thumbnailHeight:    '200',
            previewsContainer:  '#dropzone-template',
            init: function() {
                this.on('drop', function() {
                    this.removeAllFiles();
                });

                this.on('success', function(file, responseText) {
                    this.removeAllFiles();

                    $('#avatarDropzone img').attr('src', avatar_base_path + responseText.data.img_url );
                    $('.js-avatar-path').val(responseText.data.img_url);
                });

                this.on('error', function(file, errorMsg) {
                    this.removeAllFiles();
                });

                this.on('thumbnail', function(file, dataUrl) {
                    var img = dataUrl;
                });

                this.on("maxfilesexceeded", function(file) {
                    this.removeFile(file);
                });
            }
        });
    }

    if ($('form[name=user_contact_form]').length) {
        var protoElm  = $('#user_contact_form_users');
        var prototype = protoElm.data('prototype');

        $('body')
            .on('keyup', '.js-search-user', function(e) {
                var $this = $(this);
                var value = $this.val();

                clearTimeout(userSearchTimeout);
                if (value.length > 2 && lastUserValue !== value) {
                    lastUserValue = value;
                    userSearchTimeout = setTimeout(function () {
                        $this.parent().toggleClass('loading');

                        $.ajax({
                            url: Routing.generate('app_utilisateur_finduser', {publicName: value}),
                            dataType: 'json'
                        })
                            .done(function(data) {
                                var userSearchElm = $('.js-search-user-list');
                                userSearchElm.empty();
                                $.each(data.users, function (idx, User) {
                                    var div = $('<div ' +
                                        'class="js-found-user ui secondary button found user">' +
                                        '<img class="avatar" alt="' + User.publicName + '" src="' + User.avatar + '" /><span>' + User.publicName + '</span>' +
                                        '</div>')
                                        .data('user-id', User.id)
                                        .data('user-publicName', User.publicName)
                                        .data('user-avatar', User.avatar)
                                    ;

                                    div.appendTo(userSearchElm);
                                });
                            })
                            .always(function() {
                                $this.parent().toggleClass('loading');
                            });
                    }, 300);
                }
            })

            .on('click', '.js-found-user', function (e) {
                var $this = $(this);

                lastUserValue = null;
                protoElm.append(prototype.replace(/__name__/g, userIdx));

                $('#user_contact_form_users_'+userIdx).val($this.data('user-id'));
                $this.attr('data-idx', userIdx).appendTo('.js-user-list');

                userIdx++;

                $('.js-search-user').val('');
                $('.js-search-user-list').empty();
            })
        ;

        $('.js-user-list')
            .on('click', '.js-found-user', function (e) {
                var $this = $(this);

                $('#user_contact_form_users_'+$this.attr('data-idx')).remove();
                $this.remove();
            })
    }
})();
