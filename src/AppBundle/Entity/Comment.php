<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 20:17
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CommentRepository")
 * @ORM\Table(name="comment", indexes={})
 * @ORM\Cache()
 */
class Comment
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;
    
    /**
     * @ORM\ManyToOne(targetEntity="Article", inversedBy="comments")
     * @ORM\JoinColumn(name="fk_article_id", referencedColumnName="id", nullable=true)
     */
    private $fkArticle;
    
    /**
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumn(name="fk_event_id", referencedColumnName="id", nullable=true)
     */
    private $fkEvent;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    
    
    
    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    
    
    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->content = $content;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkArticle()
    {
        return $this->fkArticle;
    }
    
    
    
    /**
     * @param mixed $fkArticle
     *
     * @return $this
     */
    public function setFkArticle($fkArticle)
    {
        $this->fkArticle = $fkArticle;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkEvent()
    {
        return $this->fkEvent;
    }
    
    
    
    /**
     * @param mixed $fkEvent
     *
     * @return $this
     */
    public function setFkEvent($fkEvent)
    {
        $this->fkEvent = $fkEvent;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return $this
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return $this
     */
    public function setModificationDate(\DateTime $modificationDate)
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
}