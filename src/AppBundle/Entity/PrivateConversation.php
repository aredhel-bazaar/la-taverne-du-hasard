<?php
/**
 * Created by PhpStorm.
 * User: Aredhel
 * Date: 18/06/2017
 * Time: 10:49
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrivateConversationRepository")
 * @ORM\Table(name="private_conversation", indexes={})
 * @ORM\Cache()
 */
class PrivateConversation
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $author;
    
    /**
     * @ORM\OneToMany(targetEntity="PrivateConversationUser", mappedBy="fkPrivateConversation")
     */
    private $privateConversationUsers;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;
    
    /**
     * @var PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="PrivateMessage", mappedBy="fkPrivateConversation")
     */
    private $messages;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * PrivateConversation constructor.
     */
    public function __construct()
    {
        $this->privateConversationUsers    = new ArrayCollection();
        $this->messages                    = new ArrayCollection();
    }
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return PrivateConversation
     */
    public function setId(int $id): PrivateConversation
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return User
     */
    public function getAuthor(): User
    {
        return $this->author;
    }
    
    
    
    /**
     * @param User $author
     *
     * @return PrivateConversation
     */
    public function setAuthor(User $author): PrivateConversation
    {
        $this->author = $author;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getUsers()
    {
        return new ArrayCollection();
    }
    
    
    
    /**
     * @return ArrayCollection[PrivateConversationUser]
     */
    public function getPrivateConversationUsers()
    {
        return $this->privateConversationUsers;
    }
    
    
    
    /**
     * @param PrivateConversationUser $privateConversationUser
     *
     * @return PrivateConversation
     */
    public function removePrivateConversationUser(PrivateConversationUser $privateConversationUser)
    {
        $this->privateConversationUsers->removeElement($privateConversationUser);
        
        return $this;
    }
    
    
    
    /**
     * @param PrivateConversationUser $privateConversationUser
     *
     * @return PrivateConversation
     */
    public function addPrivateConversationUser(PrivateConversationUser $privateConversationUser)
    {
        $this->privateConversationUsers->add($privateConversationUser);
        
        return $this;
    }
    
    
    
    /**
     * @param mixed $privateConversationUsers
     *
     * @return PrivateConversation
     */
    public function setPrivateConversationUsers($privateConversationUsers)
    {
        $this->privateConversationUsers = $privateConversationUsers;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }
    
    
    
    /**
     * @param string $title
     *
     * @return PrivateConversation
     */
    public function setTitle(string $title): PrivateConversation
    {
        $this->title = $title;
        
        return $this;
    }
    
    
    
    /**
     * @return PersistentCollection
     */
    public function getMessages(): PersistentCollection
    {
        return $this->messages;
    }
    
    
    
    /**
     * @param array $messages
     *
     * @return PrivateConversation
     */
    public function setMessages(array $messages): PrivateConversation
    {
        $this->messages = $messages;
        
        return $this;
    }
    
    
    
    /**
     * @param PrivateMessage $privateMessage
     *
     * @return PrivateConversation
     */
    public function removeMessage(PrivateMessage $privateMessage)
    {
        $this->messages->removeElement($privateMessage);
        
        return $this;
    }
    
    
    
    /**
     * @param PrivateMessage $privateMessage
     *
     * @return PrivateConversation
     */
    public function addMessage(PrivateMessage $privateMessage)
    {
        $this->messages->add($privateMessage);
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return PrivateConversation
     */
    public function setCreationDate(\DateTime $creationDate): PrivateConversation
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return PrivateConversation
     */
    public function setModificationDate(\DateTime $modificationDate): PrivateConversation
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @param User $user
     *
     * @return bool
     */
    public function isNew($user)
    {
        return ($this->modificationDate > $this->getUserLastRead($user));
    }
    
    
    
    /**
     * @param $user
     *
     * @return \DateTime
     * @throws \Exception
     */
    private function getUserLastRead($user)
    {
        /** @var PrivateConversationUser $privateConversationUser */
        foreach ($this->getPrivateConversationUsers() as $privateConversationUser) {
            if ($privateConversationUser->getFkUser() === $user) {
                return $privateConversationUser->getModificationDate();
            }
        }
        
        throw new \Exception("User not found");
    }
}