(function() {
    "use strict";

    /**
     * Global variables
     */
    var apc_openListFilters_container;
    var mainSection_container;
    var userSearchTimeout;
    var lastUserValue;

    /**
     * JQuery Extends
     */
    $.fn.extend({
        addSidePanel: function( content ) {
            var target    = $(this);
            var num_panel = target.data('panel');

            if( num_panel === undefined ) {
                num_panel = 1;
            }

            var div = $('<div/>', {
                class: 'hidden side-panel js-side-panel'
            }).data('panel', num_panel);

            div.append($('#js-newPanel-prototype').html());
            div.append(content);

            target.after(div);
            div.transition('slide right');
            target.transition('slide right');

            return div;
        },

        goPanelBack: function() {
            var target    = $(this).closest('.js-side-panel');
            var num_panel = target.data('panel');
            var div;

            if( target.length ) {
                target.transition('slide left');

                if( num_panel > 1 ) {
                    div = target.siblings('.js-side-panel[data-panel='+(num_panel-1)+']');
                } else {
                    div = $('.js-main-section');
                }

                div.transition('slide right');
                target.remove();
            }

            return div;
        },

        refresh: function() {
            var target = $(this);

            target.dimmer('show');

            $.ajax({
                url: target.data('href'),
                type: 'GET'
            })
                .done(function(response) {
                    target.html(response);
                })
                .always(function() {
                    target.dimmer('hide');
                })
            ;
        }
    });

    $.extend({
        initSemanticComponents: function () {
            $('.ui.sticky')
                .sticky({
                    offset: "10px",
                    bottomOffset: "10px"
                })
            ;

            $('.message .close')
                .on('click', function() {
                    $(this)
                        .closest('.message')
                        .slideUp(300)
                    ;
                })
            ;

            $('.tabular.menu .item')
                .tab()
            ;

            $('.js-popup').popup({
                position: 'top center'
            });

            $('.ui.radio.checkbox')
                .checkbox()
            ;

            $('.ui.progress')
                .progress()
            ;
        }
    });

    apc_openListFilters_container = $('.js-openEventList-filters');
    mainSection_container         = $('.js-main-section');

//-- Initialisation de Toastr
    toastr.options = {
        "closeButton": false,
        "debug": false,
        "newestOnTop": true,
        "progressBar": true,
        "positionClass": "toast-top-right",
        "preventDuplicates": false,
        "onclick": null,
        "showDuration": "300",
        "hideDuration": "1000",
        "timeOut": "3000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut"
    };
//--

//-- Initialisation des modules Semantic UI
    $.initSemanticComponents();
//--

    $('body').on('click', '.js-panel-back', function(e) {
        e.preventDefault();

        $(this).goPanelBack();
    });

    $('#main-wrapper')
        .on('click', '.js-toggle-comment-form', function() {
            var $this = $(this);
            var $form = $this.siblings('.hidden');

            $form.toggleClass('hidden');
            $this.toggleClass('hidden');

            CKEDITOR.instances.comment_form_content.focus();
        })

        .on('keyup', '#event_create_form_fkOrganizer_publicName', function(e) {
            var $this = $(this);
            var value = $this.val();

            clearTimeout(userSearchTimeout);
            if (value.length > 2 && lastUserValue !== value) {
                lastUserValue = value;
                userSearchTimeout = setTimeout(function () {
                    $this.parent().toggleClass('loading');

                    $.ajax({
                        url: Routing.generate('app_utilisateur_finduser', {publicName: value}),
                        dataType: 'json'
                    })
                        .done(function(data) {
                            var organizerListElement = $('.js-event-organizer-list');
                            organizerListElement.empty();
                            $.each(data.users, function (idx, User) {
                                var div = $('<div ' +
                                    'class="js-found-organizer ui secondary button found user">' +
                                    '<img class="avatar" alt="' + User.publicName + '" src="' + User.avatar + '" /><span>' + User.publicName + '</span>' +
                                    '</div>')
                                    .data('user-id', User.id)
                                    .data('user-publicName', User.publicName)
                                    .data('user-avatar', User.avatar)
                                ;

                                div.appendTo(organizerListElement);
                            });
                        })
                        .always(function() {
                            $this.parent().toggleClass('loading');
                        });
                }, 300);
            }
        })

        .on('click', '.js-found-organizer', function (e) {
            var $this = $(this);
            var publicNameElement = $('#event_create_form_fkOrganizer_publicName');

            $('#event_create_form_fkOrganizer_id').val($this.data('user-id'));
            publicNameElement.val($this.data('user-publicName'));
            publicNameElement.parent().find('i').addClass('hidden');
            publicNameElement.parent().prepend('<img class="input image avatar" src="' + $this.data('user-avatar') + '" alt="' + $this.data('user-publicName') + '"/>');
            $('.js-event-organizer-list').empty();
        })
    ;



    if( apc_openListFilters_container.length ) {
        var filter = new LiveFilter(apc_openListFilters_container.find('.js-filter .ui.button'), Routing.generate('app_admin_openevent'));

        filter.criterions['filter'] = [];
        apc_openListFilters_container.find('.js-filter .ui.button').each(function() {
            if( $(this).hasClass('active') ) {
                filter.criterions['filter'].push($(this).data('value'));
            }
        });

        apc_openListFilters_container
            .on('click', '.js-filter .ui.button', function(e) {
                e.preventDefault();

                $(this).toggleClass('active');

                if( $(this).hasClass('active') ) {
                    filter.addCriterion('filter', $(this).data('value'));
                } else {
                    filter.removeCriterion('filter', $(this).data('value'));
                }

            })
            .on('livefilter', '.js-filter .ui.button', function(event, data) {
                $('.admin-table-list tbody').html(data);
            });
    }

    $('.js-customize-openEvent').on('click', function(e) {
        e.preventDefault();
        e.stopPropagation();

        var target = $(this);
        target.toggleClass('loading');

        $.ajax({
            url: target.attr('href')
        })
            .done(function(reponse) {
                $('.js-main-section').addSidePanel(reponse);
            })
            .always(function() {
                target.toggleClass('loading');
            })
    });

    $('.js-openEvent-list').on('click', '.js-action-opentEvent .ui.button', function(e) {
        e.preventDefault();

        var target           = $(this);
        var data_value       = target.data('value');
        var status_container = target.parents('tr').find('.js-openEvent-status');

        target.toggleClass('loading');

        $.ajax({
            url: $(this).attr('href')
        })
            .done(function() {
                target.parent().find('.ui.button').removeClass('hidden');
                target.toggleClass('hidden');
                status_container.find('.icon').addClass('hidden');
                status_container.find('.icon[data-value='+data_value+']').removeClass('hidden');
            })
            .always(function() {
                target.toggleClass('loading');
            })
    });

    $('body').on('click', '.js-customize-openEvent-submit', function(e) {
        e.preventDefault();

        CKEDITOR.instances['event_create_form_content'].updateElement();

        var target      = $(this);
        var form        = $('.js-customize-OpenEvent-form form');
        var form_data   = form.serializeArray();

        $.ajax({
            url: form.attr('action') + '&action=' + target.data('action'),
            type: 'POST',
            data: form_data
        })
            .done(function() {
                form.goPanelBack();

                $('.js-openEvent-list tbody').refresh();
            })
        ;
    });

    $('.js-display-menu').on('click', function () {
        var $mainMenu = $('.js-main-menu');

        if ($mainMenu.first().css('display') === 'none') {
            $mainMenu.slideDown(300);
        } else {
            $mainMenu.slideUp(300);
        }
    });
})();
