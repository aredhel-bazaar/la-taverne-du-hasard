<?php
/**
 * Created by PhpStorm.
 * User: Aredhel
 * Date: 02/04/2017
 * Time: 19:37
 */

namespace AppBundle\Form;

use AppBundle\Entity\Accreditation;
use AppBundle\Entity\Topic;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class TopicCreateForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'topic.form.title.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('pinned', CheckboxType::class, [
                'label' => 'topic.form.pinned',
                'required' => false
            ])
            ->add('messages', CollectionType::class, [
                'entry_type' => MessageCreateForm::class,
                'entry_options' => [
                    'is_admin' => $options['is_admin']
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Topic::class,
            'is_admin'   => false,
        ]);
    }
}
