<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuestionRepository")
 * @ORM\Table(name="question", indexes={})
 * @ORM\Cache()
 * @ORM\HasLifecycleCallbacks()
 */
class Question
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Survey
     *
     * @ORM\ManyToOne(targetEntity="Survey", inversedBy="questions")
     * @ORM\JoinColumn(name="fk_survey_id", referencedColumnName="id")
     *
     */
    private $fkSurvey;
    
    /**
     * @var string
     *
     * @ORM\Column(name="label", type="string", length=150)
     */
    private $label;

    /**
     * @var int
     *
     * @ORM\Column(name="recordOrder", type="integer", length=3)
     * @ORM\OrderBy({"order"="ASC"})
     */
    private $recordOrder;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Answer", mappedBy="fkQuestion", cascade={"persist"})
     *
     */
    private $answers;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Question
     */
    public function setId(int $id): Question
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return Survey
     */
    public function getFkSurvey()
    {
        return $this->fkSurvey;
    }
    
    
    
    /**
     * @param Survey $survey
     *
     * @return Question
     */
    public function setFkSurvey(Survey $survey): Question
    {
        $this->fkSurvey = $survey;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getLabel()
    {
        return $this->label;
    }
    
    
    
    /**
     * @param string $label
     *
     * @return Question
     */
    public function setLabel(string $label): Question
    {
        $this->label = $label;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getRecordOrder()
    {
        return $this->recordOrder;
    }
    
    
    
    /**
     * @param int $recordOrder
     *
     * @return Question
     */
    public function setRecordOrder(int $recordOrder): Question
    {
        $this->recordOrder = $recordOrder;
        
        return $this;
    }
    
    
    
    /**
     * @return Collection
     */
    public function getAnswers(): Collection
    {
        return $this->answers;
    }
    
    
    
    /**
     * @param Collection $answers
     *
     * @return Question
     */
    public function setAnswers(Collection $answers): Question
    {
        foreach ($answers as $answer) {
            $this->addAnswer($answer);
        }
        
        return $this;
    }
    
    
    
    /**
     * @param Answer $answer
     *
     * @return Question
     */
    public function addAnswer(Answer $answer): Question
    {
        $answer->setFkQuestion($this)
            ->setRecordOrder($this->answers->count());
        
        $this->answers->add($answer);
        
        return $this;
    }
    
    
    
    /**
     * @param Answer $answer
     *
     * @return Question
     */
    public function removeAnswer(Answer $answer): Question
    {
        $this->answers->removeElement($answer);
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Question
     */
    public function setCreationDate(\DateTime $creationDate): Question
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Question
     */
    public function setModificationDate(\DateTime $modificationDate): Question
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $this->modificationDate = new \DateTime();
        $this->creationDate = new \DateTime();
    
        /** @var Answer $answer */
        foreach ($this->getAnswers() as &$answer) {
            $answer->setRecordOrder($this->answers->indexOf($answer))
                ->setFkQuestion($this)
            ;
        }
    }
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->modificationDate = new \DateTime();
    }
}