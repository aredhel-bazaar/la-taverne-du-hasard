<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 19:32
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ActivityLogRepository")
 * @ORM\Table(name="activity_log", indexes={})
 * @ORM\Cache()
 */
class ActivityLog
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", length=100)
     */
    private $ip;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var string
     *
     * @ORM\Column(name="browser_name", type="string", length=100)
     */
    private $browserName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="browser_version", type="string", length=20)
     */
    private $browserVersion;
    
    /**
     * @var string
     *
     * @ORM\Column(name="bot_name", type="string", length=100, nullable=true)
     */
    private $botName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="route", type="string", length=200)
     */
    private $route;
    
    /**
     * @var string
     *
     * @ORM\Column(name="status_code", type="integer", length=3)
     */
    private $statusCode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="is_mobile", type="boolean")
     */
    private $isMobile;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    
    
    
    /**
     * @return string
     */
    public function getIp(): string
    {
        return $this->ip;
    }
    
    
    
    /**
     * @param string $ip
     */
    public function setIp(string $ip)
    {
        $this->ip = $ip;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
    }
    
    
    
    /**
     * @return string
     */
    public function getBrowserName(): string
    {
        return $this->browserName;
    }
    
    
    
    /**
     * @param string $browserName
     */
    public function setBrowserName(string $browserName)
    {
        $this->browserName = $browserName;
    }
    
    
    
    /**
     * @return string
     */
    public function getBrowserVersion(): string
    {
        return $this->browserVersion;
    }
    
    
    
    /**
     * @param string $browserVersion
     */
    public function setBrowserVersion(string $browserVersion)
    {
        $this->browserVersion = $browserVersion;
    }
    
    
    
    /**
     * @return string
     */
    public function getBotName(): string
    {
        return $this->botName;
    }
    
    
    
    /**
     * @param string $botName
     */
    public function setBotName(string $botName)
    {
        $this->botName = $botName;
    }
    
    
    
    /**
     * @return string
     */
    public function getRoute(): string
    {
        return $this->route;
    }
    
    
    
    /**
     * @param string $route
     */
    public function setRoute(string $route)
    {
        $this->route = $route;
    }
    
    
    
    /**
     * @return string
     */
    public function getStatusCode(): string
    {
        return $this->statusCode;
    }
    
    
    
    /**
     * @param string $statusCode
     */
    public function setStatusCode(string $statusCode)
    {
        $this->statusCode = $statusCode;
    }
    
    
    
    /**
     * @return string
     */
    public function getIsMobile(): string
    {
        return $this->isMobile;
    }
    
    
    
    /**
     * @param string $isMobile
     */
    public function setIsMobile(string $isMobile)
    {
        $this->isMobile = $isMobile;
    }
}