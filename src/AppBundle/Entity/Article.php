<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 20:29
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\TranslationBundle\Model\Message as TransMsg;
use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ArticleRepository")
 * @ORM\Table(name="article", indexes={})
 * @ORM\Cache()
 */
class Article implements TranslationContainerInterface
{
    const MAX_PER_FRONT_PAGE = 10;
    const MAX_PER_ADMIN_PAGE = 20;
    
    const STATUS_STUB        = 0;
    const STATUS_VISIBLE     = 1;
    const STATUS_ARCHIVED    = 2;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="Tag")
     * @ORM\JoinColumn(name="fk_tag_id", referencedColumnName="id")
     */
    private $fktag;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=150)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="subtitle", type="string", length=300)
     */
    private $subtitle;
    
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;
    
    /**
     * @var string
     *
     * @ORM\Column(name="image_cover", type="string", length=250)
     */
    private $imageCover = 'logo.png';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private $status = 1;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    /**
     * @ORM\OneToMany(targetEntity="Comment", mappedBy="fkArticle")
     */
    private $comments;
    
    
    
    /**
     * Article constructor.
     */
    public function __construct()
    {
        $this->comments = new ArrayCollection();
    }
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
    
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFktag()
    {
        return $this->fktag;
    }
    
    
    
    /**
     * @param mixed $fktag
     *
     * @return $this
     */
    public function setFktag($fktag)
    {
        $this->fktag = $fktag;
    
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    
    
    /**
     * @param string $title
     *
     * @return $this
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getSubtitle()
    {
        return $this->subtitle;
    }
    
    
    
    /**
     * @param string $subtitle
     *
     * @return $this
     */
    public function setSubtitle(string $subtitle)
    {
        $this->subtitle = $subtitle;
    
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    
    
    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getImageCover()
    {
        return $this->imageCover;
    }
    
    
    
    /**
     * @param string $imageCover
     *
     * @return $this
     */
    public function setImageCover(string $imageCover)
    {
        $this->imageCover = $imageCover;
    
        return $this;
    }
    
    
    
    /**
     * @param bool $textReturn
     *
     * @return mixed
     */
    public function getStatus($textReturn=false)
    {
        if( !$textReturn ) {
            return $this->status;
        }
        
        switch ( $this->status ) {
            case self::STATUS_VISIBLE:
                $str = 'news.status.visible';
                break;
    
            case self::STATUS_STUB:
                $str = 'news.status.stub';
                break;
            
            case self::STATUS_ARCHIVED:
                $str = 'news.status.archived';
                break;
            
            default:
                throw new Exception("Status not implemented");
        }
        
        return $str;
    }
    
    
    
    /**
     * @param mixed $status
     *
     * @return $this
     */
    public function setStatus($status)
    {
        $this->status = $status;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return $this
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
    
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
    
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return $this
     */
    public function setModificationDate(\DateTime $modificationDate)
    {
        $this->modificationDate = $modificationDate;
    
        return $this;
    }
    
    
    
    /**
     * Returns an array of messages.
     *
     * @return array<Message>
     */
    public static function getTranslationMessages()
    {
        return [
            new TransMsg('news.status.stub'),
            new TransMsg('news.status.visible'),
            new TransMsg('news.status.archived')
        ];
    }
}