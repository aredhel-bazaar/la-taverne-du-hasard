<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161231162215 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (1,'Super Admin','ROLE_SUPER_ADMIN',1);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (2,'Administrateur','ROLE_ADMIN',2);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (3,'Modérateur','ROLE_MODERATOR',3);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (4,'Écrivain','ROLE_NEWS_WRITER',4);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (5,'Maître de Jeu','ROLE_GAME_MASTER',5);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (6,'Adhérent','ROLE_ADHERENT',6);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (7,'Membre','ROLE_MEMBER',7);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (8,'Nouveau','ROLE_NEWBIE',8);");
        $this->addSql("INSERT INTO `role` (`id`,`name`,`role`,`order`) VALUES (9,'Visiteur','ROLE_VISITOR',9);");
    }
    
    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');
    }
}