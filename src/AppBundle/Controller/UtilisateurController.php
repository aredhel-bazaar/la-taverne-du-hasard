<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Preference;
use AppBundle\Entity\PrivateConversation;
use AppBundle\Entity\PrivateConversationUser;
use AppBundle\Entity\PrivateMessage;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use AppBundle\Form\UserChangePasswordForm;
use AppBundle\Form\UserContactForm;
use AppBundle\Form\UserEditPreferencesForm;
use AppBundle\Form\UserEditProfileForm;
use AppBundle\Form\UserRecoverPasswordForm;
use AppBundle\Form\UserSignupForm;
use AppBundle\Service\ImageManipulator;
use AppBundle\Service\Notifier;
use AppBundle\Service\Slugger;
use AppBundle\Service\TokenGenerator;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class UtilisateurController
 *
 * @Route("/utilisateur")
 */
class UtilisateurController extends BaseController
{
    /**
     * Gestion de l'inscription d'un utilisateur
     *
     * --------------------*
     * @Route("/inscription", name="user_register")
     * @Method({"GET", "POST"})
     * @Security("!is_granted('IS_AUTHENTICATED_FULLY')")
     * --------------------*
     *
     * @param EntityManager       $em
     * @param Request             $request
     *
     * @param TokenGenerator      $tokenGeneratorService
     * @param Slugger             $sluggerService
     * @param Notifier            $notifierService
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function registerAction(EntityManager $em, Request $request, TokenGenerator $tokenGeneratorService, Slugger $sluggerService, Notifier $notifierService, TranslatorInterface $translator)
    {
        $user = new User();
        $form = $this->createForm(
            UserSignupForm::class,
            $user,
            [
                'action' => $this->generateUrl('user_register'),
            ]
        );
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $token = $tokenGeneratorService->generateToken(30);
                $role  = $em->getRepository('AppBundle:Role')->findOneBy(['role' => Role::ROLE_NEWBIE]);
                $now   = new \DateTime();
                
                $user
                    ->setNickname($sluggerService->slugify($user->getPublicName()))
                    ->setTheme(
                        User::DEFAULT_THEME
                    )->setStatus(User::STATUS_PENDING)
                    ->setAccountValidationCode($token->getToken())
                    ->setCreationDate($now)
                    ->setModificationDate($now)
                    ->addRole($role);
                
                foreach (Preference::PREFERENCES_NAME as $preference_name => $default_value) {
                    $preference = new Preference();
                    $preference->setName($preference_name)->setValue($default_value)->setCreationDate(
                            $now
                        )->setModificationDate($now);
                    
                    $em->persist($preference);
                    
                    $user->addPreference($preference);
                }
                
                $em->persist($user);
                $em->flush();
    
                $notifierService->notify(Notifier::NOTIFY_CASE_REGISTER, $user->getId());
                
                $this->addFlash('success', $translator->trans('signup.success'));
                
                return $this->redirectToRoute('homepage');
            }
        }
        
        return $this->render(
            'user/register.html.twig',
            [
                'Utilisateur' => $user,
                'form'        => $form->createView(),
                'nosidebar'   => true,
            ]
        );
    }
    
    
    
    /**
     * --------------------*
     * @Route("/validation/{user}/{token}", name="user_email_validation")
     * @Method("GET")
     * --------------------*
     * @param EntityManager       $em
     * @param TranslatorInterface $translator
     * @param User                $user
     * @param string              $token
     *
     * @return RedirectResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function emailValidationAction(EntityManager $em, TranslatorInterface $translator, User $user, string $token)
    {
        if ($user->getAccountValidationCode() === $token) {
            $role = $em->getRepository('AppBundle:Role')->findOneBy(['role' => Role::ROLE_MEMBER]);
            $user->setAccountValidationCode(null)->setStatus(User::STATUS_ACTIVE)->addRole($role);
            
            $em->persist($user);
            $em->flush();
            
            $this->addFlash('success', $translator->trans('email_validation.success'));
        }
        
        return new RedirectResponse($this->generateUrl('homepage'));
    }
    
    
    
    /**
     *  Mot de passe oublié - étape 1 : demande d'un nouveau mot de passe
     *
     * --------------------*
     * @Route("/mot-de-passe-perdu", name="user_recover_password_request")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param TranslatorInterface   $translator
     * @param Request               $request
     *
     * @param TokenGenerator        $tokenGeneratorService
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function recoverPasswordAction(EntityManager $em, TranslatorInterface $translator, Request $request, TokenGenerator $tokenGeneratorService, Notifier $notifier)
    {
        $form = $this->createForm(
            UserRecoverPasswordForm::class,
            null,
            [
                'action' => $this->generateUrl('user_recover_password_request'),
            ]
        );
        
        $form->handleRequest($request);
        
        $form_data = $form->getData();
        
        if ($form->isSubmitted()) {
            /** @var User $user */
            $user = $em->getRepository('AppBundle:User')->findOneByEmail($form_data['email']);
            
            if (!is_object($user)) {
                $form->addError(
                    new FormError($translator->trans('lostpassword.text.error_email_not_found'))
                );
            }
            
            if ($form->isValid()) {
                $token = $tokenGeneratorService->generateToken()->getToken();
                
                //-- Génération d'un nouveau Token
                $user->setRecoverPasswordCode($token);
                $user->setCodeExpirationDate(
                    new \DateTime(date(\DateTime::W3C, time() + User::DEFAULT_TOKEN_EXPIRATION_TIME))
                );
                //--
                
                //-- Sauvegarde du Token dans la base
                $em->persist($user);
                $em->flush();
                //--
                
                //-- Préparation et envoi du mail à l'utilisateur avec le lien de récupération de mot de passe
                try {
                    $notifier->notify(Notifier::NOTIFY_PASSWORD_LOST, $user->getId());
                    $this->addFlash('success', $translator->trans('lostpassword.text.mail_sent'));
                    
                    return $this->redirectToRoute('user_recover_password_request');
                } catch (\Exception $e) {
                    $form->addError(new FormError($translator->trans('lostpassword.text.error_sendmail')));
                }
                //--
            }
        }
        
        return $this->render(
            'user/recover_password.html.twig',
            [
                'form'      => $form->createView(),
                'nosidebar' => true,
            ]
        );
    }
    
    
    
    /**
     *  Mot de passe oublié - étape 2 : vérification du token
     *
     * --------------------*
     * @Route("/nouveau-mot-de-passe/{token}", name="user_change_password")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager       $em
     * @param TranslatorInterface $translator
     * @param string              $token
     * @param Request             $request
     *
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function changePasswordAction(EntityManager $em, TranslatorInterface $translator, string $token, Request $request)
    {
        /** @var User $user */
        $user = $em->getRepository('AppBundle:User')->findOneByRecoverPasswordCode($token);
        
        if (is_null($user)) {
            $this->addFlash('error', $translator->trans('error.token_invalid'));
            
            return $this->redirectToRoute('homepage');
        }
        
        if ($user->getCodeExpirationDate() < new \DateTime()) {
            $this->addFlash('error', $translator->trans('error.token_expired'));
            
            return $this->redirectToRoute('homepage');
        }
        
        $form = $this->createForm(UserChangePasswordForm::class, $user);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $user->setRecoverPasswordCode(null);
                $user->setCodeExpirationDate(null);
                $user->setModificationDate(new \DateTime());
                
                $em->persist($user);
                $em->flush();
                
                $this->addFlash('success', $translator->trans('user_change_password.success'));
                
                return $this->redirectToRoute('security_login');
            }
        }
        
        return $this->render(
            'user/change_password.html.twig',
            [
                'Form'      => $form->createView(),
                'nosidebar' => true,
            ]
        );
    }
    
    
    
    /**
     * --------------------*
     * @Route("/find-user/{publicName}", defaults={"publicName": null}, options={"expose"=true}, name="app_utilisateur_finduser")
     * @Method({"GET"})
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param string        $publicName
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function findUserAction(EntityManager $em, string $publicName = null)
    {
        /** @var User[] $users */
        $users = $em->getRepository('AppBundle:User')->findByPublicNameLike($publicName);
    
        foreach ($users as &$user) {
            $user = [
                'id'         => $user->getId(),
                'publicName' => $user->getPublicName(),
                'avatar'     => $user->getAvatar(),
            ];
        }
        
        return $this->json([
            'users' => $users,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager $em
     * @param string        $nickname
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userProfileAction(EntityManager $em, string $nickname)
    {
        /** @var User $user */
        $user       = $em->getRepository('AppBundle:User')->findOneByNickname($nickname);
        $data_views = [
            'user' => $user,
        ];
        
        return $this->render('user/profile.html.twig', $data_views);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}/edit")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param string        $nickname
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userEditAction(EntityManager $em, string $nickname)
    {
        /** @var User $user */
        $user       = $em->getRepository('AppBundle:User')->findOneByNickname($nickname);
    
        if (!$this->getUser()->canEditProfileOf($user)) {
            return $this->redirectToRoute('app_utilisateur_userprofile', ['nickname' => $nickname]);
        }
        
        $data_views = [
            'user' => $user,
        ];
        
        return $this->render('user/user_edit.html.twig', $data_views);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}/edit-profile")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager       $em
     * @param TranslatorInterface $translator
     * @param string              $nickname
     * @param Request             $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function userEditProfileAction(EntityManager $em, TranslatorInterface $translator, string $nickname, Request $request)
    {
        /** @var User $user */
        $user = $em->getRepository('AppBundle:User')->findOneByNickname($nickname);
        
        if (is_null($user)) {
            if (is_null($this->getUser())) {
                return $this->redirectToRoute('homepage');
            }
            
            return $this->redirectToRoute(
                'app_utilisateur_userprofile',
                ['nickname' => $this->getUser()->getNickname()]
            );
        }
        
        if (!$this->getUser()->canEditProfileOf($user)) {
            return $this->redirectToRoute('app_utilisateur_userprofile', ['nickname' => $nickname]);
        }
        
        $form = $this->createForm(
            UserEditProfileForm::class,
            $user,
            [
                'action' => $this->generateUrl('app_utilisateur_usereditprofile', ['nickname' => $user->getNickname()]),
            ]
        );
        
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                
                $em->flush();
    
                $this->addFlash('success', $translator->trans('user.profile_edit.success'));
            }
            return $this->redirectToRoute('app_utilisateur_userprofile', ['nickname' => $nickname]);
        }
        
        return $this->render(
            'user/profile_edit.html.twig',
            [
                'user'   => $user,
                'form'   => $form->createView(),
            ]
        );
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}/edit-preferences")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager       $em
     * @param TranslatorInterface $translator
     * @param string              $nickname
     * @param Request             $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function userEditPreferencesAction(EntityManager $em, TranslatorInterface $translator, string $nickname, Request $request)
    {
        /** @var User $user */
        $user = $em->getRepository('AppBundle:User')->findOneByNickname($nickname);
        
        //-- Some checkings
        if (is_null($user)) {
            if (is_null($this->getUser())) {
                return $this->redirectToRoute('homepage');
            }
            
            return $this->redirectToRoute(
                'app_utilisateur_userprofile',
                ['nickname' => $this->getUser()->getNickname()]
            );
        }
        
        if (!$this->getUser()->canEditProfileOf($user)) {
            return $this->redirectToRoute('app_utilisateur_userprofile', ['nickname' => $nickname]);
        }
        //-- End of the checkings
        
        $userPreferences = $em->getRepository('AppBundle:Preference')->findBy(['fkUser' => $user]);
    
        $form = $this->createForm(
            UserEditPreferencesForm::class,
            null,
            [
                'action'      => $this->generateUrl('app_utilisateur_usereditpreferences', ['nickname' => $user->getNickname()]),
                'preferences' => $userPreferences,
            ]
        );
    
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $form_data = $form->getData();
                
                foreach( $form_data as $key => $value ) {
                    $userPreferences[$key]->setValue($value);
                }
                
                $em->flush();
                
                $this->addFlash('success', $translator->trans('user.preference_edit.success'));
                return $this->redirectToRoute('app_utilisateur_userprofile', ['nickname' => $nickname]);
            }
        }
        
        return $this->render(
            'user/preferences_edit.html.twig',
            [
                'user'            => $user,
                'form' => $form->createView(),
            ]
        );
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}/conversations")
     * @Method("GET")
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param string        $nickname
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userConversationsAction(EntityManager $em, string $nickname)
    {
        if (is_null($this->getUser())) {
            return $this->redirectToRoute('homepage');
        }
        
        //-- Temporaire. Innaccessible aux membres le temps du dev
        if (!$this->isGranted('ROLE_ADMIN')) {
            return new Response('', Response::HTTP_NOT_IMPLEMENTED);
        }
        //--
        
        if ($this->getUser()->getNickname() !== $nickname) {
            return $this->redirectToRoute(
                'app_utilisateur_userconversations',
                [
                    'nickname' => $this->getUser()->getNickname(),
                ]
            );
        }
        
        $conversations = $em
            ->getRepository('AppBundle:PrivateConversation')
            ->findByUser($this->getUser())
        ;
        
        $conversationsUsers = $em
            ->getRepository('AppBundle:PrivateConversationUser')
            ->findByConversations($conversations);
    
        /**
         * @var int $id
         * @var PrivateConversation $conversation
         */
        foreach ($conversations as $id => &$conversation) {
            $conversation->setPrivateConversationUsers($conversationsUsers[$id]);
        }
        
        return $this->render('user/conversation/list.html.twig', [
           'conversations' => $conversations,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}/conversation/{privateConversation}")
     * @Method("GET")
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager       $em
     * @param string              $nickname
     * @param PrivateConversation $privateConversation
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function userConversationAction(EntityManager $em, string $nickname, PrivateConversation $privateConversation)
    {
        if (is_null($this->getUser())) {
            return $this->redirectToRoute('homepage');
        }
        
        if ($this->getUser()->getNickname() !== $nickname) {
            return $this->redirectToRoute(
                'app_utilisateur_userconversations',
                [
                    'nickname' => $this->getUser()->getNickname(),
                ]
            );
        }
        
        /** @var PrivateConversationUser $privateConversationUser */
        foreach ($privateConversation->getPrivateConversationUsers() as &$privateConversationUser) {
            if ($privateConversationUser->getFkUser() === $this->getUser()) {
                $privateConversationUser->setModificationDate(new \DateTime());
            }
        }
        
        $em->flush();
        
        return $this->render('user/conversation/view.html.twig', [
           'conversation' => $privateConversation,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}/contact")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param string        $nickname
     *
     * @param Request       $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function userContactAction(EntityManager $em, string $nickname, Request $request)
    {
        $user_contact       = [];
        $users_conversation = [];
        if ($this->getUser()->getNickname() !== $nickname) {
            $user = $em->getRepository('AppBundle:User')->findOneByNickname($nickname);
            if ($user !== null) {
                $user_contact = [$user->getId()];
                $users_conversation[$user->getId()] = $user;
            }
        }
    
        $form = $this->createForm(UserContactForm::class, ['users' => $user_contact]);
        $form->handleRequest($request);
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $form_data = $form->getData();
                $now       = new \DateTime();
                $empty_dateTime = new \DateTime();
                $empty_dateTime->setTimestamp(0);
    
                $conversation = new PrivateConversation();
                $em->persist($conversation);
    
                $conversation->setAuthor($this->getUser())
                    ->setTitle($form_data['title'])
                    ->setCreationDate($now)
                    ->setModificationDate($now)
                ;
    
                array_unshift($form_data['users'], $this->getUser()->getId());
                foreach ($form_data['users'] as $user_id) {
                    try {
                        $user = $em->getRepository('AppBundle:User')->find($user_id);
                        if (is_null($user->getId())) {
                            throw new \Exception();
                        }
                    } catch (\Exception $e) {
                        continue;
                    }
                    
                    $conversationUser = new PrivateConversationUser();
                    $em->persist($conversationUser);
                    
                    $conversationUser->setFkUser($user)
                        ->setFkPrivateConversation($conversation)
                        ->setCreationDate($now)
                        ->setModificationDate($this->getUser()->getId() == $user_id ? $now : $empty_dateTime)
                    ;
                    $conversation->addPrivateConversationUser($conversationUser);
                }
    
    
                $message = new PrivateMessage();
                $em->persist($message);
    
                $message->setFkUser($conversation->getAuthor())
                    ->setFkPrivateConversation($conversation)
                    ->setMessage($form_data['message'])
                    ->setCreationDate($now)
                    ->setModificationDate($now)
                ;
    
                $conversation->addMessage($message);
    
                try {
                    $em->flush();
                    
                    return $this->redirectToRoute('app_utilisateur_userconversations', ['nickname' => $this->getUser()->getNickname()]);
                } catch (\Exception $e) {
                
                }
            }
        }
    
        return $this->render('user/conversation/create.html.twig', [
            'form'               => $form->createView(),
            'users_conversation' => $users_conversation
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/id/{nickname}/conversation/{privateConversation}/supprimer")
     * @Method("GET")
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager       $em
     * @param string              $nickname
     * @param PrivateConversation $privateConversation
     *
     * @return RedirectResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteConversationAction(EntityManager $em, string $nickname, PrivateConversation $privateConversation)
    {
        if ($this->getUser()->getNickname() !== $nickname) {
            return $this->redirectToRoute(
                'app_utilisateur_userconversations',
                [
                    'nickname' => $this->getUser()->getNickname(),
                ]
            );
        }
    
        /** @var PrivateConversationUser $privateConversationUser */
        foreach ($privateConversation->getPrivateConversationUsers() as $privateConversationUser) {
            if ($privateConversationUser->getFkUser() == $this->getUser()) {
                $em->remove($privateConversationUser);
                $em->flush();
            }
        }
        
        return $this->redirectToRoute('app_utilisateur_userconversations');
    }
    
    
    
    /**
     * --------------------*
     * @Route("/upload-avatar", name="user_upload_avatar", options={"expose"=true})
     * @Method("POST")
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param Request             $request
     * @param TranslatorInterface $translator
     * @param ImageManipulator    $imageManipulatorService
     *
     * @return Response
     */
    public function uploadAvatarAction(Request $request, TranslatorInterface $translator, ImageManipulator $imageManipulatorService)
    {
        $file = $request->files->get('file');
        
        if (!$file->isValid()) {
            return $this->outputJSON(
                null,
                $translator->trans('avatar.upload.error'),
                Response::HTTP_BAD_REQUEST
            );
        }
        
        if (!in_array($file->getMimeType(), User::ACCEPTED_IMAGE_MIMES)) {
            return $this->outputJSON(
                null,
                $translator->trans('avatar.upload.mime_not_allowed'),
                Response::HTTP_FORBIDDEN
            );
        }
        
        $filename = $this->getUser()->getNickname().uniqid().'.png';
        
        try {
            $imageManipulatorService
                ->init($file, $filename)
                ->resize()
                ->move($this->getKernel()->getRootDir().DIRECTORY_SEPARATOR.'../web/media/upload/avatar/');
        } catch (\Exception $e) {
            return $this->outputJSON(
                null,
                $translator->trans('avatar.process.error'),
                Response::HTTP_BAD_REQUEST
            );
        }
        
        return $this->outputJSON(
            [
                'img_url' => $filename,
            ]
        );
    }
}
