<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SurveyRepository")
 * @ORM\Table(name="survey", indexes={})
 * @ORM\Cache()
 * @ORM\HasLifecycleCallbacks()
 */
class Survey
{
    const MAX_PER_PAGE = 20;
    
    const STATUS_OPEN  = 1;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Question", mappedBy="fkSurvey", cascade={"persist"})
     *
     */
    private $questions;
    
    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="closure_date", type="datetime")
     */
    private $closureDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="open_date", type="datetime")
     */
    private $openDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    public function __construct()
    {
        $this->questions = new ArrayCollection();
    }
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Survey
     */
    public function setId(int $id): Survey
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Survey
     */
    public function setCreationDate(\DateTime $creationDate): Survey
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return ArrayCollection
     */
    public function getQuestions()
    {
        return $this->questions;
    }
    
    
    
    /**
     * @param Collection $questions
     *
     * @return Survey
     */
    public function setQuestions(Collection $questions)
    {
        foreach ($questions as $question) {
            $this->addQuestion($question);
        }
        
        return $this;
    }
    
    
    
    /**
     * @param Question $question
     *
     * @return Survey
     */
    public function addQuestion($question)
    {
        $question->setFkSurvey($this)
            ->setRecordOrder($this->questions->count());
        
        $this->questions->add($question);
        
        return $this;
    }
    
    
    
    /**
     * @param Question $question
     *
     * @return Survey
     */
    public function removeQuestion($question)
    {
        $this->questions->removeElement($question);
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Survey
     */
    public function setModificationDate(\DateTime $modificationDate): Survey
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    
    /**
     * @param int $status
     *
     * @return Survey
     */
    public function setStatus(int $status): Survey
    {
        $this->status = $status;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getClosureDate()
    {
        return $this->closureDate;
    }
    
    
    
    /**
     * @param \DateTime $closureDate
     *
     * @return Survey
     */
    public function setClosureDate(\DateTime $closureDate): Survey
    {
        $this->closureDate = $closureDate;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    
    /**
     * @param string $name
     *
     * @return Survey
     */
    public function setName(string $name = null): Survey
    {
        $this->name = $name;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getOpenDate()
    {
        return $this->openDate;
    }
    
    
    
    /**
     * @param \DateTime $openDate
     *
     * @return Survey
     */
    public function setOpenDate(\DateTime $openDate): Survey
    {
        $this->openDate = $openDate;
        
        return $this;
    }
    
    
    
    /**
     * @ORM\PrePersist()
     */
    public function prePersist()
    {
        $now                    = new \DateTime();
     
        $this->modificationDate = $now;
        $this->creationDate     = $now;
        $this->status           = self::STATUS_OPEN;
        $this->openDate         = $now;
    
        /** @var Question $question */
        foreach ($this->getQuestions() as &$question) {
            $question->setRecordOrder($this->questions->indexOf($question))
                ->setFkSurvey($this)
            ;
        }
    }
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->modificationDate = new \DateTime();
    }
    
    
    
    public function getAnswers()
    {
        $answers = [];
        /** @var Question $question */
        foreach ($this->questions as $question) {
            /** @var Answer $anwser */
            foreach ($question->getAnswers() as $anwser) {
                $answers[] = $anwser;
            }
        }
        
        return $answers;
    }
}