<?php

namespace AppBundle\Service;

use AppBundle\Entity\Preference;
use AppBundle\Entity\User;
use AppBundle\Twig\AssetVersionExtension;
use Doctrine\Common\Persistence\ObjectManager;
use Psr\Log\LoggerInterface;
use Symfony\Bridge\Twig\TwigEngine;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Translation\TranslatorInterface;
use TijsVerkoyen\CssToInlineStyles\CssToInlineStyles;

class Notifier {
    const NOTIFY_CASE_NEW_ARTICLE = 'new_article';
    const NOTIFY_CASE_NEW_COMMENT = 'new_comment';
    const NOTIFY_CASE_NEW_EVENT   = 'new_event';
    const NOTIFY_CASE_NEW_MESSAGE = 'new_comment';
    const NOTIFY_CASE_REGISTER    = 'register';
    const NOTIFY_PASSWORD_LOST    = 'password_lost';
    
    const CACHE_KEY            = 'email_queue';
    
    /**
     * @var ObjectManager
     */
    private $objectManager;
    /**
     * @var \Swift_Mailer
     */
    private $mailer;
    /**
     * @var string
     */
    private $kernel_root_dir;
    /**
     * @var TwigEngine
     */
    private $templating;
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var array
     */
    private $mail_params;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    /**
     * @var AdapterInterface
     */
    private $cacheAdapter;
    
    
    
    /**
     * Notifier constructor.
     *
     * @param ObjectManager          $objectManager
     * @param \Swift_Mailer          $mailer
     * @param string                 $kernel_root_dir
     * @param TwigEngine             $templating
     * @param TranslatorInterface    $translator
     * @param LoggerInterface        $logger
     * @param array                  $mail_params
     */
    public function __construct(
        ObjectManager $objectManager,
        \Swift_Mailer $mailer,
        string $kernel_root_dir,
        TwigEngine $templating,
        TranslatorInterface $translator,
        LoggerInterface $logger,
        array $mail_params,
        AdapterInterface $cacheAdapter)
    {
        $this->objectManager    = $objectManager;
        $this->mailer           = $mailer;
        $this->kernel_root_dir  = $kernel_root_dir;
        $this->templating       = $templating;
        $this->cacheAdapter     = $cacheAdapter;
        $this->logger           = $logger;
        $this->mail_params      = $mail_params;
        $this->translator       = $translator;
    }
    
    
    
    /**
     * @param string $case
     * @param int    $object_id
     * @param bool   $prepare
     *
     * @throws \Exception
     */
    public function notify(string $case, int $object_id, $prepare = true)
    {
        if ($prepare) {
            $cachedEmailQueue = $this->cacheAdapter->getItem(self::CACHE_KEY);
            $emailQueue = $cachedEmailQueue->get();
            
            $emailQueue[] = ['case' => $case, 'object_id' => $object_id];
            $cachedEmailQueue->set($emailQueue);
            return;
        }
        
        switch( $case ) {
            case self::NOTIFY_PASSWORD_LOST:
                $this->notifyLostPassword($object_id);
                break;
                
            case self::NOTIFY_CASE_NEW_ARTICLE:
                $this->notifyArticle($object_id);
                break;
                
            case self::NOTIFY_CASE_NEW_COMMENT:
                $this->notifyComment($object_id);
                break;
                
            case self::NOTIFY_CASE_NEW_EVENT:
                $this->notifyEvent($object_id);
                break;
                
            case self::NOTIFY_CASE_NEW_MESSAGE:
                $this->notifyMessage($object_id);
                break;
                
            case self::NOTIFY_CASE_REGISTER:
                $this->notifyRegister($object_id);
                break;
        }
    }
    
    
    
    /**
     * Envoie un mail à tous les utilisateurs ayant demandé à recevoir une notification
     *
     * @param $object_id
     *
     * @throws \Exception
     */
    private function notifyArticle($object_id)
    {
        $article = $this->objectManager->getRepository('AppBundle:Article')->find($object_id);
        
        if( is_null($article) ) {
            throw new \Exception("Article [$object_id] not found");
        }
        
        $users     = $this->objectManager->getRepository('AppBundle:User')
            ->findByPreference(
                Preference::PREFERENCE_ALERT_ARTICLE,
                Preference::PREFERENCES_NAME[Preference::PREFERENCE_ALERT_ARTICLE]
            );
        $subject   = $this->translator->trans('mail.title.new_article');
        $mail_from = $this->mail_params['no-reply'];
        $template  = 'article_notification';
        
        /** @var User $user */
        foreach( $users as $user ) {
            $this->sendMail($mail_from, $user->getEmail(), $subject, $template, [
                'user'    => $user,
                'article' => $article
            ]);
        }
    }
    
    
    
    /**
     * @param $object_id
     *
     * @throws \Exception
     */
    private function notifyComment($object_id)
    {
        $comment = $this->objectManager->getRepository('AppBundle:Comment')->find($object_id);
    
        if( is_null($comment) ) {
            throw new \Exception("Comment [$object_id] not found");
        }
    
        $users     = $this->objectManager->getRepository('AppBundle:User')
            ->findByPreference(
                Preference::PREFERENCE_ALERT_COMMENT,
                Preference::PREFERENCES_NAME[Preference::PREFERENCE_ALERT_COMMENT]
            );
        $subject   = $this->translator->trans('mail.title.new_comment');
        $mail_from = $this->mail_params['no-reply'];
        $template  = 'comment_notification';
    
        /** @var User $user */
        foreach( $users as $user ) {
            $this->sendMail($mail_from, $user->getEmail(), $subject, $template, [
                'user'    => $user,
                'comment' => $comment
            ]);
        }
    }
    
    
    
    /**
     * @param $object_id
     *
     * @throws \Exception
     */
    private function notifyEvent($object_id)
    {
        $event = $this->objectManager->getRepository('AppBundle:Event')->find($object_id);
    
        if( is_null($event) ) {
            throw new \Exception("Event [$object_id] not found");
        }
    
        $users     = $this->objectManager
            ->getRepository('AppBundle:User')
            ->findByPreference(
                Preference::PREFERENCE_ALERT_EVENT,
                Preference::PREFERENCES_NAME[Preference::PREFERENCE_ALERT_EVENT]
            );
        $subject   = $this->translator->trans('mail.title.new_event');
        $mail_from = $this->mail_params['no-reply'];
        $template  = 'event_notification';
    
        /** @var User $user */
        foreach( $users as $user ) {
            $this->sendMail($mail_from, $user->getEmail(), $subject, $template, [
                'user'  => $user,
                'event' => $event
            ]);
        }
    }
    
    
    
    private function notifyMessage($object_id)
    {
        // TODO handle new private message notification here
    }
    
    
    
    /**
     * @param $user_id
     *
     * @throws \Exception
     */
    private function notifyRegister($user_id)
    {
        $user = $this->objectManager->getRepository('AppBundle:User')->find($user_id);
        
        if( is_null($user) ) {
            throw new \Exception("User [$user_id] not found.");
        }
        
        try {
            $this->sendMail(
                $this->mail_params['no-reply'],
                $user->getEmail(),
                $this->translator->trans('mail.title.new_registration'),
                'registration_complete',
                [
                    'user' => $user
                ]
            );
        } catch( \Exception $e ) {
            $this->logger->critical(
                "Impossible d'envoyer le mail [".$this->translator->trans('mail.title.new_registration')."]
                à l'adresse [".$user->getEmail()."] : ".$e->getMessage());
            
            throw $e;
        }
    }
    
    
    
    /**
     * @param $user_id
     *
     * @throws \Exception
     */
    private function notifyLostPassword($user_id)
    {
        $this->logger->info("Called ".__METHOD__);
    
        $user = $this->objectManager->getRepository('AppBundle:User')->find($user_id);
    
        $this->logger->info("Preparing to send an email to ".$user->getEmail());
        
        try {
            $this->sendMail(
                $this->mail_params['no-reply'],
                $user->getEmail(),
                $this->translator->trans('lostpassword.mail_subject.recover_password_request'),
                'user_recover_password_request',
                [
                    'user' => $user,
                ]
            );
        } catch( \Exception $e ) {
            $this->logger->critical(
                "Impossible d'envoyer le mail [".$this->translator->trans('lostpassword.mail_subject.recover_password_request')."]
                à l'adresse [".$user->getEmail()."] : ".$e->getMessage());
    
            throw $e;
        }
    }
    
    
    
    /**
     * Envoie un Email avec SwiftMailer
     *
     * @param string $from
     * @param string $to
     * @param string $subject
     * @param string $template
     * @param array  $data
     *
     * @throws \Exception
     */
    public function sendMail(string $from, string $to, string $subject, string $template, $data = [])
    {
        $webDir = $this->kernel_root_dir.'/../web/';
        
        //-- Initialisation de SwiftMailer et des variables de base
        $message          = \Swift_Message::newInstance();
        $data['logo_cid'] = $message->embed(\Swift_Image::fromPath($webDir.'/static/media/img/logo.png'));
        
        if( !isset($page['title']) ) {
            $data['title'] = $subject;
        }
        //--
        
        $cssToInlineStyles = new CssToInlineStyles();
        
        $htmlMail = $this->templating->render(
            'emails/'.$template.'.html.twig', $data
        );
        $cssMail  = file_get_contents($webDir.'/static/css/email.css');
        
        $htmlMail = $cssToInlineStyles->convert($htmlMail, $cssMail);
        
        $message
            ->setSubject(       $subject )
            ->setFrom(          $from    )
            ->setTo(            $to      )
            ->setBody(
                $htmlMail,
                'text/html'
            );
        
        $message
            ->addPart(
                $this->templating->render(
                    'emails/'.$template.'.txt.twig', $data
                ),
                'text/plain'
            );
        
        $this->mailer->send($message, $this->mail_params['dev']);
        
        $this->logger->info("Mail sent", [
            'from'    => $from,
            'to'      => $to,
            'subject' => $subject
        ]);
    }
}
