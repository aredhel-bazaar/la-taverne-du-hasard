<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161211204656 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        
        $this->addSql('BEGIN');
        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C82FA4C0F');
        $this->addSql('DROP INDEX IDX_9474526C458B648C ON comment');
        $this->addSql('DROP INDEX IDX_8D93D649CC7C9A99 ON user');
        $this->addSql('ALTER TABLE user DROP fk_group_id');
        $this->addSql('COMMIT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE subscriber');
        $this->addSql('CREATE INDEX IDX_9474526C458B648C ON comment (fk_article_id)');
        $this->addSql('ALTER TABLE user ADD fk_group_id INT DEFAULT NULL');
        $this->addSql('CREATE INDEX IDX_8D93D649CC7C9A99 ON user (fk_group_id)');
    }
}
