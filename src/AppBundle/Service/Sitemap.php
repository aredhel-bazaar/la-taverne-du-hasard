<?php

namespace AppBundle\Service;

use AppBundle\Entity\Article;
use AppBundle\Entity\Event;
use AppBundle\Entity\Topic;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class Sitemap
{
    const KEEP_CACHE_DELAY   = 86400;
    const CHANGE_FREQ_WEEKLY = 'weekly';
    const CHANGE_FREQ_DAILY  = 'daily';
    
    /**
     * @var RouterInterface
     */
    private $router;
    
    /**
     * @var ObjectManager
     */
    private $em;
    
    /**
     * @var array
     */
    private $sitemap;
    
    /**
     * @var string
     */
    private $cache_path;
    
    
    
    /**
     * Sitemap constructor.
     *
     * @param RouterInterface $router
     * @param ObjectManager   $em
     * @param string          $cache_dir
     */
    public function __construct(RouterInterface $router, ObjectManager $em, string $cache_dir)
    {
        $this->router       = $router;
        $this->em           = $em;
        $this->cache_path   = $cache_dir.DIRECTORY_SEPARATOR.'sitemap.cache';
    }
    
    
    
    /**
     * @return array
     */
    public function generate()
    {
        $this->sitemap = [];
        
        if( $this->checkCache() ) {
            $this->sitemap = $this->getFromCache();
        } else {
            //-- Génération des routes de ArticleController
            $articles = $this->em->getRepository('AppBundle:Article')->findBy(['status' => 1], ['creationDate' => 'DESC']);
            
            /** @var Article $article */
            foreach( $articles as $article ) {
                $data = [
                    'loc'        => $this->router->generate(
                        'app_article_read',
                        [
                            'article' => $article->getId()
                        ],
                        Router::ABSOLUTE_URL),
                    'lastmod'    => $article->getModificationDate()->format(DATE_W3C),
                    'changefreq' => self::CHANGE_FREQ_WEEKLY
                ];
                
                $this->sitemap[] = $data;
            }
            //--
            
            //-- Génération des routes de CalendarController
            $events = $this->em->getRepository('AppBundle:Event')->findBy([], ['creationDate' => 'DESC']);
            
            /** @var Event $event*/
            foreach( $events as $event ) {
                $data = [
                    'loc'        => $this->router->generate(
                        'app_calendar_day',
                        [
                            'year'  => $event->getStartDate()->format('Y'),
                            'month' => $event->getStartDate()->format('m'),
                            'day'   => $event->getStartDate()->format('d')
                        ],
                        Router::ABSOLUTE_URL),
                    'lastmod'    => $event->getModificationDate()->format(DATE_W3C),
                    'changefreq' => self::CHANGE_FREQ_WEEKLY
                ];
                
                $this->sitemap[] = $data;
            }
            //--
    
            //-- Génération des routes de ForumController
            $topics = $this->em->getRepository('AppBundle:Topic')->findBy(['status' => 1], ['creationDate' => 'DESC']);
    
            /** @var Topic $topic */
            foreach( $topics as $topic ) {
                $data = [
                    'loc'        => $this->router->generate(
                        'app_forum_topic',
                        [
                            'id' => $topic->getId(),
                            'slug' => $topic->getSlug()
                        ],
                        Router::ABSOLUTE_URL),
                    'lastmod'    => $topic->getModificationDate()->format(DATE_W3C),
                    'changefreq' => self::CHANGE_FREQ_DAILY
                ];
        
                $this->sitemap[] = $data;
            }
            //--
            
            $this->writeToCache();
        }
        
        return $this->sitemap;
    }
    
    
    
    /**
     * Vérifie que le fichier en cache est encore exploitable
     * Si le fichier n'existe pas ou que sa date d'expiration est passé, on retourne faux
     *
     * @return bool
     */
    private function checkCache()
    {
        return ( file_exists($this->cache_path) && filemtime($this->cache_path)+self::KEEP_CACHE_DELAY >= time() );
    }
    
    
    
    /**
     * Sauvegarde le contenu du sitemap dans le cache
     *
     * @throws \Exception
     */
    private function writeToCache()
    {
        $input = file_put_contents($this->cache_path, json_encode($this->sitemap));
        
        if( $input === false ) {
            throw new \Exception("Unable to save sitemap to cache.");
        }
    }
    
    
    
    /**
     * @return array
     *
     * @throws \Exception
     */
    private function getFromCache()
    {
        $content = json_decode(file_get_contents($this->cache_path), true);
        
        if( is_null($content) ) {
            throw new \Exception("Unable to get sitemap from cache.");
        }
        
        return $content;
    }
}