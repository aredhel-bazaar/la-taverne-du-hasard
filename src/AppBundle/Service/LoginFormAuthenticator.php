<?php

namespace AppBundle\Service;

use AppBundle\Controller\SecurityController;
use AppBundle\Entity\User;
use AppBundle\Entity\Utilisateur;
use AppBundle\Form\LoginForm;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoder;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\Authenticator\AbstractFormLoginAuthenticator;
use Symfony\Component\Security\Http\Util\TargetPathTrait;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class Guard Authenticator. A chaque appel d'une page, si on détecte une tentative de connexion, on vérifie que ses
 * credentials sont bons
 *
 * Class LoginFormAuthenticator
 * @package AppBundle\Service
 */
class LoginFormAuthenticator extends AbstractFormLoginAuthenticator
{
    use TargetPathTrait;

    /**
     * @var FormFactoryInterface
     */
    private $formFactory;
    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var RouterInterface
     */
    private $router;
    /**
     * @var UserPasswordEncoder
     */
    private $passwordEncoder;
    /**
     * @var FlashBagInterface
     */
    private $flashBag;
    /**
     * @var TranslatorInterface
     */
    private $translator;
    
    const LOGIN_ROUTE_NAME      = 'security_login';
    const CUSTOM_LOGIN_ERROR    = 999;
    
    
    
    public function __construct(FormFactoryInterface $formFactory, EntityManager $em, RouterInterface $router, UserPasswordEncoder $passwordEncoder, FlashBagInterface $flashBag, TranslatorInterface $translator)
    {
        $this->formFactory     = $formFactory;
        $this->em              = $em;
        $this->router          = $router;
        $this->passwordEncoder = $passwordEncoder;
        $this->flashBag        = $flashBag;
        $this->translator      = $translator;
    }
    
    
    
    /**
     * A chaque appel de page, on passe par ici. Si l'utilisateur tente de se connecter, on va lancer la suite du
     * process Guard. Autrement on skip tout puisque l'utilisateur est soit déjà connecté, soit anonyme.
     *
     * @param Request $request
     *
     * @return mixed|null|void
     */
    public function getCredentials(Request $request)
    {
        $isLoginSubmit = $request->get('_route') == self::LOGIN_ROUTE_NAME && $request->isMethod('POST');
        if (!$isLoginSubmit) {
            return;
        }

        $form = $this->formFactory->create(LoginForm::class);
        $form->handleRequest($request);


        /** @var User $user */
        $user = $form->getData();

        $credentials['email']          = $user->getEmail();
        $credentials['password']       = $user->getPassword();

        $request->getSession()->set(
            Security::LAST_USERNAME,
            $credentials['email']
        );

        return $credentials;
    }
    
    
    
    /**
     * La personne tente de se connecter, on récupère son ID et on le teste sur la base. Si la requête retourne null,
     * c'est que l'utilisateur n'existe pas. Dans ce cas Guard se chargera de renvoyer l'utilisateur sur la page de
     * connexion (@see self::getLoginUrl())
     *
     * @param mixed                 $credentials
     * @param UserProviderInterface $userProvider
     *
     * @return User|null|UserInterface
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $username = $credentials['email'];

        return $this->em->getRepository('AppBundle:User')
            ->loadUserByUsername($username);
    }
    
    
    
    /**
     * On a bien un utilisateur, maintenant il nous faut vérifier que le mot de passe fourni est bien le bon. Nous le
     * vérifions donc et retournons VRAI ou FAUX en fonction. Même comportement quel getUser() en cas de FAUX, on
     * nettoie la session et on renvoie sur la page de connexion (@see self::getLoginUrl())
     *
     * @param mixed         $credentials
     * @param UserInterface $user
     *
     * @return bool
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        $password = $credentials['password'];

        if ($this->passwordEncoder->isPasswordValid($user, $password)) {
            return true;
        }

        return false;
    }

    /**
     * Retourne l'url de connexion
     */
    protected function getLoginUrl()
    {
        return $this->router->generate(self::LOGIN_ROUTE_NAME);
    }

    /**
     * Cette méthode est appelée lorsque le process de connexion a réussi. Défini l'action à suivre lorsque
     * l'utilisateur s'est connecté
     *
     * @param Request        $request
     * @param TokenInterface $token
     * @param string         $providerKey
     *
     * @return \Symfony\Component\HttpFoundation\Response|void
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        $this->flashBag->add('success', $this->translator->trans('security.signin.success'));
        
        return;
    }

    /**
     * En cas d'erreur, on ajoute l'exception dans le FlashBag et on renvoie l'utilisateur sur la page de connexion
     *
     * @param Request                 $request
     * @param AuthenticationException $authenticationException
     *
     * @return RedirectResponse
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $authenticationException)
    {
        if( $authenticationException->getCode() == self::CUSTOM_LOGIN_ERROR ) {
            $this->flashBag->add('error', $authenticationException->getMessage());
        } else {
            if ($request->getSession() instanceof SessionInterface) {
                $request->getSession()->set(Security::AUTHENTICATION_ERROR, $authenticationException);
            }
        }

        return new RedirectResponse($this->getLoginUrl());
    }
}
