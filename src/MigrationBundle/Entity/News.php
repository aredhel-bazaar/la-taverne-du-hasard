<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace MigrationBundle\Entity;

use AppBundle\Entity\Article;

class News
{
    public $news_id;
    public $news_tag;
    public $news_titre;
    public $news_titre2;
    public $news_corps;
    public $news_img;
    public $news_etat;
    public $lk_news_membre;
    public $news_date_creation;
    public $news_date_modification;
    
    
    
    public function convertToV2(Article &$article, array $membres)
    {
        $now = new \DateTime();
        
        $article
            ->setTitle($this->news_titre)
            ->setSubtitle($this->news_titre2)
            ->setContent($this->news_corps)
            ->setImageCover($article->getImageCover())
            ->setStatus($this->news_etat == 'live')
            ->setFkUser($membres[$this->lk_news_membre]['v2'])
            ->setCreationDate(      !empty($this->news_date_creation)     ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->news_date_creation)     : $now)
            ->setModificationDate(  !empty($this->news_date_modification) ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->news_date_modification) : $now)
        ;
    }
}