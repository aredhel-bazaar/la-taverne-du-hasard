<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 07/10/2016
 * Time: 23:53
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\TranslationBundle\Model\Message as TransMsg;
use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PreferenceRepository")
 * @ORM\Table(name="preference", indexes={})
 * @ORM\Cache()
 */
class Preference implements TranslationContainerInterface
{
    const PREFERENCE_NEWSLETTER            = 'newsletter';
    const PREFERENCE_ALERT_ARTICLE         = 'alert_article';
    const PREFERENCE_ALERT_EVENT           = 'alert_event';
    const PREFERENCE_ALERT_COMMENT         = 'alert_comment';
    const PREFERENCE_ALERT_PRIVATE_MESSAGE = 'alert_private_message';
    
    const PREFERENCES_NAME                 = [
        self::PREFERENCE_NEWSLETTER            => 'true',
        self::PREFERENCE_ALERT_EVENT           => 'all',
        self::PREFERENCE_ALERT_COMMENT         => 'all',
        self::PREFERENCE_ALERT_PRIVATE_MESSAGE => 'true',
        self::PREFERENCE_ALERT_ARTICLE         => 'true',
    ];
    
    const PREFERENCES_VALUES               = [
        self::PREFERENCE_NEWSLETTER            => [
            'label.oui' => 'true',
            'label.non' => 'false'
        ],
        self::PREFERENCE_ALERT_EVENT           => [
            'label.oui'          => 'all',
            'label.subscribed_days' => 'self',
            'label.non'          => 'none',
        ],
        self::PREFERENCE_ALERT_COMMENT         => [
            'label.oui'          => 'all',
            'label.comment.answers' => 'self',
            'label.non'          => 'none'
        ],
        self::PREFERENCE_ALERT_PRIVATE_MESSAGE => [
            'label.oui' => 'true',
            'label.non' => 'false'
        ],
        self::PREFERENCE_ALERT_ARTICLE         => [
            'label.oui' => 'true',
            'label.non' => 'false'
        ],
    ];
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User", inversedBy="preferences")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string", length=100)
     */
    private $value;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    
    
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    
    
    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return $this
     */
    public function setModificationDate(\DateTime $modificationDate)
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
    
    
    
    /**
     * @param string $value
     *
     * @return Preference
     */
    public function setValue(string $value): Preference
    {
        $this->value = $value;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return Preference
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * Returns an array of messages.
     *
     * @return array<Message>
     */
    public static function getTranslationMessages()
    {
        return [
            new TransMsg('label.comment.answers'),
            new TransMsg('label.subscribed_days'),
            new TransMsg('label.oui'),
            new TransMsg('label.non')
        ];
    }
}