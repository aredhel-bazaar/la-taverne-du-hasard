<?php

namespace AppBundle\Form;

use AppBundle\Entity\Utilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserRecoverPasswordForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label'         => 'lostpassword.label.email',
                'label_attr'    => array('class' => 'sr-only'),
                'attr'          => array('placeholder' => 'lostpassword.label.email')
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
    }
}
