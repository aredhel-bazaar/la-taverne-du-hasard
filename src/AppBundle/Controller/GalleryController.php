<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Gallery;
use AppBundle\Entity\Media;
use AppBundle\Form\CreateGalleryForm;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CalendarController
 * @package AppBundle\Controller
 *
 * --------------------*
 * @Route("/galerie")
 * --------------------*
 */
class GalleryController extends BaseController
{
    private $projectDir;
    
    public function __construct($projectDir)
    {
        $this->projectDir = $projectDir;
    }
    /**
     * --------------------*
     * @Route("s/", name="app_gallery_list")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $entityManager
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(EntityManager $entityManager)
    {
        $galleries = $entityManager->getRepository('AppBundle:Gallery')->findBy(['status' => 1]);
        
        return $this->render(
            'gallery/list.html.twig',
            [
               'galleries' => $galleries,
            ]
        );
    }
    
    
    
    /**
     * --------------------*
     * @Route("/{gallery}-{slug}", name="app_gallery_view")
     * @Method("GET")
     * --------------------*
     *
     * @param Gallery $gallery
     * @param string  $slug
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function viewAction(Gallery $gallery, string $slug)
    {
        $mediaList = $gallery->getMedia();
        
        return $this->render(
            'gallery/media-list.html.twig',
            [
                'gallery'  => $gallery,
                'mediaList' => $mediaList,
            ]
        );
    }
    
    
    
    /**
     * --------------------*
     * @Route("/creer", name="app_gallery_create")
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     * --------------------*
     *
     * @param Request       $request
     * @param EntityManager $entityManager
     * @param Filesystem    $filesystem
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createAction(Request $request, EntityManager $entityManager, Filesystem $filesystem)
    {
        $form = $this->createForm(CreateGalleryForm::class, null, ['tmp_id' => $request->get('tmp_id', uniqid('gallery_'))]);

        $form->handleRequest($request);
    
        if ($form->isSubmitted()) {
            /** @var Gallery $gallery */
            $gallery   = $form->getData();
            $mediaList = $entityManager->getRepository('AppBundle:Media')->findBy(['id' => $request->get('mediaList')]);
            $mediaData = $request->get('mediaData');
            
            /** @var Media $media */
            foreach ($mediaList as $index => $media) {
                $media->setName($mediaData['name'][$index])
                    ->setDescription($mediaData['description'][$index])
                ;
                
                $gallery->addMedia($media);
            }
        }
    
        if ($form->isValid()) {
            $entityManager->persist($gallery);
            $entityManager->flush();
    
            try {
                $filesystem->rename(
                    $this->projectDir.'/web/uploads/galleries/'.$request->get($form->getName())['tmp_id'],
                    $this->projectDir.'/web/uploads/galleries/'.$gallery->getId()
                );
    
                foreach ($gallery->getMedia() as $media) {
                    $media
                        ->setThumbnailPath(
                            str_replace(
                                $request->get($form->getName())['tmp_id'],
                                $gallery->getId(),
                                $media->getThumbnailPath()
                            )
                        )
                        ->setPath(
                            str_replace(
                                $request->get($form->getName())['tmp_id'],
                                $gallery->getId(),
                                $media->getPath()
                            )
                        )
                    ;
                }
                
                $entityManager->flush();
    
                return $this->redirectToRoute('app_gallery_list');
            } catch (IOException $e) {
                $entityManager->remove($gallery);
                $entityManager->flush();
            }
        }
    
        return $this->render(
            'gallery/create.html.twig',
            [
                'form'  => $form->createView(),
            ]
        );
    }
}
