(function() {
    "use strict";

    var activeMedia;
    var nextMedia;
    var previousMedia;
    var fullscreenElm  = $('.js-fullscreen-image');
    var canCloseViewer = true;

    if (fullscreenElm.length) {
        var previousMediaBtn = $('.js-previous');
        var nextMediaBtn     = $('.js-next');

        $('.js-media-list').on('click', '.js-media', function() {
            activeMedia   = this;
            previousMedia = activeMedia.previousElementSibling;
            nextMedia     = activeMedia.nextElementSibling;

            fullscreenElm.find('.js-image img').attr('src', $(activeMedia).attr('data-path'));

            nextMediaBtn.removeClass('hidden');
            previousMediaBtn.removeClass('hidden');

            if (!nextMedia) {
                nextMediaBtn.addClass('hidden');
            }

            if (!previousMedia) {
                previousMediaBtn.addClass('hidden');
            }

            fullscreenElm.find('.js-media-name').text($(activeMedia).attr('data-name'));
            fullscreenElm.find('.js-media-description').text($(activeMedia).attr('data-description'));

            fullscreenElm.removeClass('hidden');
        });

        $('.js-fullscreen-image img')
            .mouseenter(function() {
                canCloseViewer = false;
            })
            .mouseleave(function () {
                canCloseViewer = true;
            })
        ;

        $('.background, .js-image').click(function() {
            if (canCloseViewer) {
                resetFullscreenMedia();
            }
        });

        $('.js-fullscreen-image .js-close').click(function() {
            resetFullscreenMedia();
        });

        $('.js-fullscreen-image .js-previous').click(function() {
            loadSiblingMedia(-1);
        });

        $('.js-fullscreen-image .js-next').click(function() {
            loadSiblingMedia(1);
        });

        $(document).keydown(function(e) {
            if ($('.js-fullscreen-image').hasClass('hidden')) {
                return;
            }

            switch(e.which) {
                case 27: // Escape
                    resetFullscreenMedia();
                    break;

                case 37: // Left
                    loadSiblingMedia(-1);
                    break;

                case 38: // Up
                    break;

                case 39: // Right
                    loadSiblingMedia(1);
                    break;

                case 40: // Down
                    resetFullscreenMedia();
                    break;

                default: return;
            }

            e.preventDefault();
        });
    }

    function resetFullscreenMedia() {
        $('.js-fullscreen-image').addClass('hidden');
        $('.js-next').removeClass('hidden');
        $('.js-previous').removeClass('hidden');

        activeMedia   = null;
        previousMedia = null;
        nextMedia     = null;
    }

    function loadSiblingMedia(place) {
        if (place === -1) {
            if (previousMedia) {
                $(previousMedia).click();
            }
        } else {
            if (nextMedia) {
                $(nextMedia).click();
            }
        }
    }
})();

