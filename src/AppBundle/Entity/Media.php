<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MediaRepository")
 * @ORM\Table(name="media", indexes={})
 * @ORM\Cache()
 * @ORM\HasLifecycleCallbacks()
 */
class Media
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="thumbnail_path", type="string", length=255)
     */
    private $thumbnailPath;
    
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="string", length=255)
     */
    private $path;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */
    private $description;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private $status;
    
    /**
     * @ORM\ManyToOne(targetEntity="Gallery", inversedBy="media")
     * @ORM\JoinColumn(name="fk_gallery_id", referencedColumnName="id", nullable=true)
     */
    private $fkGallery;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime", nullable=true)
     */
    private $modificationDate;
    
    
    
    public function __construct()
    {
        $this->creationDate = new DateTime();
    }
    
    
    
    /**
     * @ORM\PreUpdate()
     */
    public function preUpdate()
    {
        $this->modificationDate = new DateTime();
    }
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Media
     */
    public function setId(int $id): Media
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    
    /**
     * @param string $name
     *
     * @return Media
     */
    public function setName(string $name): Media
    {
        $this->name = $name;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getThumbnailPath(): string
    {
        return $this->thumbnailPath;
    }
    
    
    
    /**
     * @param string $thumbnailPath
     *
     * @return Media
     */
    public function setThumbnailPath(string $thumbnailPath): Media
    {
        $this->thumbnailPath = $thumbnailPath;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }
    
    
    
    /**
     * @param string $path
     *
     * @return Media
     */
    public function setPath(string $path): Media
    {
        $this->path = $path;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }
    
    
    
    /**
     * @param string $description
     *
     * @return Media
     */
    public function setDescription(string $description): Media
    {
        $this->description = $description;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    
    /**
     * @param int $status
     *
     * @return Media
     */
    public function setStatus(int $status): Media
    {
        $this->status = $status;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkGallery()
    {
        return $this->fkGallery;
    }
    
    
    
    /**
     * @param mixed $fkGallery
     *
     * @return Media
     */
    public function setFkGallery($fkGallery)
    {
        $this->fkGallery = $fkGallery;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return Media
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Media
     */
    public function setCreationDate(\DateTime $creationDate): Media
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Media
     */
    public function setModificationDate(\DateTime $modificationDate): Media
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
}
