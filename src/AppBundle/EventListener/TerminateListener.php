<?php
 
 namespace AppBundle\EventListener;
 
 use AppBundle\Entity\User;
 use Doctrine\Common\Persistence\ObjectManager;
 use Symfony\Component\Filesystem\Filesystem;

 class TerminateListener
 {
     /**
      * @var ObjectManager
      */
     private $em;
     /**
      * @var Filesystem
      */
     private $fs;
     /**
      * @var string
      */
     private $kernel_root_dir;
    
    
    
     public function __construct(ObjectManager $em, Filesystem $fs, string $kernel_root_dir)
     {
         $this->em = $em;
         $this->fs = $fs;
         $this->kernel_root_dir = $kernel_root_dir;
     }
    
    
    
     public function onKernelTerminate()
     {
         $probability = rand(1, 100);
         
         if ($probability > 80) {
             $this->cleanAvatarFolder();
         }
     }
     
     
     
     private function cleanAvatarFolder()
     {
         $users   = $this->em->getRepository('AppBundle:User')->findAll();
         $avatars = [];
         $web_dir = $this->kernel_root_dir.'/../web';
    
         /** @var User $user */
         foreach ($users as $user) {
             if ($user->getAvatar() !== User::NO_AVATAR_PATH && $this->fs->exists($web_dir.$user->getAvatar())) {
                 $avatars[] = realpath($web_dir.$user->getAvatar());
             }
         }
    
         $files = glob($web_dir.User::AVATAR_BASE_DIR.'*');
         array_walk($files, function(&$file) {
             $file = realpath($file);
         });
    
         foreach ($files as $file) {
             $file = realpath($file);
             //-- N'appartient à aucun user et est vieux de plus d'une heure
             if (!in_array($file, $avatars) && filectime($file) < (time() - 3600)) {
                 $this->fs->remove($file);
             }
             //--
         }
     }
 }