<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170408193253 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE accreditation (id INT AUTO_INCREMENT NOT NULL, fk_topic_id INT DEFAULT NULL, fk_section_id INT DEFAULT NULL, fk_role_id INT DEFAULT NULL, right_type VARCHAR(30) NOT NULL, access TINYINT(1) NOT NULL, INDEX IDX_3BF9D0D82D7D63E3 (fk_topic_id), INDEX IDX_3BF9D0D8284D29E9 (fk_section_id), INDEX IDX_3BF9D0D8262C1F80 (fk_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE accreditation ADD CONSTRAINT FK_3BF9D0D82D7D63E3 FOREIGN KEY (fk_topic_id) REFERENCES topic (id)');
        $this->addSql('ALTER TABLE accreditation ADD CONSTRAINT FK_3BF9D0D8284D29E9 FOREIGN KEY (fk_section_id) REFERENCES section (id)');
        $this->addSql('ALTER TABLE accreditation ADD CONSTRAINT FK_3BF9D0D8262C1F80 FOREIGN KEY (fk_role_id) REFERENCES role (id)');
        $this->addSql('DROP TABLE `right`');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE `right` (id INT AUTO_INCREMENT NOT NULL, fk_role_id INT DEFAULT NULL, fk_section_id INT DEFAULT NULL, fk_topic_id INT DEFAULT NULL, right_type VARCHAR(30) NOT NULL COLLATE utf8mb4_unicode_ci, access TINYINT(1) NOT NULL, INDEX IDX_B4CA75142D7D63E3 (fk_topic_id), INDEX IDX_B4CA7514284D29E9 (fk_section_id), INDEX IDX_B4CA7514262C1F80 (fk_role_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE `right` ADD CONSTRAINT FK_B4CA7514262C1F80 FOREIGN KEY (fk_role_id) REFERENCES role (id)');
        $this->addSql('ALTER TABLE `right` ADD CONSTRAINT FK_B4CA7514284D29E9 FOREIGN KEY (fk_section_id) REFERENCES section (id)');
        $this->addSql('ALTER TABLE `right` ADD CONSTRAINT FK_B4CA75142D7D63E3 FOREIGN KEY (fk_topic_id) REFERENCES topic (id)');
        $this->addSql('DROP TABLE accreditation');
    }
}
