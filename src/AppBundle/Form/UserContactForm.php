<?php

namespace AppBundle\Form;

use AppBundle\Entity\Comment;
use AppBundle\Entity\User;
use AppBundle\Extensions\Form\Type\UserSearchType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;

class UserContactForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'privateMessage.title'
            ])
            ->add('user_search', UserSearchType::class, [
                'label' => 'privateMessage.user.add',
                'attr' => [
                    'class' => 'js-search-user'
                ],
                'required' => false
            ])
            ->add('users', CollectionType::class, [
                'allow_add'    => true,
                'allow_delete' => true,
                'entry_type'   => IntegerType::class,
                'entry_options' => [
                    'label_attr' => [
                        'class' => 'hidden'
                    ],
                    'attr' => [
                        'class' => 'hidden'
                    ]
                ]
            ])
            ->add('message', CKEditorType::class, [
                'label' => 'privateMessage.message',
                'config_name' => 'writer_config'
            ])
        ;
    }
}
