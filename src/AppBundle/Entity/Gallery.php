<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GalleryRepository")
 * @ORM\Table(name="gallery", indexes={})
 * @ORM\Cache()
 */
class Gallery
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private $status;
    /**
     * @ORM\OneToMany(targetEntity="Media", mappedBy="fkGallery", fetch="EAGER")
     */
    private $media;
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime", nullable=true)
     */
    private $modificationDate;
    
    
    
    public function __construct()
    {
        $this->media = new ArrayCollection();
        $this->creationDate = new \DateTime();
    }
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Gallery
     */
    public function setId(int $id): Gallery
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    
    /**
     * @param string $name
     *
     * @return Gallery
     */
    public function setName(string $name): Gallery
    {
        $this->name = $name;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    
    /**
     * @param int $status
     *
     * @return Gallery
     */
    public function setStatus(int $status): Gallery
    {
        $this->status = $status;
        
        return $this;
    }
    
    
    
    /**
     * @return ArrayCollection
     */
    public function getMedia()
    {
        return $this->media;
    }
    
    
    
    /**
     * @param array $media
     *
     * @return $this
     */
    public function setMedia(array $media)
    {
        $this->media = $media;
        
        return $this;
    }
    
    
    
    /**
     * @param Media $media
     *
     * @return $this
     */
    public function addMedia(Media $media)
    {
        $media->setFkGallery($this);
        $this->media->add($media);
        
        return $this;
    }
    
    
    
    /**
     * @param Media $media
     *
     * @return $this
     */
    public function removeMedia(Media $media)
    {
        $this->media->removeElement($media);
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return Gallery
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Gallery
     */
    public function setCreationDate(\DateTime $creationDate): Gallery
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Gallery
     */
    public function setModificationDate(\DateTime $modificationDate): Gallery
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
}
