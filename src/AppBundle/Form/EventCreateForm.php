<?php

namespace AppBundle\Form;

use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use AppBundle\Extensions\Form\Type\CalendarType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;

class EventCreateForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'event_create_form.label.name',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('fkOrganizer', EventOrganizerForm::class, [
                'label'              => false,
                'required'           => false
            ])
            ->add('startDate', CalendarType::class, [
                'label' => 'event_create_form.label.startDate',
                'attr'  => [
                    'readonly'    => false,
                    'placeholder' => 'event_create_form.label.startDate'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('endDate', CalendarType::class, [
                'label' => 'event_create_form.label.endDate',
                'attr'  => [
                    'readonly'    => false,
                    'placeholder' => 'event_create_form.label.endDate'
                ],
                'constraints' => [
                    new NotBlank()
                ]
            ]);
        
        if( $options['no_places'] == false ) {
            $builder
                ->add('places', IntegerType::class, [
                'label' => 'event_create_form.label.places',
                'constraints' => new Range([
                    'min' => 0
                ])
            ]);
        }
        
        $builder
            ->add('content', CKEditorType::class, [
                'label'       => 'event_create_form.label.content',
                'config_name' => 'admin_config',
                'constraints' => [
                    new NotBlank()
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'         => Event::class,
            'no_organizer'       => false,
            'no_places'          => false
        ));
    }
}
