<?php

namespace AppBundle\Form;

use AppBundle\Entity\Survey;
use AppBundle\Extensions\Form\Type\CalendarType;
use JMS\TranslationBundle\Model\Message;
use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class SurveyForm extends AbstractType implements TranslationContainerInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, [
                'label' => 'survey.form.name.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('questions', CollectionType::class, [
                'entry_type'   => QuestionForm::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'attr'         => [
                    'class'        => 'js-questions-container questions-container',
                    'add_label'    => 'question.add.label',
                ],
                'prototype_name' => '__question_index__'
            ])
            ->add('closureDate', CalendarType::class, [
                'label' => 'survey.form.closureDate.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Survey::class
        ]);
    }
    
    
    
    /**
     * Returns an array of messages.
     *
     * @return array<Message>
     */
    public static function getTranslationMessages()
    {
        return [
            new Message('question.add.label'),
        ];
    }
}
