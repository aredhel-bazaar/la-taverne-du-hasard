<?php

namespace AppBundle\Form;

use AppBundle\Entity\Event;
use AppBundle\Entity\Preference;
use AppBundle\Entity\User;
use AppBundle\Extensions\Form\Type\CalendarType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Range;

class UserEditPreferencesForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $preferences = $options['preferences'];
        
        $builder
            ->add(Preference::PREFERENCE_NEWSLETTER, ChoiceType::class, [
                'label' => 'user.preference.label.newsletter',
                'choices' => Preference::PREFERENCES_VALUES[Preference::PREFERENCE_NEWSLETTER],
                'multiple' => false,
                'expanded' => true,
                'attr'      => [
                    'choices_inline' => true
                ],
                'data' => $preferences[Preference::PREFERENCE_NEWSLETTER]->getValue()
            ])
            ->add(Preference::PREFERENCE_ALERT_ARTICLE, ChoiceType::class, [
                'label' => 'user.preference.label.alert_article',
                'choices' => Preference::PREFERENCES_VALUES[Preference::PREFERENCE_ALERT_ARTICLE],
                'multiple' => false,
                'expanded' => true,
                'attr'      => [
                    'choices_inline' => true
                ],
                'data' => $preferences[Preference::PREFERENCE_ALERT_ARTICLE]->getValue()
            ])
            ->add(Preference::PREFERENCE_ALERT_EVENT, ChoiceType::class, [
                'label' => 'user.preference.label.alert_event',
                'choices' => Preference::PREFERENCES_VALUES[Preference::PREFERENCE_ALERT_EVENT],
                'multiple' => false,
                'expanded' => true,
                'attr'      => [
                    'choices_inline' => true
                ],
                'data' => $preferences[Preference::PREFERENCE_ALERT_EVENT]->getValue()
            ])
            ->add(Preference::PREFERENCE_ALERT_COMMENT, ChoiceType::class, [
                'label' => 'user.preference.label.alert_comment',
                'choices' => Preference::PREFERENCES_VALUES[Preference::PREFERENCE_ALERT_COMMENT],
                'multiple' => false,
                'expanded' => true,
                'attr'      => [
                    'choices_inline' => true
                ],
                'data' => $preferences[Preference::PREFERENCE_ALERT_COMMENT]->getValue()
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'preferences' => []
        ));
    }
}