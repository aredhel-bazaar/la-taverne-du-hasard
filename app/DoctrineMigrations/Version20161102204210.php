<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161102204210 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment CHANGE fk_news_id fk_article_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE comment ADD CONSTRAINT FK_9474526C82FA4C0F FOREIGN KEY (fk_article_id) REFERENCES article (id)');
        $this->addSql('CREATE INDEX IDX_9474526C82FA4C0F ON comment (fk_article_id)');
        $this->addSql('ALTER TABLE lk_user_role ADD CONSTRAINT FK_A686F952D60322AC FOREIGN KEY (role_id) REFERENCES role (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment DROP FOREIGN KEY FK_9474526C82FA4C0F');
        $this->addSql('DROP INDEX IDX_9474526C82FA4C0F ON comment');
        $this->addSql('ALTER TABLE comment CHANGE fk_article_id fk_news_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE lk_user_role DROP FOREIGN KEY FK_A686F952D60322AC');
    }
}
