(function() {
    "use strict";

    var objectCollection = {};

    $.fn.extend({
        buttonLoader: function (option) {
            if (option === undefined) {
                throw "Error";
            }

            var elm = this[0];
            var loaderHeight;
            var cssClass;
            var inode = objectCollection.size;

            switch (option) {
                case 'start':
                    objectCollection[inode] = elm;
                    elm.setAttribute('data-inode', inode);

                    elm.style.width   = elm.offsetWidth + 'px';
                    elm.style.height  = elm.offsetHeight + 'px';
                    elm.className    += ' ui active inline loader';
                    elm.innerHTML     = '';

                    loaderHeight = window.getComputedStyle(elm, ':before').getPropertyValue('height').replace('px', '');
                    cssClass     = '.' + elm.className.split(' ').join('.');

                    if (typeof document.styleSheets[0].insertRule === 'function') {
                        document.styleSheets[0].insertRule(cssClass+'::before, '+cssClass+'::after {margin-top: ' + ((elm.offsetHeight - loaderHeight) / 2) + 'px}', 1);
                    } else if (typeof document.styleSheets[0].addRule === 'function') {
                        document.styleSheets[0].addRule(cssClass+'::before, '+cssClass+'::after', 'margin-top: ' + ((elm.offsetHeight - loaderHeight) / 2) + 'px}');
                    }
                    break;

                case 'stop':
                    if (elm.length) {
                        inode = elm.getAttribute('data-inode');
                        var originalElm = objectCollection[inode];

                        document.replaceChild(elm, originalElm);
                    }
                    break;
            }
        }
    });
})();
