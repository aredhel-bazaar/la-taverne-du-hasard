<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170319183637 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE message (id INT AUTO_INCREMENT NOT NULL, fk_topic_id INT DEFAULT NULL, fk_user_id INT DEFAULT NULL, content LONGTEXT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_B6BD307F2D7D63E3 (fk_topic_id), INDEX IDX_B6BD307F5741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE section (id INT AUTO_INCREMENT NOT NULL, fk_section_id INT DEFAULT NULL, title VARCHAR(100) NOT NULL, `order` INT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_2D737AEF284D29E9 (fk_section_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE topic (id INT AUTO_INCREMENT NOT NULL, fk_section_id INT DEFAULT NULL, fk_user_id INT DEFAULT NULL, title VARCHAR(100) NOT NULL, slug VARCHAR(100) NOT NULL, status INT NOT NULL, pinned TINYINT(1) NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_9D40DE1B284D29E9 (fk_section_id), INDEX IDX_9D40DE1B5741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F2D7D63E3 FOREIGN KEY (fk_topic_id) REFERENCES topic (id)');
        $this->addSql('ALTER TABLE message ADD CONSTRAINT FK_B6BD307F5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE section ADD CONSTRAINT FK_2D737AEF284D29E9 FOREIGN KEY (fk_section_id) REFERENCES section (id)');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1B284D29E9 FOREIGN KEY (fk_section_id) REFERENCES section (id)');
        $this->addSql('ALTER TABLE topic ADD CONSTRAINT FK_9D40DE1B5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE section DROP FOREIGN KEY FK_2D737AEF284D29E9');
        $this->addSql('ALTER TABLE topic DROP FOREIGN KEY FK_9D40DE1B284D29E9');
        $this->addSql('ALTER TABLE message DROP FOREIGN KEY FK_B6BD307F2D7D63E3');
        $this->addSql('DROP TABLE message');
        $this->addSql('DROP TABLE section');
        $this->addSql('DROP TABLE topic');
    }
}
