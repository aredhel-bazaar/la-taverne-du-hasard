<?php

namespace AppBundle\Service\Block;

use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

abstract class AbstractBlock
{
    /**
     * @var AuthorizationCheckerInterface
     */
    protected $authorizationChecker;
    
    
    public function checkAuthorizations()
    {
        return $this->authorizationChecker->isGranted(get_called_class()::MIN_ROLE);
    }
}