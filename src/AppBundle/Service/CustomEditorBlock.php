<?php

namespace AppBundle\Service;

use AppBundle\Service\Block\BlockInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class CustomEditorBlock
{
    const DETECTION_PATTERN = '/\[\[block\]\]([0-9a-zA-Z;=]+)\[\[endblock\]\]/i';
    const BLOCKS_NAMESPACE  = 'AppBundle\\Service\\Block\\';

    /**
     * @var string
     */
    private $text;
    /**
     * @var array
     */
    private $blocks = [];
    /**
     * @var array
     */
    private $replaces;
    /**
     * @var ContainerInterface
     */
    private $container;
    
    
    
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }
    
    
    
    /**
     * @param string $text
     *
     * @return mixed
     */
    public function parseCustomBlocks(string $text)
    {
        $this->text = $text;
        
        preg_match_all(self::DETECTION_PATTERN, $this->text, $matches);
        
        if (empty($matches)) {
            return $this->text;
        }
        
        $blocks = $matches[1];
    
        foreach ($blocks as $index => $block) {
            $pairs = explode(';', $block);
    
            foreach ($pairs as $pair) {
                $key_value = explode('=', $pair);
                
                $this->blocks[$index][$key_value[0]] = $key_value[1];
            }
        }
        
        return $this->convertBlockToHtml();
    }
    
    
    
    /**
     * @return mixed
     */
    private function convertBlockToHtml()
    {
        foreach ($this->blocks as $index => $block) {
            $className = self::BLOCKS_NAMESPACE.ucfirst($block['type']).'Block';
            if ($this->container->has($className)) {
                /**
                 * @var BlockInterface $class
                 */
                $class = $this->container->get($className);
    
                $this->replaces[$index] = '';
                
                if ($class->checkAuthorizations()) {
                    $this->replaces[$index] = $this->container->get('templating')->render(
                        'template/block/default.html.twig',
                        [
                            'type' => $block['type'],
                            'id'   => $block['id'],
                            'url'  => get_class($class)::ROUTE_NAME
                        ]
                    );
                }
            }
        }
        
        return preg_replace_callback(
            self::DETECTION_PATTERN,
            function () {
                return array_shift($this->replaces);
            },
            $this->text
        );
    }
}