<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\TranslationBundle\Model\Message as TransMsg;
use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="user", indexes={
 *     @ORM\Index(name="email", columns={"email"}),
 *     @ORM\Index(name="nickname", columns={"nickname"})
 * })
 * @UniqueEntity(fields={"email"}, message="form.error.email_already_exists")
 * @ORM\Cache(usage="NONSTRICT_READ_WRITE")
 */
class User implements AdvancedUserInterface , \Serializable, TranslationContainerInterface, EquatableInterface
{
    const STATUS_INACTIVE   = 0;
    const STATUS_PENDING    = 1;
    const STATUS_ACTIVE     = 2;
    
    const DEFAULT_TOKEN_EXPIRATION_TIME = 3600;
    
    const DEFAULT_THEME   = 'default';
    
    const AVATAR_BASE_DIR = '/media/upload/avatar/';
    const NO_AVATAR_PATH  = '/media/img/placeholders/noavatar.png';
    
    const MAX_PER_ADMIN_PAGE = 20;
    
    const STATUS_LABELS = [
        'user.status.inactive',
        'user.status.pending',
        'user.status.active',
        'user.status.banned'
    ];
    
    const ACCEPTED_IMAGE_MIMES = [
        'image/png',
        'image/gif',
        'image/jpg',
        'image/jpeg',
    ];
    
    
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="nickname", type="string", length=60)
     */
    private $nickname;
    
    /**
     * @var string
     *
     * @ORM\Column(name="public_name", type="string", length=60)
     */
    private $publicName;
    
    /**
     * @var string
     *
     * @Assert\NotBlank(groups={"Registration"})
     * @Assert\Email(
     *     checkMX = true,
     *     checkHost = true
     * )
     * @ORM\Column(name="email", type="string", length=255)
     */
    private $email;
    
    /**
     * Mot de passe non encodé. Non sauvegardé en base !
     *
     * @var string
     *
     * @Assert\NotBlank(groups={"Registration"})
     * @Assert\Length(
     *     min="6",
     *     minMessage="form.error.password_too_short"
     * )
     */
    private $plainPassword;
    
    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=60)
     */
    private $password;
    
    /**
     * @ORM\ManyToMany(targetEntity="Role", inversedBy="users", fetch="EAGER")
     * @ORM\OrderBy({"order" = "ASC"})
     * @ORM\JoinTable(name="lk_user_role")
     *
     */
    private $roles;
    
    /**
     * @ORM\OneToMany(targetEntity="Preference", mappedBy="fkUser", cascade={"persist"})
     */
    private $preferences;
    
    /**
     * @var string
     *
     * @ORM\Column(name="avatar", type="string", length=255, nullable=true)
     */
    private $avatar;
    
    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern     = "/^[a-z\-]+$/i",
     *     htmlPattern = "^[a-zA-Z\-]+$",
     *     message     = "form.error.firstname_should_only_contains_letters"
     * )
     */
    private $firstName;
    
    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=true)
     * @Assert\Regex(
     *     pattern     = "/^[a-z\-]+$/i",
     *     htmlPattern = "^[a-zA-Z\-]+$",
     *     message     = "form.error.lastname_should_only_contains_letters"
     * )
     */
    private $lastName;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birthdate", type="datetime", nullable=true)
     */
    private $birthdate;
    
    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", nullable=true)
     */
    private $gender;
    
    /**
     * @var string
     *
     * @ORM\Column(name="locality", type="string", length=100, nullable=true)
     */
    private $locality;
    
    /**
     * @var string
     *
     * @ORM\Column(name="presentation", type="string", nullable=true)
     */
    private $presentation;
    
    /**
     * @var string
     *
     * @ORM\Column(name="theme", type="string", length=40)
     */
    private $theme;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private $status;
    
    /**
     * @var string
     *
     * @ORM\Column(name="account_validation_code", type="string", length=60, nullable=true)
     */
    private $AccountValidationCode;
    
    /**
     * @var string
     *
     * @ORM\Column(name="recover_password_code", type="string", length=60, nullable=true)
     */
    private $recoverPasswordCode;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="code_expiration_date", type="datetime", nullable=true)
     */
    private $CodeExpirationDate;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="super_admin", type="boolean")
     */
    private $superAdmin = false;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    public function __construct()
    {
        $this->roles       = new ArrayCollection();
        $this->preferences = new ArrayCollection();
    }
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return User
     */
    public function setId(int $id): User
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getNickname()
    {
        return $this->nickname;
    }
    
    
    
    /**
     * @param string $nickname
     *
     * @return $this
     */
    public function setNickname(string $nickname)
    {
        $this->nickname = $nickname;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getPublicName()
    {
        return $this->publicName;
    }
    
    
    
    /**
     * @param string $publicName
     *
     * @return $this
     */
    public function setPublicName(string $publicName)
    {
        $this->publicName = $publicName;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }
    
    
    
    /**
     * @param string $email
     *
     * @return $this
     */
    public function setEmail($email)
    {
        $this->email = $email;
        
        return $this;
    }
    
    
    
    /**
     * @param string $password
     *
     * @return $this
     */
    public function setPassword($password) {
        $this->password = $password;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }
    
    
    
    /**
     * @param string $plainPassword
     *
     * @return $this
     */
    public function setPlainPassword($plainPassword)
    {
        $this->plainPassword = $plainPassword;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getAvatar(): string
    {
        if( is_null($this->avatar) ) {
            return self::NO_AVATAR_PATH;
        }
        
        return self::AVATAR_BASE_DIR.$this->avatar;
    }
    
    
    
    /**
     * @param string $avatar
     *
     * @return $this
     */
    public function setAvatar(string $avatar)
    {
        $this->avatar = $avatar;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }
    
    
    
    /**
     * @param string $firstName
     *
     * @return $this
     */
    public function setFirstName(string $firstName)
    {
        $this->firstName = $firstName;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }
    
    
    
    /**
     * @param string $lastName
     *
     * @return $this
     */
    public function setLastName(string $lastName)
    {
        $this->lastName = $lastName;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getBirthdate()
    {
        return $this->birthdate;
    }
    
    
    
    /**
     * @param \DateTime $birthdate
     *
     * @return $this
     */
    public function setBirthdate(\DateTime $birthdate)
    {
        $this->birthdate = $birthdate;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }
    
    
    
    /**
     * @param string $gender
     *
     * @return $this
     */
    public function setGender(string $gender)
    {
        $this->gender = $gender;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getLocality()
    {
        return $this->locality;
    }
    
    
    
    /**
     * @param string $locality
     *
     * @return $this
     */
    public function setLocality(string $locality)
    {
        $this->locality = $locality;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getPresentation()
    {
        return $this->presentation;
    }
    
    
    
    /**
     * @param string $presentation
     *
     * @return $this
     */
    public function setPresentation(string $presentation)
    {
        $this->presentation = $presentation;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getTheme()
    {
        return $this->theme;
    }
    
    
    
    /**
     * @param string $theme
     *
     * @return $this
     */
    public function setTheme(string $theme)
    {
        $this->theme = $theme;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    
    /**
     * @param int $status
     *
     * @return $this
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getAccountValidationCode()
    {
        return $this->AccountValidationCode;
    }
    
    
    
    /**
     * @param string $AccountValidationCode
     *
     * @return $this
     */
    public function setAccountValidationCode($AccountValidationCode)
    {
        $this->AccountValidationCode = $AccountValidationCode;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getRecoverPasswordCode()
    {
        return $this->recoverPasswordCode;
    }
    
    
    
    /**
     * @param string $recoverPasswordCode
     *
     * @return $this
     */
    public function setRecoverPasswordCode($recoverPasswordCode)
    {
        $this->recoverPasswordCode = $recoverPasswordCode;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCodeExpirationDate()
    {
        return $this->CodeExpirationDate;
    }
    
    
    
    /**
     * @param \DateTime $CodeExpirationDate
     *
     * @return $this
     */
    public function setCodeExpirationDate($CodeExpirationDate)
    {
        $this->CodeExpirationDate = $CodeExpirationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return boolean
     */
    public function isSuperAdmin(): bool
    {
        if ($this->getRoles()->indexOf(Role::ROLE_SUPER_ADMIN)  !== false) {
            return true;
        }
        
        return false;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return $this
     */
    public function setModificationDate(\DateTime $modificationDate)
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
	/**
	 * Returns an array of messages.
	 *
	 * @return array<Message>
	 */
	public static function getTranslationMessages() {
	    $messages = [];
	    
	    foreach( self::STATUS_LABELS as $label ) {
	        $messages[] = new TransMsg($label);
        }
	    
        return $messages;
    }
    
    
    
    public function setRoles($roles)
    {
        $this->roles = $roles;
        
        return $this;
	}
    
    
    
    public function addRole(Role $role)
    {
        $this->roles[] = $role;
        
        return $this;
    }
    
    
    
    public function removeRole(Role $role)
    {
        $this->roles->removeElement($role);
    }
    
    
    
    /**
     * Returns the roles granted to the user.
     *
     * <code>
     * public function getRoles()
     * {
     *     return array('ROLE_USER');
     * }
     * </code>
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @param bool $raw
     *
     * @return ArrayCollection|array [] The user roles
     */
	public function getRoles($raw=false) {
        if( $raw !== true && $this->roles instanceof PersistentCollection) {
            $roleList = new ArrayCollection();
    
            if( $this->roles->count() > 0 ) {
                /** @var Role $role */
                foreach( $this->roles as $role ) {
                    $roleList->add($role->getRole());
                }
            }
    
            return $roleList->toArray();
        }
        
        return $this->roles;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getPreferences()
    {
        return $this->preferences;
    }
    
    
    
    public function addPreference(Preference &$preference)
    {
        $preference->setFkUser($this);
        
        $this->preferences[] = $preference;
        
        return $this;
    }
    
    
    
    public function removePreference(Preference $preference)
    {
        $this->preferences->removeElement($preference);
    }
    
    
    
    /**
     * @param mixed $preferences
     *
     * @return User
     */
    public function setPreferences($preferences)
    {
        $this->preferences = $preferences;
        
        return $this;
    }
	
	
	
	/**
	 * Returns the password used to authenticate the user.
	 *
	 * This should be the encoded password. On authentication, a plain-text
	 * password will be salted, encoded, and then compared to this value.
	 *
	 * @return string The password
	 */
	public function getPassword() {
		return $this->password;
	}
	
	
	
	/**
	 * Returns the salt that was originally used to encode the password.
	 *
	 * This can return null if the password was not encoded using a salt.
	 *
	 * @return string|null The salt
	 */
	public function getSalt() {
		// TODO: Implement getSalt() method.
	}
	
	
	
	/**
	 * Returns the username used to authenticate the user.
	 *
	 * @return string The username
	 */
	public function getUsername() {
        return $this->email;
	}
	
	
	
	/**
	 * Removes sensitive data from the user.
	 *
	 * This is important if, at any given point, sensitive information like
	 * the plain-text password is stored on this object.
	 */
	public function eraseCredentials() {
		$this->setPlainPassword(null);
	}
    
    
    
    /**
     * String representation of object
     * @link  http://php.net/manual/en/serializable.serialize.php
     * @return string the string representation of the object or null
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->nickname,
            $this->password,
        ));
    }
    
    
    
    /**
     * Constructs the object
     * @link  http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     *
     * @return void
     * @since 5.1.0
     */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->nickname,
            $this->password
        ) = unserialize($serialized);
    }
    
    
    
    /**
     * Checks whether the user's account has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw an AccountExpiredException and prevent login.
     *
     * @return bool true if the user's account is non expired, false otherwise
     *
     * @see AccountExpiredException
     */
    public function isAccountNonExpired()
    {
        return true;
    }
    
    
    
    /**
     * Checks whether the user is locked.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a LockedException and prevent login.
     *
     * @return bool true if the user is not locked, false otherwise
     *
     * @see LockedException
     */
    public function isAccountNonLocked()
    {
        return true;
    }
    
    
    
    /**
     * Checks whether the user's credentials (password) has expired.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a CredentialsExpiredException and prevent login.
     *
     * @return bool true if the user's credentials are non expired, false otherwise
     *
     * @see CredentialsExpiredException
     */
    public function isCredentialsNonExpired()
    {
        return true;
    }
    
    
    
    /**
     * Checks whether the user is enabled.
     *
     * Internally, if this method returns false, the authentication system
     * will throw a DisabledException and prevent login.
     *
     * @return bool true if the user is enabled, false otherwise
     *
     * @see DisabledException
     */
    public function isEnabled()
    {
        return $this->status == 2;
    }
    
    
    
    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * Also implementation should consider that $user instance may implement
     * the extended user interface `AdvancedUserInterface`.
     *
     * @param UserInterface $user
     *
     * @return bool
     */
    public function isEqualTo(UserInterface $user)
    {
        return (
            ($this->getUsername() === $user->getUsername() || $this->getNickname() === $user->getNickname())
            &&
            $this->getPassword() === $user->getPassword()
        );
    }
    
    
    
    public function getStatusLabel($index)
    {
        return self::STATUS_LABELS[$index];
    }
    
    
    
    public function canEditProfileOf(UserInterface $user)
    {
        return (
            $this->getRoles(true)->indexOf(Role::ROLE_ADMIN)
            ||
            $this->isEqualTo($user)
        );
    }
}