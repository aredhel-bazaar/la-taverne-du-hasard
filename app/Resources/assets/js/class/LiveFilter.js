"use strict";

function LiveFilter(context, target) {
    this.context     = context;
    this.target      = target;
    this.criterions  = {};
    this.timer       = null;
}

LiveFilter.prototype.addCriterion = function(name, value) {
    this.resetTimer();

    if( this.criterions[name] === undefined ) {
        this.criterions[name] = [];
    }

    this.criterions[name].push(value);
};

LiveFilter.prototype.removeCriterion = function(name, value) {
    this.resetTimer();

    var index = this.criterions[name].indexOf(value);

    if( index !== -1 ) {
        this.criterions[name].splice(index, 1);
    }
};

LiveFilter.prototype.setCriterion = function(name, value) {
    this.resetTimer();

    this.criterions[name] = value;
};

LiveFilter.prototype.resetTimer = function() {
    if( this.timer !== undefined ) {
        clearTimeout(this.timer);
    }

    var instance = this;

    this.timer = setTimeout(function() {
        instance.sendQuery();
    }, 500);
};

LiveFilter.prototype.sendQuery = function() {
    var context = this.context;

    $.ajax({
        url: this.target,
        type: 'POST',
        data: this.criterions
    })
        .done(function(data) {
            $(context).trigger('livefilter', data);
        });
};
