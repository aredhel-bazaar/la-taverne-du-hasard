'use strict';

var gulp      = require('gulp'),
    compass   = require('gulp-compass'),
    minifyCSS = require('gulp-minify-css'),
    concat    = require('gulp-concat'),
    minify    = require('gulp-minify');
const del   = require('del');
var config = {
    assetsDir: 'app/Resources/assets',
    production: false,
    sourceMaps: true,
    bowerDir: 'vendor/bower_components'
};
var minifyConfig = {
    ext: {
        min: '.min.js'
    }
};

/** START : Arguments to Config */
process.argv.forEach(function(argument) {
    if (argument.charAt(0) === '-') {
        var argumentKey   = '';
        var argumentValue = '';

        if (argument.charAt(1) === '-') {
            argumentKey   = argument.slice(2).replace(/(\-\w)/g, function(m){return m[1].toUpperCase();});
            argumentValue = true;
        } else {
            var splittedArgument = argument.split('=');
            argumentKey          = splittedArgument[0].slice(1).replace(/(\-\w)/g, function(m){return m[1].toUpperCase();});
            argumentValue        = splittedArgument[1];
        }

        config[argumentKey] = argumentValue;
    }
});
/** END : Arguments to Config */

var tasks = [
    'clean', 'media', 'sass', 'javascript', 'libraries', 'fonts'
];

if (!config.noWatch && !config.production) {
    tasks.push('sass:watch', 'javascript:watch');
}

gulp.task('sass', function () {
    gulp.src(config.assetsDir + '/scss/main.scss')
        .pipe(compass({
            config_file: './config.rb',
            css: 'web/static/css',
            sass: config.assetsDir + '/scss'
        }))
        .pipe(minifyCSS())
        .pipe(gulp.dest('web/static/css'))
    ;
});

gulp.task('sass:watch', function () {
    gulp.watch(config.assetsDir+'/scss/**/*.scss', ['sass']);
});

gulp.task('media', function () {
    gulp
        .src([config.assetsDir + '/media/**/*'])
        .pipe(gulp.dest('web/static/media'))
    ;
});

gulp.task('javascript', function () {
    gulp.src(config.assetsDir + '/js/**/*.js')
        .pipe(concat('main.js'))
        .pipe(minify(minifyConfig))
        .pipe(gulp.dest('web/static/js'))
    ;
});

gulp.task('libraries', function () {
    /* CSS */
    gulp.src([
        config.bowerDir+'/toastr/toastr.css',
        config.bowerDir+'/semantic-ui-calendar/dist/calendar.css',
        'vendor/enyo/dropzone/dist/dropzone.css'
    ])
        .pipe(concat('libraries.css'))
        .pipe(minifyCSS())
        .pipe(gulp.dest('web/static/css'));

    /* JS */
    gulp.src([
        config.bowerDir+'/jquery/dist/jquery.js',
        config.bowerDir+'/semantic/dist/semantic.js',
        config.bowerDir+'/toastr/toastr.js',
        config.bowerDir+'/semantic-ui-calendar/dist/calendar.js',
        config.bowerDir+'/moment/min/moment-with-locales.js',
        'vendor/enyo/dropzone/dist/dropzone.js',
        config.bowerDir+'/ckeditor/*'
    ])
        .pipe(concat('libraries.js'))
        .pipe(minify(minifyConfig))
        .pipe(gulp.dest('web/static/js'))
    ;

    gulp.src('app/web/**/*')
        .pipe(gulp.dest('web/static/lib'));

    /* Font Awesome */
    gulp.src(config.bowerDir+'/font-awesome/css/font-awesome.min.css')
        .pipe(gulp.dest('web/static/lib/font-awesome/css'))
    ;
});

gulp.task('javascript:watch', function () {
    gulp.watch(config.assetsDir+'/js/**/*.js', ['javascript']);
});

gulp.task('fonts', function() {
    gulp.src(config.bowerDir+'/semantic/dist/**/*')
        .pipe(gulp.dest('web/static/lib/semantic'))
    ;
});

gulp.task('clean', function () {
    del.sync([
        'web/static/*'
    ]);
});

/** Run the tasks */
gulp.task('default', tasks);
