<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 19:41
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OpenEventRepository")
 * @ORM\Table(name="open_event", indexes={})
 * @ORM\Cache()
 */
class OpenEvent
{
    const MAX_PER_ADMIN_PAGE = 20;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="open", type="boolean")
     */
    private $open;
    
    /**
     * @ORM\ManyToOne(targetEntity="Event")
     * @ORM\JoinColumn(name="fk_event_id", referencedColumnName="id")
     */
    private $fkEvent;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="date")
     */
    private $date;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return OpenEvent
     */
    public function setId(int $id): OpenEvent
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return boolean
     */
    public function isOpen(): bool
    {
        return $this->open;
    }
    
    
    
    /**
     * @param boolean $open
     *
     * @return OpenEvent
     */
    public function setOpen(bool $open): OpenEvent
    {
        $this->open = $open;
        
        return $this;
    }
    
    
    
    /**
     * @return Event|null
     */
    public function getFkEvent()
    {
        return $this->fkEvent;
    }
    
    
    
    /**
     * @param mixed $fkEvent
     *
     * @return OpenEvent
     */
    public function setFkEvent($fkEvent)
    {
        $this->fkEvent = $fkEvent;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return OpenEvent
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getDate(): \DateTime
    {
        return $this->date;
    }
    
    
    
    /**
     * @param \DateTime $date
     *
     * @return OpenEvent
     */
    public function setDate(\DateTime $date): OpenEvent
    {
        $this->date = $date;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return OpenEvent
     */
    public function setCreationDate(\DateTime $creationDate): OpenEvent
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return OpenEvent
     */
    public function setModificationDate(\DateTime $modificationDate): OpenEvent
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
}
