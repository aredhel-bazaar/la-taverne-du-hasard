<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Message;
use AppBundle\Entity\Accreditation;
use AppBundle\Entity\Role;
use AppBundle\Entity\Section;
use AppBundle\Entity\Topic;
use AppBundle\Entity\User;
use AppBundle\Form\MessageCreateForm;
use AppBundle\Form\SectionCreateForm;
use AppBundle\Form\TopicCreateForm;
use AppBundle\Service\AccreditationProvider;
use AppBundle\Service\CustomEditorBlock;
use AppBundle\Service\Slugger;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Flash\FlashBag;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class NewsController
 *
 * --------------------*
 * @Route("/forum")
 * --------------------*
 */
class ForumController extends BaseController
{
    /**
     * --------------------*
     * @Route("/widget", name="app_forum_widget")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetAction(EntityManager $em)
    {
        if (is_null($this->getUser())) {
            $roles = [
                'ROLE_VISITOR'
            ];
        } else {
            //-- Full access, no restriction
            if ($this->isGranted('ROLE_ADMIN')) {
                $roles = [];
            } else {
                $roles = $this->getUser()->getRoles();
            }
        }
        
        $topics   = $em->getRepository('AppBundle:Topic')->findLastViewableTopics($roles);
        $messages = $em->getRepository('AppBundle:Message')->findLastViewableMessages($roles);
        
        return $this->render('forum/widget.html.twig', [
            'topics'   => $topics,
            'messages' => $messages
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/", name="app_forum_index")
     * @Route("/section/{id}-{slug}", name="app_forum_section")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager         $em
     * @param AccreditationProvider $accreditationProviderService
     * @param int|null              $id
     * @param string|null           $slug
     *
     * @return Response
     * @internal param Accreditation $accreditationService
     */
    public function indexAction(EntityManager $em, AccreditationProvider $accreditationProviderService, int $id = null, string $slug = null)
    {
        $section = null;
        if (!is_null($slug)) {
            $section = $em->getRepository('AppBundle:Section')
                ->find($id);
            
            if (is_null($section) || $section->isArchived() and !$accreditationProviderService->canModerate($section)) {
                $this->redirectToRoute('app_forum_index');
            }
    
            //-- Security layer
            if (!$accreditationProviderService->canRead($section)) {
                $slug = (!empty($section->getFkSection()) ? $section->getFkSection()->getSlug() : null);
                $id   = (!empty($section->getFkSection()) ? $section->getFkSection()->getId() : null);
                return $this->redirectToRoute(
                    (is_null($slug) ? 'app_forum_index' : 'app_forum_section'),
                    [
                        'slug' => $slug,
                        'id'   => $id,
                    ]
                );
            }
            //--
        }
    
        $criterions = [
            'fkSection' => $section,
            'archived'  => false,
        ];
        if ($this->isGranted('ROLE_MODERATOR') || (!is_null($section) && $accreditationProviderService->canModerate($section))) {
            unset($criterions['archived']);
        }
        $sections = $em->getRepository('AppBundle:Section')
            ->findBy($criterions, ['title' => 'ASC']);
    
        $accreditationProviderService->filter($sections, Accreditation::TYPE_READ);
    
        $criterions = [
            'fkSection' => $section,
            'status'    => 1,
            'archived'  => false,
        ];
        if ($this->isGranted('ROLE_MODERATOR') || (!is_null($section) && $accreditationProviderService->canModerate($section))) {
            unset($criterions['archived']);
        }
        $topics = $em->getRepository('AppBundle:Topic')
            ->findBy($criterions, ['pinned' => 'DESC', 'modificationDate' => 'DESC']);
        $accreditationProviderService->filter($topics, Accreditation::TYPE_READ);
    
        return $this->render('forum/section/list.html.twig', [
            'sectionParent' => $section,
            'sections'      => $sections,
            'topics'        => $topics,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/sujet/{id}-{slug}", name="app_forum_topic")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param TranslatorInterface   $translator
     * @param EntityManager         $em
     * @param Request               $request
     * @param AccreditationProvider $accreditationProviderService
     * @param FlashBag              $flashBag
     * @param int                   $id
     * @param string                $slug
     *
     * @return Response
     */
    public function topicAction(TranslatorInterface $translator, EntityManager $em, Request $request, AccreditationProvider $accreditationProviderService, FlashBag $flashBag, int $id, string $slug)
    {
        $page = $request->query->getInt('page', 1);
        
        $topic = $em->getRepository('AppBundle:Topic')
            ->find($id);
    
        //-- Security layer
        if (
            is_null($topic) ||
            $topic->isArchived() && $accreditationProviderService->canModerate($topic) ||
            !$accreditationProviderService->canRead($topic)
        ) {
            $flashBag->add('warning', $translator->trans('forum.error.no_access_granted'));
            return $this->redirectToRoute(
                'app_forum_section',
                [
                'id' => $topic->getFkSection()->getId(),
                'slug' => $topic->getFkSection()->getSlug(),
                ]
            );
        }
        //--
        
        $nb_messages = $em->getRepository('AppBundle:Message')
            ->countBy(['fkTopic' => $topic]);
        
        $nb_pages    = ceil($nb_messages / Message::NB_PER_PAGE);
        
        $messages = $em->getRepository('AppBundle:Message')
            ->findBy(['fkTopic' => $topic], ['creationDate' => 'ASC'], Message::NB_PER_PAGE, Message::NB_PER_PAGE*($page-1));
        
        $formView = null;
        if ($accreditationProviderService->canWrite($topic)) {
            $message = new Message();
            $form    = $this->createForm(MessageCreateForm::class, $message, [
                'action'   => $this->generateUrl('app_forum_new_message', ['id' => $topic->getId()]),
                'is_admin' => $this->isGranted('ROLE_ADMIN')
            ]);
            $formView = $form->createView();
        }
    
        return $this->render('forum/topic/view.html.twig', [
            'topic'        => $topic,
            'messages'     => $messages,
            'form'         => $formView,
            'current_page' => $page,
            'nb_pages'     => $nb_pages,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/sujet/{id}/nouveau-message/", name="app_forum_new_message")
     * @Route("/message/{id}/editer", name="app_forum_edit_message")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param TranslatorInterface   $translator
     * @param Request               $request
     * @param AccreditationProvider $accreditationProviderService
     * @param CustomEditorBlock     $editorBlock
     * @param int|null              $id
     *
     * @return RedirectResponse|Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createMessageAction(EntityManager $em, TranslatorInterface $translator, Request $request, AccreditationProvider $accreditationProviderService, CustomEditorBlock $editorBlock, int $id)
    {
        if ($request->get('_route') == 'app_forum_new_message') {
            $topic   = $em->getRepository('AppBundle:Topic')->find($id);
            $message = new Message();
        } else {
            $message   = $em->getRepository('AppBundle:Message')->find($id);
            $topic     = $message->getFkTopic();
        }
        
        //-- Security layer
        if (!$accreditationProviderService->canWrite($topic)) {
            return $this->redirectToRoute('app_forum_topic', [
                'id'   => $topic->getId(),
                'slug' => $topic->getSlug(),
            ]);
        }
        //--
        
        $form    = $this->createForm(MessageCreateForm::class, $message, ['is_admin' => $this->isGranted('ROLE_ADMIN')]);
        $form->handleRequest($request);
    
        if ($form->isSubmitted()) {
            if (mb_strlen($message->getContent()) <= 5) {
                $form->get('content')->addError(new FormError($translator->trans('message.content.error')));
            }
    
            if ($form->isValid()) {
                $message->setContent($editorBlock->parseCustomBlocks($message->getContent()));
                
                $message->setModificationDate(new \DateTime());
                $topic->setLastActivityDate($message->getModificationDate());
                /** @var Section $section */
                $section = $topic->getFkSection();
                $section->setLastActivityDate($topic->getLastActivityDate());
                while ($section->getFkSection() != null) {
                    $section = $section->getFkSection();
                    $section->setLastActivityDate($topic->getLastActivityDate());
                }
                
                if (is_null($message->getId())) {
                    $message->setFkTopic($topic)
                        ->setCreationDate(new \DateTime())
                        ->setFkUser($this->getUser());
    
                    $em->persist($message);
                }
    
                $em->flush();
                
                $page = 1;
                $i    = 0;
                /** @var Message $topic_message */
                foreach ($topic->getMessages() as $topic_message) {
                    $i++;
                    if ($topic_message->getId() == $id) {
                        break;
                    }
                    
                    if ($i%Message::NB_PER_PAGE == 0) {
                        $page++;
                    }
                }
            
                return $this->redirectToRoute('app_forum_topic', [
                    'id'        => $topic->getId(),
                    'slug'      => $topic->getSlug(),
                    'page'      => $page,
                    '_fragment' => $message->getId(),
                ]);
            }
        }
    
        return $this->render('forum/message/create.html.twig', [
            'topic'        => $topic,
            'form'         => $form->createView(),
            'message'      => $message,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/nouvelle-section/{id}-{slug}", name="app_forum_section_create")
     * @Route("/editer-section/{id}-{slug}", name="app_forum_section_update")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param Request               $request
     * @param AccreditationProvider $accreditationProviderService
     * @param Slugger               $sluggerService
     * @param int|null              $id
     * @param string                $slug
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createSectionAction(
        EntityManager $em,
        Request $request,
        AccreditationProvider $accreditationProviderService,
        Slugger $sluggerService,
        int $id = null, string $slug = null
    )
    {
        //-- Security layer
        if (empty($slug)) {
            if (!$this->isGranted(Role::ROLE_MODERATOR) || $request->get('_route') == 'app_forum_section_update') {
                return $this->redirectToRoute('app_forum_index');
            }
        }
        //--
    
        $section         = null;
        $current_rank    = null;
        if (!is_null($id)) {
            $section     = $em->getRepository('AppBundle:Section')->find($id);
        }
        
        //-- Section update
        if ($request->get('_route') == 'app_forum_section_update') {
            if (!$accreditationProviderService->canModerate($section)) {
                return $this->redirectToRoute('app_forum_index');
            }
    
            $parentSection = $section->getFkSection();
            $current_rank  = $section->getRank();
        }
        //-- Section creation
        else {
            $parentSection = $section;
            $section       = null;
            
            if (is_null($parentSection)) {
                if (!$this->isGranted('ROLE_ADMIN')) {
                    return $this->redirectToRoute('app_forum_index');
                }
            } else {
                if (!$accreditationProviderService->canWrite($parentSection)) {
                    return $this->redirectToRoute('app_forum_index');
                }
            }
        }
        //--
    
        //-- A way to place the section inbetween
        /** @var Section[] $siblingSections */
        $siblingSections = $em->getRepository('AppBundle:Section')
            ->findBy(['fkSection' => $parentSection], ['rank' => 'ASC']);
    
        $maxRank = (is_array($siblingSections) && count($siblingSections) > 0 ? end($siblingSections)->getRank() : 0);
    
        if (is_null($section)) {
            $section = new Section();
            $section->setRank($maxRank);
        }
        //--
    
        $form = $this->createForm(SectionCreateForm::class, $section);
        $role_list = $em->getRepository('AppBundle:Role')->findBy([], ['order' => 'DESC']);
        //-- They cannot be restricted as they are admin
        unset($role_list[Role::ROLE_SUPER_ADMIN], $role_list[Role::ROLE_ADMIN]);
        //--
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (is_null($section->getId())) {
                    $section->setCreationDate(new \DateTime())
                        ->setRank($section->getRank() + 1);
                    
                    if (!is_null($parentSection)) {
                        $section->setFkSection($parentSection);
                    }
                }
    
                if (!empty($siblingSections)) {
                    $em->getRepository('AppBundle:Section')
                        ->updateSectionsRank($siblingSections, $section, $current_rank);
                }
    
                $section->setModificationDate(new \DateTime());
                $section->setSlug($sluggerService->slugify($section->getTitle()));
    
                $em->persist($section);
                $em->flush();
    
                //--
                $request->request->set('object_type', 'section');
                $request->request->set('object_id', $section->getId());
                $this->updatePermisionsAction($em, $request, $accreditationProviderService);
                //--
                
                return $this->redirectToRoute('app_forum_section', ['id' => $section->getId(), 'slug' => $section->getSlug() ]);
            }
        }
        
        return $this->render('forum/section/create.html.twig', [
            'form'      => $form->createView(),
            'sections'  => $siblingSections,
            'section'   => $section,
            'role_list' => $role_list,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/archiver-section/{id}", name="app_forum_section_archive")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param AccreditationProvider $accreditationProviderService
     * @param string|null           $id
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function archiveSectionAction(EntityManager $em, AccreditationProvider $accreditationProviderService, string $id = null)
    {
        //--
        if (empty($id)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
    
        $section = $em->getRepository('AppBundle:Section')
            ->find($id);
    
        //-- Security layer
        if (!$accreditationProviderService->canModerate($section)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
    
        if (!$section->isArchived()) {
            $section->setArchived(true);
        } else {
            $em->remove($section);
        }
        
        $em->flush();
        
        return $this->redirectToRoute('app_forum_index');
    }
    
    
    
    /**
     * --------------------*
     * @Route("/restore-section/{id}", name="app_forum_section_restore")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param AccreditationProvider $accreditationProviderService
     * @param string|null           $id
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function restoreSectionAction(EntityManager $em, AccreditationProvider $accreditationProviderService, string $id = null)
    {
        //--
        if (empty($id)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
    
        $section = $em->getRepository('AppBundle:Section')
            ->find($id);
    
        //-- Security layer
        if (!$accreditationProviderService->canModerate($section)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
        
        $section->setArchived(false);
        $em->flush();
        
        return $this->redirectToRoute('app_forum_index');
    }
    
    
    
    /**
     * --------------------*
     * @Route("/nouveau-sujet/{id}-{slug}", name="app_forum_topic_create")
     * @Route("/editer-sujet/{id}-{slug}", name="app_forum_topic_update")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param Request               $request
     * @param AccreditationProvider $accreditationProviderService
     * @param Slugger               $sluggerService
     * @param CustomEditorBlock     $editorBlock
     * @param int                   $id
     * @param string                $slug
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createTopicAction(
        EntityManager $em,
        Request $request,
        AccreditationProvider $accreditationProviderService,
        Slugger $sluggerService,
        CustomEditorBlock $editorBlock,
        int $id, string $slug)
    {
        
        if ($request->get('_route') == 'app_forum_topic_create') {
            $section = $em->getRepository('AppBundle:Section')
                ->findOneBy(['id' => $id, 'archived' => false]);
            
            if (is_null($section)) {
                return $this->redirectToRoute('app_forum_index');
            }
            
            //-- Security layer
            if (!$accreditationProviderService->canWrite($section)) {
                return $this->redirectToRoute('app_forum_section', ['id' => $section->getId(), 'slug' => $section->getSlug()]);
            }
            //--
            
            $topic = new Topic();
            $topic->addMessage(new Message());
        } else {
            $topic = $em->getRepository('AppBundle:Topic')
                ->findOneBy(['slug' => $slug, 'archived' => false]);
    
            if (is_null($topic)) {
                return $this->redirectToRoute('app_forum_index');
            }
    
            //-- Security layer
            if (!$accreditationProviderService->canModerate($topic)) {
                return $this->redirectToRoute('app_forum_index');
            }
            //--
    
            $section = $topic->getFkSection();
        }
    
        $form      = $this->createForm(TopicCreateForm::class, $topic, ['is_admin' => $this->isGranted('ROLE_ADMIN')]);
        $role_list = $em->getRepository('AppBundle:Role')->findBy([], ['order' => 'DESC']);
    
        $form->handleRequest($request);
    
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $topic->setFkSection($section)
                    ->setFkUser($this->getUser())
                    ->setSlug($sluggerService->slugify($topic->getTitle()))
                    ->setStatus(Topic::STATUS_OPEN)
                    ->setCreationDate(new \DateTime())
                    ->setModificationDate(new \DateTime());
    
                if (!$accreditationProviderService->canModerate($section)) {
                    $topic->setPinned(false);
                }
    
                /** @var Message $message */
                $message = $topic->getMessages()->first();
                $message->setFkUser($this->getUser())
                    ->setCreationDate(new \DateTime())
                    ->setModificationDate(new \DateTime())
                    ->setFkTopic($topic)
                    ->setContent($editorBlock->parseCustomBlocks($message->getContent()))
                ;
    
                $topicSection = $topic->getFkSection();
                $topic->setLastActivityDate($message->getModificationDate());
                $topicSection->setLastActivityDate($topic->getLastActivityDate());
                while ($section->getFkSection() != null) {
                    $section = $section->getFkSection();
                    $section->setLastActivityDate($topic->getLastActivityDate());
                }
                
                if ($accreditationProviderService->canModerate($section)) {
                    foreach (Accreditation::TYPES as $right_type) {
                        /** @var Role $role */
                        foreach ($role_list as $role) {
                            $right = new Accreditation();
                            $right->setFkRole($role)
                                ->setType($right_type)
                                ->setFkTopic($topic)
                                ->setAccess($request->request->get('right_'.$role->getRole().'_'.$right_type, false));
            
                            $em->persist($right);
                        }
                    }
                } else {
                    $section_rights = $topicSection->getRights(true);
                    foreach (Accreditation::TYPES as $right_type) {
                        /** @var Role $role */
                        foreach ($role_list as $role) {
                            $right = new Accreditation();
                            $right->setFkRole($role)
                                ->setType($right_type)
                                ->setFkTopic($topic)
                                ->setAccess(boolval($section_rights[$role->getRole()][$right_type]));
            
                            $em->persist($right);
                        }
                    }
                }
                
                $em->persist($message);
                $em->persist($topic);
                $em->flush();
    
                //--
                $request->request->set('object_type', 'topic');
                $request->request->set('object_id', $topic->getId());
                $this->updatePermisionsAction($em, $request, $accreditationProviderService);
                //--
                
                return $this->redirectToRoute('app_forum_topic', ['id' => $topic->getId(), 'slug' => $topic->getSlug() ]);
            }
        }
    
        return $this->render('forum/topic/create.html.twig', [
            'form'      => $form->createView(),
            'topic'     => $topic,
            'role_list' => $role_list,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/archiver-topic/{id}", name="app_forum_topic_archive")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param AccreditationProvider $accreditationProviderService
     * @param int                   $id
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function archiveTopicAction(EntityManager $em, AccreditationProvider $accreditationProviderService, int $id = null)
    {
        //--
        if (empty($id)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
        
        $topic = $em->getRepository('AppBundle:Topic')
            ->findOneBy(['id' => $id]);
        
        //-- Security layer
        if (!$accreditationProviderService->canModerate($topic)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
        
        $topic->setArchived(true);
        $em->flush();
        
        return $this->redirectToRoute('app_forum_index');
    }
    
    
    
    /**
     * --------------------*
     * @Route("/restore-topic/{id}", name="app_forum_topic_restore")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager         $em
     * @param AccreditationProvider $accreditationProviderService
     * @param string|null           $id
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function restoreTopicAction(EntityManager $em, AccreditationProvider $accreditationProviderService, string $id = null)
    {
        //--
        if (empty($id)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
        
        $topic = $em->getRepository('AppBundle:Topic')
            ->find($id);
        
        //-- Security layer
        if (!$accreditationProviderService->canModerate($topic)) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
        
        $topic->setArchived(false);
        $em->flush();
        
        return $this->redirectToRoute('app_forum_index');
    }
    
    
    
    /**
     * --------------------*
     * @Route("/breadcrumb/topic/{id}", name="app_topic_breadcrumb")
     * @Route("/breadcrumb/section/{id}", name="app_section_breadcrumb")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     * @param int|null      $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function breadcrumbAction(EntityManager $em, Request $request, int $id = null)
    {
        $breadcrumb = [];
        
        if (!is_null($id)) {
            $section    = null;
            if ($request->get('_route') == "app_topic_breadcrumb") {
                $topic = $em->getRepository('AppBundle:Topic')
                    ->find($id);
        
                if (is_null($topic)) {
                    throw new \Exception("Unable to find topic");
                }
        
                $section      = $topic->getFkSection();
                $breadcrumb[] = [
                    'url'   => $this->generateUrl('app_forum_topic', ['id' => $topic->getId(), 'slug' => $topic->getSlug()]),
                    'title' => $topic->getTitle(),
                ];
            }
    
            if (empty($section)) {
                $section = $em->getRepository('AppBundle:Section')
                    ->find($id);
            }
    
            do {
                if (is_null($section)) {
                    break;
                }
        
                $breadcrumb[] = [
                    'url'   => $this->generateUrl('app_forum_section', ['id' => $section->getId(), 'slug' => $section->getSlug()]),
                    'title' => $section->getTitle(),
                ];
        
                $section = $section->getFkSection();
            } while (!empty($section));
        }
        
        return $this->render('forum/breadcrumb.html.twig', [
            'breadcrumb' => array_reverse($breadcrumb, true),
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/permissions", name="app_forum_update_permissions")
     * @Method("POST")
     * --------------------*
     *
     * @param EntityManager         $em
     * @param Request               $request
     *
     * @param AccreditationProvider $accreditationProviderService
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function updatePermisionsAction(EntityManager $em, Request $request, AccreditationProvider $accreditationProviderService)
    {
        $role_list        = $em->getRepository('AppBundle:Role')->findAll();
        $rights_hierarchy = $em->getRepository('AppBundle:Accreditation')->getRightsHierarchy();
        
        //-- They cannot be restricted as they are admin
        unset($rights_hierarchy[Role::ROLE_SUPER_ADMIN], $rights_hierarchy[Role::ROLE_ADMIN]);
        //--
        
        $object    = $request->request->get('object_type');
        $id        = $request->request->get('object_id');
        $section   = null;
        $topic     = null;
    
        if ($object == 'section') {
            $section  = $em->getRepository('AppBundle:Section')->find($id);
            $rights   = $section->getRights();
            $isAbleTo = ($accreditationProviderService->canModerate($section));
        } else {
            $topic    = $em->getRepository('AppBundle:Topic')->find($id);
            $rights   = $topic->getRights();
            $isAbleTo = ($accreditationProviderService->canModerate($topic));
        }
        
        //-- Security layer
        if (!$isAbleTo) {
            return $this->redirectToRoute('app_forum_index');
        }
        //--
        
        /** @var Accreditation $right */
        foreach ($rights as &$right) {
            $role = $right->getFkRole()->getRole();
            $type = $right->getType();
            
            $right->setAccess($request->request->getBoolean('right_'.$role.'_'.$type));
            
            //-- This one has been processed
            unset($rights_hierarchy[$role][$type]);
            //--
        }
        
        //-- Do we miss some ?
        if (count($rights_hierarchy)) {
            foreach ($rights_hierarchy as $role => $right_types) {
                foreach ($right_types as $type => $default_value) {
                    $acccreditation = new Accreditation();
                    $acccreditation->setAccess(true)
                        ->setAccess($request->request->getBoolean('right_'.$role.'_'.$type))
                        ->setType($type)
                        ->setFkRole($role_list[$role]);
    
                    if ($object == 'section') {
                        $acccreditation->setFkSection($section);
                    } else {
                        $acccreditation->setFkTopic($topic);
                    }
                    
                    $em->persist($acccreditation);
                }
            }
        }
        //--
        
        $em->flush();
        
        return $this->redirectToRoute('app_forum_section', [
            'id'   => (isset($section) ? (!is_null($section->getFkSection()) ? $section->getFkSection()->getId() : null) : $topic->getFkSection()->getId()),
            'slug' => (isset($section) ? (!is_null($section->getFkSection()) ? $section->getFkSection()->getSlug() : null) : $topic->getFkSection()->getSlug()),
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/message/delete/{id}", name="app_forum_delete_message")
     * @Method("POST")
     * --------------------*
     *
     * @param Request               $request
     * @param AccreditationProvider $accreditationProvider
     *
     * @return Response
     */
    public function deleteMessageAction(Request $request, AccreditationProvider $accreditationProvider, int $id)
    {
    
    }
}
