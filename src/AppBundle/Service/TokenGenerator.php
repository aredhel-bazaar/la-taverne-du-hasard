<?php

namespace AppBundle\Service;

class TokenGenerator
{
    /**
     * @var string
     */
    private $token;

    /**
     * Taille par défaut d'un token
     */
    const DEFAULT_TOKEN_LENGTH          = 30;
    
    
    
    /**
     * Génère un token aléatoire et sécurisé de taille $length
     *
     * @param int  $length
     *
     * @return TokenGenerator
     */
    public function generateToken(int $length=self::DEFAULT_TOKEN_LENGTH)
    {
        $this->token = bin2hex(random_bytes($length));

        return $this;
    }

    public function getToken()
    {
        return $this->token;
    }
}
