<?php

namespace AppBundle\Form;

use AppBundle\Entity\Gallery;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class CreateGalleryForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('tmp_id', HiddenType::class, [
                'mapped' => false,
                'data'   => $options['tmp_id']
            ])
            ->add('name', TextType::class, [
                'label' => 'gallery.create.form.name',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('status', CheckboxType::class, [
                'label' => 'gallery.create.form.status',
                'attr'  => [
                    'checkbox_type' => 'toggle'
                ],
                'required' => false
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Gallery::class,
            'tmp_id'     => null
        ));
    }
}
