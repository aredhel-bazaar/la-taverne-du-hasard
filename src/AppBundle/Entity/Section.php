<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 19:41
 */

namespace AppBundle\Entity;

use AppBundle\Extensions\Entity\PermissionInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SectionRepository")
 * @ORM\Table(name="section", indexes={})
 * @ORM\Cache()
 */
class Section implements PermissionInterface
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Section
     *
     * @ORM\ManyToOne(targetEntity="Section", inversedBy="sections")
     * @ORM\JoinColumn(name="fk_section_id", referencedColumnName="id", nullable=true)
     */
    private $fkSection;
    
    /**
     * @ORM\OneToMany(targetEntity="Topic", mappedBy="fkSection", cascade={"remove"})
     */
    private $topics;
    
    /**
     * @ORM\OneToMany(targetEntity="Section", mappedBy="fkSection", cascade={"remove"})
     */
    private $sections;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="excerpt", type="string", length=300, nullable=true)
     * @Assert\Length(
     *      max = 300,
     *      maxMessage = "forum.section.excerpt.maxLength.error"
     * )
     */
    private $excerpt;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100)
     */
    private $slug;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="archived", type="boolean")
     */
    private $archived = false;
    
    /**
     * @var int
     *
     * @ORM\Column(name="rank", type="integer", length=3)
     */
    private $rank;
    
    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="Accreditation", mappedBy="fkSection", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $rights;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_activity_date", type="datetime", nullable=true)
     */
    private $lastActivityDate;
    
    
    
    /**
     * Section constructor.
     */
    public function __construct()
    {
        $this->topics = new ArrayCollection();
        $this->rights = new ArrayCollection();
    }
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Section
     */
    public function setId(int $id): Section
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return Section
     */
    public function getFkSection()
    {
        return $this->fkSection;
    }
    
    
    
    /**
     * @param Section $fkSection
     *
     * @return Section
     */
    public function setFkSection(Section $fkSection): Section
    {
        $this->fkSection = $fkSection;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getTopics()
    {
        return $this->topics;
    }
    
    
    
    /**
     * @param bool $processed
     *
     * @return array|Collection
     */
    public function getRights($processed = false): Collection
    {
        if ($processed) {
            $processedRights = [];
    
            /** @var Accreditation $right */
            foreach ($this->rights as $right) {
                $processedRights[$right->getFkRole()->getRole()][$right->getType()] = $right->hasAccess();
            }
            
            return new ArrayCollection($processedRights);
        }
        
        return $this->rights;
    }
    
    
    
    /**
     * @param ArrayCollection $rights
     *
     * @return Section
     */
    public function setRights(ArrayCollection $rights): Section
    {
        $this->rights = $rights;
        
        return $this;
    }
    
    
    
    public function getAccessRights()
    {
        return $this->getRights(true)->toArray();
    }
    
    
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    
    
    /**
     * @param string $title
     *
     * @return Section
     */
    public function setTitle(string $title): Section
    {
        $this->title = $title;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getExcerpt()
    {
        return $this->excerpt;
    }
    
    
    
    /**
     * @param string $excerpt
     *
     * @return Section
     */
    public function setExcerpt(string $excerpt): Section
    {
        $this->excerpt = $excerpt;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    
    
    /**
     * @param string $slug
     *
     * @return Section
     */
    public function setSlug(string $slug): Section
    {
        $this->slug = $slug;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getRank()
    {
        return $this->rank;
    }
    
    
    
    /**
     * @param int $rank
     *
     * @return $this
     */
    public function setRank(int $rank)
    {
        $this->rank = $rank;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Section
     */
    public function setCreationDate(\DateTime $creationDate): Section
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Section
     */
    public function setModificationDate(\DateTime $modificationDate): Section
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return bool
     */
    public function isArchived(): bool
    {
        return $this->archived;
    }
    
    
    
    /**
     * @param bool $archived
     *
     * @return Section
     */
    public function setArchived(bool $archived): Section
    {
        $this->archived = $archived;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getLastActivityDate()
    {
        return $this->lastActivityDate;
    }
    
    
    
    /**
     * @param \DateTime $lastActivityDate
     *
     * @return Section
     */
    public function setLastActivityDate(\DateTime $lastActivityDate): Section
    {
        $this->lastActivityDate = $lastActivityDate;
        
        return $this;
    }
}