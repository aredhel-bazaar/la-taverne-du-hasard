<?php

namespace AppBundle\Twig;

class HighlightTextExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('highlight_text', array($this, 'highlight_text'))
        );
    }



    /**
     * Met en surbrillance un morceau d'un texte
     *
     * @param $text
     * @param $highlight
     *
     * @return mixed
     * @internal param $text_to_highlight
     * @internal param $search
     */
    public function highlight_text($text, $highlight)
    {
        return str_ireplace($highlight, '<span class="highlight">'.$highlight.'</span>', $text);
    }



    /**
     * @return string
     */
    public function getName()
    {
        return 'highlight_text_extension';
    }
}
