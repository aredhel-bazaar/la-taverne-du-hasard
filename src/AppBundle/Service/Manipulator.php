<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\File;

class Manipulator
{
    private $width;
    private $height;
    
    private $maxHeight;
    private $maxWidth;
    
    private $newHeight;
    private $newWidth;
    private $x;
    private $y;
    /**
     * @var string
     */
    private $projectDir;
    /**
     * @var File
     */
    private $file;
    /**
     * @var string
     */
    private $filename;
    /**
     * @var resource|false
     */
    private $destination;
    /**
     * @var string
     */
    private $filepath;
    /**
     * @var bool
     */
    private $allowCrop;
    /**
     * @var bool
     */
    private $resized = false;
    
    
    
    /**
     * ImageManipulator constructor.
     *
     * @param int         $maxHeight
     * @param int         $maxWidth
     * @param string      $projectDir
     * @param File        $file
     * @param null|string $filename
     */
    public function __construct(int $maxHeight, int $maxWidth, string $projectDir, File $file, ?string $filename)
    {
        $this->maxHeight  = $maxHeight;
        $this->maxWidth   = $maxWidth;
        $this->projectDir = $projectDir;
        $this->file       = $file;
        $this->filename   = $filename ?? uniqid().'.'.$this->file->getExtension();
    
        list($this->width, $this->height, $type, $attr) = getimagesize($this->file);
    }
    
    
    
    /**
     * @param int $maxHeight
     *
     * @return $this
     */
    public function setMaxHeight(?int $maxHeight)
    {
        $this->maxHeight = $maxHeight;
        
        return $this;
    }
    
    
    
    /**
     * @param int $maxWidth
     *
     * @return $this
     */
    public function setMaxWidth(?int $maxWidth)
    {
        $this->maxWidth = $maxWidth;
        
        return $this;
    }
    
    
    
    /**
     * @param bool $allowCrop
     *
     * @return $this
     * @throws \Exception
     */
    public function resize($allowCrop = true)
    {
        $this->allowCrop = $allowCrop;
        
        if (is_null($this->maxWidth)) {
            $this->setMaxWidth($this->width);
        }
        
        if (is_null($this->maxHeight)) {
            $this->setMaxHeight($this->height);
        }
        
        $this->defineNewDimensions();
        
        $this->processResize();
        $this->resized   = true;
        
        return $this;
    }
    
    
    
    /**
     * @param $path
     *
     * @return $this
     * @throws \Exception
     */
    public function move($path)
    {
        $this->filepath = $this->projectDir.DIRECTORY_SEPARATOR.$path;
        
        if (!is_dir($this->filepath)) {
            mkdir($this->filepath, 0777, true);
        }
        
        if ($this->resized) {
            if (!imagepng($this->destination, $this->filepath.DIRECTORY_SEPARATOR.$this->filename)) {
                throw new \Exception("Failed to copy resized image to its destination.");
            }
        } else {
            if (!copy($this->file->getRealPath(), $this->filepath.DIRECTORY_SEPARATOR.$this->filename)) {
                throw new \Exception("Failed to copy image to its destination.");
            }
        }
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getResizedImageHeight()
    {
        return $this->newHeight;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getResizedImageWidth()
    {
        return $this->newWidth;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getResizedImagePath()
    {
        return $this->filepath.DIRECTORY_SEPARATOR.$this->filename;
    }
    
    
    
    /**
     *
     */
    private function defineNewDimensions()
    {
        $this->newHeight = $this->maxHeight;
        $this->newWidth  = $this->maxWidth;
        $x               = 0;
        $y               = 0;
        $ratioHaut       = $this->maxHeight / $this->height;
        $ratioLarg       = $this->maxWidth / $this->width;
        
        if ($this->width > $this->maxWidth && $this->height > $this->maxHeight) {
            
            if ($ratioHaut >= $ratioLarg) {
                $newLarg = $this->width * $ratioHaut;
                $x = round(($this->maxWidth - $newLarg) / 2);
            }
            else {
                $newHaut = $this->height * $ratioLarg;
                $y = round(($this->maxHeight - $newHaut) / 2);
            }
        }
        elseif ($this->width >= $this->maxWidth && $this->height <= $this->maxHeight) {
            if ($this->allowCrop) {
                $y = round(($this->maxHeight - $this->height) / 2);
            } else {
                $this->newHeight = $this->height * $ratioLarg;
            }
        }
        elseif ($this->width <= $this->maxWidth && $this->height >= $this->maxHeight) {
            if ($this->allowCrop) {
                $x = round(($this->maxWidth - $this->width) / 2);
            } else {
                $this->newWidth = $this->width * $ratioHaut;
            }
        }
        elseif ($this->width <= $this->maxWidth && $this->height <= $this->maxHeight) {
            $this->newWidth = $this->width;
            $this->newHeight = $this->height;
            $x = round(($this->width - $this->maxWidth) / 2);
            $y = round(($this->height - $this->maxHeight) / 2);
        }
        
        $this->x         = $x;
        $this->y         = $y;
    }
    
    
    
    /**
     * @throws \Exception
     */
    private function processResize() {
        try {
            $destination = imagecreatetruecolor($this->newWidth, $this->newHeight);
            
            if ($destination === false) {
                throw new \Exception();
            }
            
            imagesavealpha($destination, true);
            $mime        = $this->file->getMimeType();
            
            if ($mime == 'image/png' ) {
                $source = imagecreatefrompng($this->file->getPathName());
            } elseif ($mime == 'image/jpg' || $mime == 'image/jpeg' ) {
                $source = imagecreatefromjpeg($this->file->getPathName());
            } elseif ($mime == 'image/gif' ) {
                $source = imagecreatefromgif($this->file->getPathName());
            } else {
                throw new \Exception("Unsupported media type.");
            }
            
            imagealphablending($destination, false);
            $transparencyIndex = imagecolortransparent($source);
            $transparencyColor = array('red' => 255, 'green' => 255, 'blue' => 255);
            
            if ($transparencyIndex >= 0) {
                throw new UnsupportedMediaType();
                // TODO Find an elegant way to handle transparent images
                //                $transparencyColor    = imagecolorsforindex($source, $transparencyIndex);
            }
            
            $transparencyIndex    = imagecolorallocate(
                $destination,
                $transparencyColor['red'],
                $transparencyColor['green'],
                $transparencyColor['blue']
            );
            imagefill($destination, 0, 0, $transparencyIndex);
            imagecolortransparent($destination, $transparencyIndex);
            
            $copied = imagecopyresampled(
                $destination,
                $source,
                0,
                0,
                0,
                0,
                $this->newWidth,
                $this->newHeight,
                $this->width,
                $this->height
            );
            
            if( $copied === false ) {
                throw new \Exception();
            }
            
            $destination = imagecrop($destination, [
                'x'         => $this->x,
                'y'         => $this->y,
                'width'     => $this->newWidth,
                'height'    => $this->newHeight
            ]);
            
            if( $destination === false ) {
                throw new \Exception();
            }
            
            imagedestroy($source);
            
            $this->newWidth  = imagesx($destination);
            $this->newHeight = imagesy($destination);
            $this->destination = $destination;
        } catch (\Exception $e) {
            throw new \Exception("Unable to resize source image");
        }
    }
    
    
    
    /**
     * @return mixed
     */
    public function getResizedImageWebPath()
    {
        return str_replace($this->projectDir.'/web', '', $this->getResizedImagePath());
    }
}
