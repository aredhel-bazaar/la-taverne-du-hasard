<?php

namespace AppBundle\Form;

use AppBundle\Entity\Question;
use AppBundle\Entity\Survey;
use AppBundle\Extensions\Form\Type\CalendarType;
use JMS\TranslationBundle\Model\Message;
use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;

class QuestionForm extends AbstractType implements TranslationContainerInterface
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->setAttributes([
                'class' => 'js-question question'
            ])
            ->add('label', TextType::class, [
                'label' => 'question.form.label.label',
                'constraints' => [
                    new NotBlank()
                ]
            ])
            ->add('recordOrder', HiddenType::class, [])
            ->add('answers', CollectionType::class, [
                'entry_type'   => AnswerForm::class,
                'allow_add'    => true,
                'allow_delete' => true,
                'label'        => 'answer.form.label',
                'attr'         => [
                    'class' => 'js-answers-container answers-container',
                    'add_label'    => 'answer.add.label',
                ],
                'prototype_name' => '__answer_index__'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Question::class,
            'label_attr'   => [
                'class' => 'hidden'
            ]
        ]);
    }
    
    
    
    /**
     * Returns an array of messages.
     *
     * @return array<Message>
     */
    public static function getTranslationMessages()
    {
        return [
            new Message('answer.add.label'),
        ];
    }
}
