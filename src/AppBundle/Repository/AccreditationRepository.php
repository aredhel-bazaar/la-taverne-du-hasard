<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Accreditation;
use AppBundle\Entity\Role;
use Doctrine\ORM\EntityRepository;

class AccreditationRepository extends BaseRepository
{
    public function getRightsHierarchy()
    {
        $rights = [];
        $roles  = $this->getEntityManager()->getRepository('AppBundle:Role')
            ->findAll();
    
        /** @var Role $role */
        foreach ($roles as $role) {
            foreach (Accreditation::TYPES as $type) {
                $rights[$role->getRole()][$type] = false;
            }
        }
        
        return $rights;
    }
}