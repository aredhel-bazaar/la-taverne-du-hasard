<?php

namespace AppBundle\Extensions\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormView;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CalendarType extends AbstractType {
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'ranged' => false
        ]);
    }
    
    
    
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $formBuilder
     * @param array                                        $options
     */
    public function buildForm(FormBuilderInterface $formBuilder, array $options)
    {
        $formBuilder
            ->addModelTransformer(new CallbackTransformer(
                function (\DateTime $startDateTime=null) {
                    // transform the DateTime to a string
                    return (!is_null($startDateTime) ? $startDateTime->format('d/m/Y H:i') : null) ;
                },
                function ($startDateString) {
                    // transform the string back to a DateTime
                    return (!is_null($startDateString) ? \DateTime::createFromFormat('d/m/Y H:i', $startDateString) : null);
                }
            ))
            ->addViewTransformer(new CallbackTransformer(
                function($var) {
                    return $var;
                },
                function($var) {
                    return $var;
                }
            ))
        ;
    }
    
    
    
    public function getParent() {
        return TextType::class;
    }
}