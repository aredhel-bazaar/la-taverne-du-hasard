<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 08/10/2016
 * Time: 00:15
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GameRepository")
 * @ORM\Table(name="game", indexes={})
 * @ORM\Cache()
 */
class Game
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=5)
     */
    private $type;
    
    /**
     * @var string
     *
     * @ORM\Column(name="image_cover", type="string", length=250)
     */
    private $imageCover;
    
    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text")
     */
    private $description;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="min_players", type="integer", length=2)
     */
    private $minPlayers;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="max_players", type="integer", length=2)
     */
    private $maxPlayers;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="duration", type="time")
     */
    private $duration;
    
    /**
     * @var string
     *
     * @ORM\Column(name="website", type="string", length=250)
     */
    private $website;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private $status;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    
    
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    
    
    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }
    
    
    
    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }
    
    
    
    /**
     * @param string $type
     */
    public function setType(string $type)
    {
        $this->type = $type;
    }
    
    
    
    /**
     * @return string
     */
    public function getImageCover(): string
    {
        return $this->imageCover;
    }
    
    
    
    /**
     * @param string $imageCover
     */
    public function setImageCover(string $imageCover)
    {
        $this->imageCover = $imageCover;
    }
    
    
    
    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }
    
    
    
    /**
     * @param string $description
     */
    public function setDescription(string $description)
    {
        $this->description = $description;
    }
    
    
    
    /**
     * @return int
     */
    public function getMinPlayers(): int
    {
        return $this->minPlayers;
    }
    
    
    
    /**
     * @param int $minPlayers
     */
    public function setMinPlayers(int $minPlayers)
    {
        $this->minPlayers = $minPlayers;
    }
    
    
    
    /**
     * @return int
     */
    public function getMaxPlayers(): int
    {
        return $this->maxPlayers;
    }
    
    
    
    /**
     * @param int $maxPlayers
     */
    public function setMaxPlayers(int $maxPlayers)
    {
        $this->maxPlayers = $maxPlayers;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getDuration(): \DateTime
    {
        return $this->duration;
    }
    
    
    
    /**
     * @param \DateTime $duration
     */
    public function setDuration(\DateTime $duration)
    {
        $this->duration = $duration;
    }
    
    
    
    /**
     * @return string
     */
    public function getWebsite(): string
    {
        return $this->website;
    }
    
    
    
    /**
     * @param string $website
     */
    public function setWebsite(string $website)
    {
        $this->website = $website;
    }
    
    
    
    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }
    
    
    
    /**
     * @param int $status
     */
    public function setStatus(int $status)
    {
        $this->status = $status;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     */
    public function setModificationDate(\DateTime $modificationDate)
    {
        $this->modificationDate = $modificationDate;
    }
}