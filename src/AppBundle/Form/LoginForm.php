<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LoginForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', TextType::class, [
                'label'         => 'connexion.label.email',
                'label_attr'    => array('class' => 'sr-only'),
                'attr'          => array('placeholder' => 'connexion.label.email')
            ])
            ->add('password', PasswordType::class, [
                'label'         => 'connexion.label.password',
                'label_attr'    => array('class' => 'sr-only'),
                'attr'          => array('placeholder' => 'connexion.label.password')
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => User::class,
            'validation_groups' => ['Default', 'Registration']
        ));
    }
}
