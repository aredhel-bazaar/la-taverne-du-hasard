<?php

namespace AppBundle\EventListener;

use AppBundle\Controller\BaseController;
use Psr\Container\NotFoundExceptionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Firewall custom. Écoute les requêtes et s'assure que l'utilisate à le droit d'accéder à la page qu'il demande
 * Si les conditions ne sont pas remplies, on retourne un JSON d'erreur.
 *
 * Class ExceptionListener
 * @package AppBundle\EventListener
 */
class ExceptionListener
{
    /**
     * @var AuthorizationCheckerInterface
     */
    private $securityContext;

    /**
     * @var object|\Symfony\Component\Translation\IdentityTranslator
     */
    private $translator;

    /**
     * @var RouterInterface
     */
    private $router;

    /**
     * @var RequestStack
     */
    private $requestStack;

    public function __construct(ContainerInterface $container, RouterInterface $router, RequestStack $requestStack)
    {
        $this->securityContext = $container->get('security.authorization_checker');
        $this->translator      = $container->get('translator');
        $this->router          = $router;
        $this->requestStack    = $requestStack;
    }

    /**
     * Appelé en cas d'exception. Nous ne gérons ici que le cas où l'accès à une ressource a été refusée.
     *
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event)
    {
        $exception = $event->getException();

        if( $exception instanceof AccessDeniedException ) {
            if( $this->securityContext->isGranted('IS_AUTHENTICATED_FULLY') ) {
                if( $this->requestStack->getCurrentRequest()->isXmlHttpRequest() ) {
                    $response = BaseController::outputJSON(null, $this->translator->trans('core.exception.forbidden_access'), Response::HTTP_FORBIDDEN);
                } else {
                    $response = new RedirectResponse($this->router->generate('homepage'));
                }
            } else {
                if( $this->requestStack->getCurrentRequest()->isXmlHttpRequest() ) {
                    $response = BaseController::outputJSON(null, $this->translator->trans('core.exception.unauthorized_access'), Response::HTTP_UNAUTHORIZED);
                } else {
                    $response = new RedirectResponse($this->router->generate('homepage'));
                }
            }
        }
        
        if ($exception instanceof NotFoundHttpException) {
            $response = new RedirectResponse(
                $this->router->generate('homepage')
            );
        }
        
        if (isset($response)) {
            $event->setResponse($response);
        }
    }
}
