(function() {
    "use strict";

    $('.js-section-rank input').change(function () {
        var $this = $(this);
        var value = $this.val();
        var $elem = $this.parents('.ui.checkbox');

        if ($elem.hasClass('checked')) {
            $('.js-section-rank-input').val(value);
        }
    });

    $('.js-toggle-permissions').click(function (e) {
        e.preventDefault();

        var $this = $(this);

        $('#' + $this.data('id')).slideToggle();
    });

    $('.js-toggle-archives').click(function(e) {
        var $this = $(this);

        if ($this.is(':checked')) {
            $('.archived.element').fadeIn(function() {
                $(this).css('display', 'flex');
            });
        } else {
            $('.archived.element').fadeOut();
        }
    });
})();
