<?php

namespace AppBundle\Extensions\Entity;

interface PermissionInterface
{
    public function getAccessRights();
}