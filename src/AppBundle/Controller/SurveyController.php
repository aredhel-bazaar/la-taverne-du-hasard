<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Answer;
use AppBundle\Entity\Survey;
use AppBundle\Entity\UserAnswer;
use AppBundle\Form\SurveyForm;
use DateTime;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SurveyController
 * @package AppBundle\Controller
 *
 * @Route("/sondage")
 */
class SurveyController extends BaseController
{
    /**
     * --------------------*
     * @Route("s/", name="app_survey_list")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(EntityManager $em, Request $request)
    {
        $page    = $request->get('page', 1);
        $surveys = $em->getRepository('AppBundle:Survey')
            ->findBy([], ['creationDate' => 'DESC'], Survey::MAX_PER_PAGE, ($page-1)*Survey::MAX_PER_PAGE);
        
        return $this->render('surveys/list.html.twig', [
            'surveys'  => $surveys,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("s/inserer", name="app_survey_select", options={"expose"="true"})
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function insertAction(EntityManager $em, Request $request)
    {
        $page    = $request->get('page', 1);
        $surveys = $em->getRepository('AppBundle:Survey')
            ->findBy([], ['creationDate' => 'DESC'], Survey::MAX_PER_PAGE, ($page-1)*Survey::MAX_PER_PAGE);
        
        return $this->render('surveys/modal_list.html.twig', [
            'surveys'  => $surveys,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/nouveau")
     * @Route("/éditer/{id}")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     * @param int|null      $id
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createEditAction(EntityManager $em, Request $request, int $id = null)
    {
        if (isset($id)) {
            $survey = $em->getRepository('AppBundle:Survey')->find($id);
        } else {
            $survey = new Survey();
        }
        
        $form = $this->createForm(SurveyForm::class, $survey);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                if (is_null($survey->getId())) {
                    $em->persist($survey);
                }
                
                $em->flush();
                
                return $this->redirectToRoute('app_admin_index');
            }
        }
        
        return $this->render('surveys/create_edit.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/voir/{id}.{_format}", defaults={"_format"="html"}, name="app_survey_show")
     * @Method({"GET"})
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     * @param int|null      $id
     *
     * @return Response
     */
    public function showAction(EntityManager $em, Request $request, $id)
    {
        /** @var Survey $survey */
        $survey = $em->getRepository('AppBundle:Survey')->get($id);
        $user_answer = [];
        $nb_total    = 0;
        
        if (empty($survey)) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }
        
        //-- On récupère les réponses (si il y a) déjà données pour ce sondage
        $userAnswers = $em->getRepository('AppBundle:UserAnswer')->findBy([
            'fkUser'   => $this->getUser(),
            'fkAnswer' => $survey->getAnswers()
        ]);
        $user_answers_id = array_map(function($userAnswer) {
            /** @var UserAnswer $userAnswer */
            return $userAnswer->getFkAnswer()->getId();
        }, $userAnswers);
    
        //-- On choisi le type d'affichage pour le sondage
        $default_view = 'form';
        if (!empty($user_answers_id) || $this->isGranted('ROLE_ADMIN')) {
            $default_view = 'results';
        }
        $view_mode = ($survey->getClosureDate() < new DateTime() ? 'results' : $request->get('view', $default_view));
        //--
    
        if ($view_mode == 'results') {
            $user_answer = $em->getRepository('AppBundle:UserAnswer')
                ->getResults($survey)
            ;
        }
    
        if ($request->get('_format') == 'json') {
            return $this->outputJSON($this->renderView('surveys/show_'.$view_mode.'.html.twig', [
                'survey'          => $survey,
                'user_answer'     => $user_answer,
                'user_answers_id' => $user_answers_id,
            ]));
        }
    
        return $this->render('surveys/show_'.$view_mode.'.html.twig', [
            'survey'          => $survey,
            'user_answer'     => $user_answer,
            'nb_total'        => $nb_total,
            'user_answers_id' => $user_answers_id,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/soumettre/{id}")
     * @Method({"POST"})
     * --------------------*
     *
     * @param TranslatorInterface $translator
     * @param Request             $request
     * @param EntityManager       $em
     * @param int|null            $id
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function submitAction(TranslatorInterface $translator, Request $request, EntityManager $em, $id)
    {
        /** @var Survey $survey */
        $survey = $em->getRepository('AppBundle:Survey')->get($id);
        $answers = array_values($request->request->all());
    
        //-- Le sondage n'existe pas, ou bien sa date est dépassée
        if (empty($survey) || $survey->getClosureDate() < new DateTime()) {
            return new Response('', Response::HTTP_NOT_FOUND);
        }
        //--
        
        //-- On récupère les réponses (si il y a) déjà données pour ce sondage et on les supprime
        $userAnswers = $em->getRepository('AppBundle:UserAnswer')->findBy([
            'fkUser'   => $this->getUser(),
            'fkAnswer' => $survey->getAnswers()
        ]);
    
        array_map(function($userAnswer) use ($em) {
            $em->remove($userAnswer);
        }, $userAnswers);
        
        $em->flush();
        //--
    
        //-- On enregistre les réponses données
        $answers = $em->getRepository('AppBundle:Answer')->findBy(['id' => $answers]);
        /** @var Answer $answer */
        foreach ($answers as $answer) {
            $userAnswer = new UserAnswer();
            $userAnswer->setFkUser($this->getUser())
                ->setFkAnswer($answer);
            
            $em->persist($userAnswer);
        }
        
        $em->flush();
        //--
    
        return $this->outputJSON($this->forward('AppBundle:Survey:show', ['id' => $survey->getId()])->getContent(), $translator->trans('survey.save.success'));
    }
}
