<?php

namespace AppBundle\Service;

use Symfony\Component\HttpFoundation\File\File;

class ImageManipulator {
    /**
     * @var int
     */
    private $maxHeight;
    /**
     * @var int
     */
    private $maxWidth;
    /**
     * @var string
     */
    private $projectDir;
    
    
    
    /**
     * ImageManipulator constructor.
     *
     * @param int    $maxHeight
     * @param int    $maxWidth
     * @param string $projectDir
     */
    public function __construct(int $maxHeight, int $maxWidth, string $projectDir)
    {
        $this->maxHeight  = $maxHeight;
        $this->maxWidth   = $maxWidth;
        $this->projectDir = $projectDir;
    }
    
    
    
    /**
     * @param File        $file
     * @param string|null $filename
     *
     * @return Manipulator
     */
    public function init(File $file, string $filename = null)
    {
        return new Manipulator($this->maxHeight, $this->maxWidth, $this->projectDir, $file, $filename);
    }
}
