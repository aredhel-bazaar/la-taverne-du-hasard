<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Form\LoginForm;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Cache;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class SecurityController
 * @package AppBundle\Controller
 *
 * @Cache(maxage=0)
 */
class SecurityController extends BaseController
{
    /**
     * Page de connexion appelée via twig
     *
     * --------------------*
     * @Route("/connexion", name="security_login")
     * @Method({"GET","POST"})
     * @Security("!is_granted('IS_AUTHENTICATED_FULLY')")
     * --------------------*
     *
     * @param TranslatorInterface $translator
     * @param AuthenticationUtils $authenticationUtils
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param Request $request
     */

    public function loginAction(TranslatorInterface $translator, AuthenticationUtils $authenticationUtils)
    {
        //-- Récupération de l'erreur si elle existe
        $error = $authenticationUtils->getLastAuthenticationError();

        //-- Récupération du dernier login tapé par l'utilisateur
        $lastUsername = $authenticationUtils->getLastUsername();

        $user = new User();
        $user->setEmail($lastUsername);

        $Form = $this->createForm(LoginForm::class, $user, array(
            'action'    => $this->generateUrl('security_login'),
            'attr'      => ( !empty($error) ? array('class' => 'error') : array() )
        ));

        if ( !empty($error)){
            $Form->addError(new FormError(
                /** @Ignore */
                $translator->trans($error->getMessageKey(), $error->getMessageData(), 'security')
            ));
        }

        return $this->render(
            'security/login.html.twig',
            array(
                'form'          => $Form->createView(),
                'nosidebar'     => true
            )
        );
    }

    /**
     * Détecté et géré automatiquement par Symfony. Cette méthode ne fait rien mais doit exister sans quoi l'utilisateur
     * verra une page 404.
     *
     * --------------------*
     * @Route("/logout", name="security_logout")
     * @Method("GET")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     * --------------------*
     */
    public function logoutAction()
    {
        throw new \Exception('core.exception.method_should_not_be_reached');
    }
}
