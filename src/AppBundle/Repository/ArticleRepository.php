<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Article;
use Doctrine\ORM\EntityRepository;

/**
 * ActivityLogRepository
 *
 * This class was generated by the PhpStorm "Php Annotations" Plugin. Add your own custom
 * repository methods below.
 */
class ArticleRepository extends BaseRepository
{
    public function findAll()
    {
        list($order_by, $limit, $offset) = func_get_args();
        
        $queryBuilder = $this->createQueryBuilder('article')
            ->select('article', 'fkUser')
            ->join('article.fkUser', 'fkUser')
            ->where('article.status = :status')
            ->setParameter('status', Article::STATUS_VISIBLE);
        
        if( !empty($order_by) ) {
            if( !is_array($order_by) ) {
                $order_by = [$order_by];
            }
            
            foreach( $order_by as $column => $way ) {
                $queryBuilder->addOrderBy('article.'.$column, $way);
            }
        }
        
        if( !empty($limit) ) {
            $queryBuilder->setMaxResults($limit);
        }
        
        if( !empty($offset) ) {
            $queryBuilder->setFirstResult($offset);
        }
        
        return $queryBuilder->getQuery()->getArrayResult();
    }
}
