<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170618124716 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE private_conversation (id INT AUTO_INCREMENT NOT NULL, fk_user_id INT DEFAULT NULL, title VARCHAR(100) NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_DCF38EEB5741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE private_conversation_user (id INT AUTO_INCREMENT NOT NULL, fk_user_id INT DEFAULT NULL, fk_private_conversation_id INT DEFAULT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_183BD60E5741EEB9 (fk_user_id), INDEX IDX_183BD60E4398021B (fk_private_conversation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE private_message (id INT AUTO_INCREMENT NOT NULL, fk_user_id INT DEFAULT NULL, fk_private_conversation_id INT DEFAULT NULL, message LONGTEXT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_4744FC9B5741EEB9 (fk_user_id), INDEX IDX_4744FC9B4398021B (fk_private_conversation_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE private_conversation ADD CONSTRAINT FK_DCF38EEB5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE private_conversation_user ADD CONSTRAINT FK_183BD60E5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE private_conversation_user ADD CONSTRAINT FK_183BD60E4398021B FOREIGN KEY (fk_private_conversation_id) REFERENCES private_conversation (id)');
        $this->addSql('ALTER TABLE private_message ADD CONSTRAINT FK_4744FC9B5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE private_message ADD CONSTRAINT FK_4744FC9B4398021B FOREIGN KEY (fk_private_conversation_id) REFERENCES private_conversation (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE private_conversation_user DROP FOREIGN KEY FK_183BD60E4398021B');
        $this->addSql('ALTER TABLE private_message DROP FOREIGN KEY FK_4744FC9B4398021B');
        $this->addSql('DROP TABLE private_conversation');
        $this->addSql('DROP TABLE private_conversation_user');
        $this->addSql('DROP TABLE private_message');
    }
}
