<?php

namespace AppBundle\Command;

use AppBundle\Service\Notifier;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Cache\Adapter\AdapterInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendEmailsCommand extends ContainerAwareCommand
{
    /**
     * @var AdapterInterface
     */
    private $cacheAdapter;
    /**
     * @var Notifier
     */
    private $notifierService;
    
    
    
    public function configure()
    {
        $this
            // the name of the command (the part after "bin/console")
            ->setName('app:email:send')
            // the short description shown while running "php bin/console list"
            ->setDescription('Send emails still in the queue');
    }
    
    
    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->cacheAdapter    = $this->getContainer()->get('cache.app');
        $this->notifierService = $this->getContainer()->get('AppBundle\Service\Notifier');
    }
    
    
    public function execute(InputInterface $input, OutputInterface $output)
    {
        $emailQueue = $this->cacheAdapter->getItem(Notifier::CACHE_KEY)->get();
        
        if (!empty($emailQueue)) {
            $notification = array_pop($emailQueue);
            $this->notifierService->notify($notification['case'], $notification['object_id'], false);
        }
    
        $this->cacheAdapter->getItem(Notifier::CACHE_KEY)->set($emailQueue);
    }
}
