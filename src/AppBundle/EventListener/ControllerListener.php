<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class ControllerListener {
    /**
     * @var null|User
     */
    private $user;
    private $request;
    private $router;
    
    
    
    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router, RequestStack $requestStack)
    {
        $this->user    = (!is_null( $tokenStorage->getToken()) ? $tokenStorage->getToken()->getUser() : null);
        $this->router  = $router;
        $this->request = $requestStack->getCurrentRequest();
    }
    
    
    
    public function onKernelController(FilterControllerEvent $controllerEvent)
    {
        if (!is_null($this->user) && is_object($this->user) && $controllerEvent->isMasterRequest() &&
            $this->request->get('_route') !== 'app_utilisateur_useredit' && !$this->request->isMethod('POST')
        ) {
            if (empty($this->user->getFirstName()) || empty($this->user->getLastName())) {
                $redirectUrl = $this->router->generate('app_utilisateur_useredit', ['nickname' => $this->user->getNickname()]);
                $controllerEvent->setController(function() use ($redirectUrl) {
                    return new RedirectResponse($redirectUrl);
                });
            }
        }
    }
}