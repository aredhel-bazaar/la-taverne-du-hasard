(function() {
    "use strict";

    /**
     * Global variables
     */


    /**
     * JQuery Extends
     */
    $.fn.extend({

    });

    $.extend({

    });

    $('.js-small-calendar')
        .on('click', 'a.event', function(e) {
            e.preventDefault();

            var $this            = $(this);
            var $eventContainer  = $('.js-event-container');
            var $dimmerContainer = $eventContainer.siblings('.dimmer');

            $dimmerContainer.dimmer('show');
            $.ajax({
                url:  $this.attr('href'),
                dataType: 'html'
            })
                .done(function(data) {
                    $dimmerContainer.dimmer('hide');
                    if( $eventContainer.parent().hasClass('center aligned middle aligned') ) {
                        $eventContainer.parent().removeClass('center aligned middle aligned');
                    }
                    $eventContainer.html(data);
                    $.initSemanticComponents();
                    $eventContainer.attr('data-referer', $this.attr('href'));
                })
            ;
        })
        .on('click', '.js-navigation a', function(e) {
            e.preventDefault();

            var $this = $(this);
            var $calendarContainer = $('.js-small-calendar');
            var $dimmerContainer = $calendarContainer.siblings('.dimmer');

            $dimmerContainer.dimmer('show');

            $.ajax({
                url: $this.attr('href'),
                dataType: 'html'
            })
                .done(function(data) {
                    $dimmerContainer.dimmer('hide');
                    $calendarContainer.html(data)
                })
        });

    $('body')
        .on('click', '.js-event-action', function(e) {
            e.preventDefault();

            var $this = $(this);
            var calendarReferer = $('.js-event-container').data('referer');

            if( $this.hasClass('loading') ) {
                return;
            }

            $this.toggleClass('loading');

            $.ajax({
                url: $this.attr('href'),
                dataType: 'json'
            })
                .done(function() {
                    $('.small-calendar a[href="' + calendarReferer + '"]').click();
                })
                .always(function() {
                    $this.toggleClass('loading');
                })
            ;
        });
})();
