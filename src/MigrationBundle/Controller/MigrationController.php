<?php

namespace MigrationBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use AppBundle\Entity\Event;
use AppBundle\Entity\Preference;
use AppBundle\Entity\Role;
use AppBundle\Entity\Subscriber;
use AppBundle\Entity\User;
use Doctrine\DBAL\Driver\Mysqli\MysqliConnection;
use MigrationBundle\Entity\Commentaire;
use MigrationBundle\Entity\EventMembre;
use MigrationBundle\Entity\Membre;
use MigrationBundle\Entity\News;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

/**
 * --------------------*
 * @Route("/migrations")
 * --------------------*
 */
class MigrationController extends Controller
{
    /**
     * --------------------*
     * @Route("/migrate", name="migrate_data")
     * --------------------*
     */
    public function indexAction()
    {
        //-- START : Getting v1 data
        $v1Cnx = new MysqliConnection(
            [
                'host'   => 'localhost',
                'dbname' => 'association',
                'charset'=> 'utf8'
            ],
            'root',
            'W7PaTTBci7'
        );
        $handler = $v1Cnx->getWrappedResourceHandle();
        
        $membreQuery        = $handler->query("SELECT * FROM membres");
        $newsQuery          = $handler->query("SELECT * FROM news");
        $eventQuery         = $handler->query("SELECT * FROM events");
        $commentaireQuery   = $handler->query("SELECT * FROM commentaire");
        $eventMemberQuery   = $handler->query("SELECT * FROM evenement_membres");
        //-- END : Getting v1 data
        
        //-- START : Init v2 connection
        $doctrine   = $this->getDoctrine();
        $em         = $doctrine->getManager();
        $connection = $doctrine->getConnection();
        $dbPlatform = $connection->getDatabasePlatform();
        //-- END : Init v2 connection
    
        //-- START : Empty all tables
        $connection->query('SET FOREIGN_KEY_CHECKS=0');
        
        //-- User table
        $cmd = $em->getClassMetadata(User::class);
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        //--
        
        //-- Article table
        $cmd = $em->getClassMetadata(Article::class);
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        //--
        
        //-- Event table
        $cmd = $em->getClassMetadata(Event::class);
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        //--
        
        //-- Comment table
        $cmd = $em->getClassMetadata(Comment::class);
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        //--
        
        //-- Subscriber table
        $cmd = $em->getClassMetadata(Subscriber::class);
        $q = $dbPlatform->getTruncateTableSql($cmd->getTableName());
        $connection->executeUpdate($q);
        //--
        
        $connection->query('SET FOREIGN_KEY_CHECKS=1');
        //-- END : Empty all tables
        
        $rawRoles = $em->getRepository('AppBundle:Role')->findAll();
        $roles    = [];
        foreach ($rawRoles as $role) {
            $roles[$role->getRole()] = $role;
        }
        
        $connection->beginTransaction();
    
        try {
            $membres = [];
            /** @var Membre $membre */
            while ($membre = $membreQuery->fetch_object(Membre::class)) {
                $user      = new User();
        
                $membre->convertToV2($user, $roles);
                $em->persist($user);
                $em->flush();
        
                $membres[$membre->membre_id]['v1'] = $membre;
                $membres[$membre->membre_id]['v2'] = $user;
            }
    
            $events = [];
            /** @var \MigrationBundle\Entity\Event $event */
            while ($event = $eventQuery->fetch_object(\MigrationBundle\Entity\Event::class)) {
                $v2Event = new Event();
        
                $event->convertToV2($v2Event, $membres);
                $em->persist($v2Event);
                $em->flush();
        
                $events[$event->event_id]['v1'] = $event;
                $events[$event->event_id]['v2'] = $v2Event;
            }
    
            $news = [];
            /** @var News $v1Article */
            while ($v1Article = $newsQuery->fetch_object(News::class)) {
                $v2Article = new Article();
        
                $v1Article->convertToV2($v2Article, $membres);
                $em->persist($v2Article);
                $em->flush();
        
                $news[$v1Article->news_id]['v1'] = $v1Article;
                $news[$v1Article->news_id]['v2'] = $v2Article;
            }
            
            /** @var Commentaire $v1Comment */
            while ($v1Comment = $commentaireQuery->fetch_object(Commentaire::class)) {
                $v2Comment = new Comment();
                
                $v1Comment->convertToV2($v2Comment, $membres, $events, $news);
                $em->persist($v2Comment);
                $em->flush();
            }
            
            /** @var EventMembre $v1Subscriber */
            while ($v1Subscriber = $eventMemberQuery->fetch_object(EventMembre::class)) {
                $v2Subscriber = new Subscriber();
    
                $v1Subscriber->convertToV2($v2Subscriber, $membres, $events);
                $em->persist($v2Subscriber);
                $em->flush();
            }
            
        } catch (\Exception $e) {
            $connection->rollback();
    
            echo '<pre>';
            echo $e->__toString();
            echo '</pre>';
            
            exit;
        }
    
        $connection->commit();
        
        die('Yeah !!');
    }
}
