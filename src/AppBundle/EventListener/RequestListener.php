<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\User;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class RequestListener {
    /**
     * @var null|User
     */
    private $user;
    private $request;
    private $router;
    
    
    
    public function __construct(TokenStorageInterface $tokenStorage, RouterInterface $router, RequestStack $requestStack)
    {
        $this->user    = (!is_null( $tokenStorage->getToken()) ? $tokenStorage->getToken()->getUser() : null);
        $this->router  = $router;
        $this->request = $requestStack->getCurrentRequest();
    }
    
    
    
    public function onKernelRequest(GetResponseEvent &$responseEvent)
    {
        if (!is_null($this->user) && is_object($this->user) && $responseEvent->isMasterRequest() && !in_array($this->request->get('_route'), ['app_utilisateur_useredit', 'app_utilisateur_usereditprofile'])) {
            if (empty($this->user->getFirstName()) || empty($this->user->getLastName())) {
                $response = new RedirectResponse($this->router->generate('app_utilisateur_useredit', ['nickname' => $this->user->getNickname()]));
                $responseEvent->setResponse($response);
            }
        }
    }
}