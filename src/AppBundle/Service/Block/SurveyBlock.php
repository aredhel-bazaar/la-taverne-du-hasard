<?php

namespace AppBundle\Service\Block;

use AppBundle\Entity\Role;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class SurveyBlock extends AbstractBlock
{
    const MIN_ROLE   = Role::ROLE_MODERATOR;
    const ROUTE_NAME = 'app_survey_show';
    
    public function __construct(AuthorizationCheckerInterface $authorizationChecker)
    {
        $this->authorizationChecker = $authorizationChecker;
    }
    
}