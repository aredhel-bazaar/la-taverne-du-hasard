<?php
/**
 * Created by PhpStorm.
 * User: Aredhel
 * Date: 18/06/2017
 * Time: 10:49
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrivateMessageRepository")
 * @ORM\Table(name="private_message", indexes={})
 * @ORM\Cache()
 */
class PrivateMessage
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="PrivateConversation", inversedBy="messages")
     * @ORM\JoinColumn(name="fk_private_conversation_id", referencedColumnName="id")
     */
    private $fkPrivateConversation;
    
    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return PrivateMessage
     */
    public function setId(int $id): PrivateMessage
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return User
     */
    public function getFkUser(): User
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param User $fkUser
     *
     * @return PrivateMessage
     */
    public function setFkUser(User $fkUser): PrivateMessage
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return User
     */
    public function getFkPrivateConversation(): User
    {
        return $this->fkPrivateConversation;
    }
    
    
    
    /**
     * @param PrivateConversation $fkPrivateConversation
     *
     * @return PrivateMessage
     */
    public function setFkPrivateConversation(PrivateConversation $fkPrivateConversation): PrivateMessage
    {
        $this->fkPrivateConversation = $fkPrivateConversation;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }
    
    
    
    /**
     * @param string $message
     *
     * @return PrivateMessage
     */
    public function setMessage(string $message): PrivateMessage
    {
        $this->message = $message;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return PrivateMessage
     */
    public function setCreationDate(\DateTime $creationDate): PrivateMessage
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return PrivateMessage
     */
    public function setModificationDate(\DateTime $modificationDate): PrivateMessage
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
}