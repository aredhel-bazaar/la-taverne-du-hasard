<?php

namespace AppBundle\Service;

use AppBundle\Entity\Accreditation;
use AppBundle\Entity\Role;
use AppBundle\Extensions\Entity\PermissionInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Persistence\ObjectManagerAware;

class TwigHelper extends \Twig_Extension
{
    /**
     * @var AccreditationProvider
     */
    private $accreditationProvider;
    /**
     * @var ObjectManagerAware
     */
    private $objectManager;
    /**
     * @var array
     */
    private $cache;
    
    
    
    /**
     * TwigHelper constructor.
     *
     * @param AccreditationProvider $accreditationProvider
     * @param ObjectManager         $objectManager
     */
    public function __construct(AccreditationProvider $accreditationProvider, ObjectManager $objectManager)
    {
        $this->accreditationProvider = $accreditationProvider;
        $this->objectManager         = $objectManager;
    }
    
    
    
    /**
     * @return array
     */
    public function getFunctions()
    {
        return [
            new \Twig_Function('is_accredited', [$this, 'isAccreditedTo']),
            new \Twig_Function('get_rights_hierarchy', [$this, 'getRightsHierarchy']),
        ];
    }
    
    
    
    /**
     * @param string              $action
     * @param PermissionInterface $item
     *
     * @return bool
     */
    public function isAccreditedTo(string $action, PermissionInterface $item)
    {
        return $this->accreditationProvider->can($action, $item);
    }
    
    
    
    public function getRightsHierarchy()
    {
        return $this->accreditationProvider->getRightsHierarchy();
    }
}