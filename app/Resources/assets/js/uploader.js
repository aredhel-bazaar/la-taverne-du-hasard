(function() {
    "use strict";

    /**
     * Global variables
     */
    var dropzoneContainer = $('.js-gallery-media-uploader');

    if (dropzoneContainer.length) {
        var galleryTmpId = $('[name="create_gallery_form[tmp_id]"]').val();

        dropzoneContainer.dropzone({
            url: Routing.generate('app_media_upload', {'gallery': galleryTmpId}),
            acceptedFiles: 'image/*',
            addRemoveLinks: true,
            resizeHeight: 180,
            init: function () {
                this.on('success', function (file, responseText) {
                    var $mediaListElm = $('.js-media-list');
                    var $mediaElm     = $($('.js-prototype')[0].outerHTML).removeClass('js-prototype prototype');

                    $mediaElm.attr('data-id', responseText.mediaId);
                    $mediaElm.attr('data-url', responseText.path);
                    $mediaElm.attr('data-name', responseText.name);
                    $mediaElm.attr('data-description', '');
                    $mediaElm.find('[type=hidden]').val(responseText.mediaId);
                    $mediaElm.find('.js-media-name').val(responseText.name);
                    $mediaElm.find('.js-media-description').val(responseText.description);

                    $mediaElm.append($($(file.previewElement)[0]).find('.dz-image')[0].innerHTML);
                    $mediaListElm.append($mediaElm);

                    this.removeFile(file);
                });
            },
            resize: function(file, width, height) {
                var fileWidth  = width || file.width;
                var fileHeight = height || file.height;

                height         = this.options.resizeHeight;
                width          = (fileHeight > height ? fileWidth * height / fileHeight : fileWidth);

                return {
                    srcWidth: fileWidth,
                    srcHeight: fileHeight,
                    trgWidth: width,
                    trgHeight: height,
                    srcX: 0,
                    srcY: 0,
                    trgX: 0,
                    trgY: 0
                };
            }
        });
    }
})();
