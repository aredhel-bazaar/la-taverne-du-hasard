CKEDITOR.plugins.add( 'survey', {
    icons: 'insertsurvey',

    init: function( editor ) {
        editor.addCommand('InsertSurveyCommand', {
            exec: function( editor ) {
                var modalElm = $('.js-editor-modal');

                modalElm
                    .modal({
                        blurring: true
                    })
                    .modal('show')
                ;

                $.ajax({
                    url: Routing.generate('app_survey_select', {inModal: true}),
                    dataType: 'html'
                })
                .done(function(data) {
                    modalElm.html(data);

                    modalElm.on('click', '.js-select-survey', function (e) {
                         e.preventDefault();

                         var surveyId = $(this).data('id');

                         editor.insertText('[[block]]type=survey;id='+surveyId+'[[endblock]]');

                        modalElm.modal('hide');
                    });
                });
            }
        });

        editor.ui.addButton('InsertSurvey', {
            label: "Insérer un sondage",
            command: "InsertSurveyCommand"
        });
    }
});