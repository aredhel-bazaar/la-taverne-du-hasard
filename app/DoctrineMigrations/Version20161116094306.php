<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161116094306 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE open_event (id INT AUTO_INCREMENT NOT NULL, fk_event_id INT DEFAULT NULL, fk_user_id INT DEFAULT NULL, open TINYINT(1) NOT NULL, date_time DATETIME NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_70132AA143DFAB55 (fk_event_id), INDEX IDX_70132AA15741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE open_event ADD CONSTRAINT FK_70132AA143DFAB55 FOREIGN KEY (fk_event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE open_event ADD CONSTRAINT FK_70132AA15741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE open_event');
    }
}
