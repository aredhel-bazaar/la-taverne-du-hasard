<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Media;
use AppBundle\Exception\UnsupportedMediaType;
use AppBundle\Service\ImageManipulator;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class MediaController
 * @package AppBundle\Controller
 *
 * --------------------*
 * @Route("/media")
 * --------------------*
 */
class MediaController extends BaseController
{
    /**
     * --------------------*
     * @Route("/voir/{media}", name="app_media_view")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $entityManager
     * @param Media         $media
     *
     * @return Response
     */
    public function viewAction(EntityManager $entityManager, Media $media)
    {
    
    }
    
    
    
    /**
     * --------------------*
     * @Route("/editer/{media}", name="app_media_edit", options={"expose"=true})
     * @Method({"GET", "POST"})
     * @Security("has_role('ROLE_ADMIN')")
     * --------------------*
     *
     * @param Request $request
     * @param EntityManager $entityManager
     * @param Media $media
     *
     * @throws \Doctrine\ORM\OptimisticLockException
     *
     * @return JsonResponse
     */
    public function editAction(Request $request, EntityManager $entityManager, Media $media)
    {
        if ($request->isMethod(Request::METHOD_POST)) {
            $formData         = $request->get('edit_media');
            $imageName        = $formData['name'];
            $imageDescription = $formData['description'];
            
            $media->setName($imageName)
                ->setDescription($imageDescription)
            ;
            
            $entityManager->flush();
            
            return $this->json([
                'name'        => $media->getName(),
                'description' => $media->getDescription(),
            ]);
        }
    }
    
    
    
    /**
     * --------------------*
     * @Route("/upload", name="app_media_upload", options={"expose"=true})
     * @Method("POST")
     * @Security("has_role('ROLE_ADMIN')")
     * --------------------*
     *
     * @param Request             $request
     * @param ImageManipulator    $imageManipulator
     * @param EntityManager       $entityManager
     * @param TranslatorInterface $translator
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function uploadAction(Request $request, ImageManipulator $imageManipulator, EntityManager $entityManager, TranslatorInterface $translator)
    {
        /** @var UploadedFile $file */
        $file          = $request->files->get('file');
        if ($request->get('gallery')) {
            $mediaBasePath = '/uploads/galleries/'.$request->get('gallery');
        } else {
            $mediaBasePath = '/uploads/standalone/'.date('Y/m');
        }
        
        $baseName = uniqid();
        
        try {
            //-- Thumbnail
            $thumbnailImage = $imageManipulator->init($file, $baseName.'_thumb.'.$file->getClientOriginalExtension())
                ->setMaxHeight(180)
                ->setMaxWidth(null)
                ->resize(false)
                ->move('web'.$mediaBasePath)
            ;
    
            //-- Original
            $fullsizedImage = $imageManipulator->init($file, $baseName.'.'.$file->getClientOriginalExtension())
                ->move('web'.$mediaBasePath)
            ;
        } catch (UnsupportedMediaType $e) {
            return $this->json(null, 400);
        }
        
        $media = new Media();
        $media->setThumbnailPath($thumbnailImage->getResizedImageWebPath())
            ->setPath($fullsizedImage->getResizedImageWebPath())
            ->setFkUser($this->getUser())
            ->setName(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME))
            ->setStatus(1)
        ;
        
        $entityManager->persist($media);
        $entityManager->flush();
    
        return $this->json([
            'mediaId'     => $media->getId(),
            'height'      => $thumbnailImage->getResizedImageHeight(),
            'width'       => $thumbnailImage->getResizedImageWidth(),
            'path'        => $media->getPath(),
            'name'        => $media->getName(),
            'description' => null,
        ]);
    }
}
