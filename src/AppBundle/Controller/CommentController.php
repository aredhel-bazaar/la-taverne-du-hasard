<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Event;
use AppBundle\Form\CommentForm;
use Doctrine\ORM\EntityManager;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class CommentController
 */
class CommentController extends BaseController
{
    /**
     * --------------------*
     * @Route("/event/{event}/commentaires")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Event         $event
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function eventCommentsAction(EntityManager $em, Event $event)
    {
        $commentRepository = $em->getRepository('AppBundle:Comment');
    
        $comment_list = $commentRepository->findBy(['fkEvent' => $event], ['creationDate' => 'DESC']);
        
        return $this->render('comment/comment_list.html.twig', [
            'comment_list' => $comment_list
        ]);
    }
    
    
    
    public function eventWriteCommentAction(Event $event, Request $request)
    {
        $comment = new Comment();
        $comment->setFkEvent($event)
            ->setFkUser($this->getUser());
        $form = $this->createForm(CommentForm::class, $comment);
    
        $form->handleRequest($request);
        if( $form->isSubmitted() ) {
            if( $form->isValid() ) {
            
            }
        }
    
        return $this->render('comment/comment_new.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
