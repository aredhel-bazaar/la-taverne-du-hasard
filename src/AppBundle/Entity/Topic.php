<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 19:41
 */

namespace AppBundle\Entity;

use AppBundle\Extensions\Entity\PermissionInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TopicRepository")
 * @ORM\Table(name="topic", indexes={})
 * @ORM\Cache()
 */
class Topic implements PermissionInterface
{
    const STATUS_OPEN    = 1;
    const STATUS_CLOSED  = 2;
    const STATUS_ARCHIVE = 3;
 
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Section
     *
     * @ORM\ManyToOne(targetEntity="Section", inversedBy="topics")
     * @ORM\JoinColumn(name="fk_section_id", referencedColumnName="id", nullable=true)
     */
    private $fkSection;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id", nullable=true)
     */
    private $fkUser;
    
    /**
     * @ORM\OneToMany(targetEntity="Message", mappedBy="fkTopic", cascade={"remove"})
     */
    private $messages;
    
    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=100)
     */
    private $title;
    
    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", length=100)
     */
    private $slug;
    
    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer", length=1)
     */
    private $status;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="pinned", type="boolean")
     */
    private $pinned;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="archived", type="boolean")
     */
    private $archived = false;
    
    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="Accreditation", mappedBy="fkTopic", cascade={"persist", "remove"}, fetch="EAGER")
     */
    private $rights;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="last_activity_date", type="datetime", nullable=true)
     */
    private $lastActivityDate;
    
    
    
    /**
     * Topic constructor.
     */
    public function __construct()
    {
        $this->messages = new ArrayCollection();
        $this->rights   = new ArrayCollection();
    }
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Topic
     */
    public function setId(int $id): Topic
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return Section
     */
    public function getFkSection()
    {
        return $this->fkSection;
    }
    
    
    
    /**
     * @param Section $fkSection
     *
     * @return Topic
     */
    public function setFkSection(Section $fkSection): Topic
    {
        $this->fkSection = $fkSection;
        
        return $this;
    }
    
    
    
    /**
     * @return User
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param User $fkUser
     *
     * @return Topic
     */
    public function setFkUser(User $fkUser): Topic
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @param bool $raw
     *
     * @return mixed
     */
    public function getRights($raw = false)
    {
        if ($raw) {
            $processedRights = [];
        
            /** @var Accreditation $right */
            foreach ($this->rights as $right) {
                $processedRights[$right->getFkRole()->getRole()][$right->getType()] = $right->hasAccess();
            }
        
            return new ArrayCollection($processedRights);
        }
    
        return $this->rights;
    }
    
    
    
    /**
     * @param mixed $rights
     *
     * @return Topic
     */
    public function setRights($rights)
    {
        $this->rights = $rights;
        
        return $this;
    }
    
    
    
    public function getAccessRights()
    {
        return $this->getRights(true)->toArray();
    }
    
    
    
    /**
     * @return ArrayCollection
     */
    public function getMessages()
    {
        return $this->messages;
    }
    
    
    
    public function addMessage(Message $message)
    {
        $this->messages[] = $message;
        
        return $this;
    }
    
    
    
    public function removeMessage(Message $message)
    {
        $this->messages->removeElement($message);
    }
    
    
    
    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }
    
    
    
    /**
     * @param string $slug
     *
     * @return Topic
     */
    public function setSlug(string $slug): Topic
    {
        $this->slug = $slug;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
    
    
    
    /**
     * @param string $title
     *
     * @return Topic
     */
    public function setTitle(string $title): Topic
    {
        $this->title = $title;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }
    
    
    
    /**
     * @param int $status
     *
     * @return Topic
     */
    public function setStatus(int $status): Topic
    {
        $this->status = $status;
        
        return $this;
    }
    
    
    
    /**
     * @return bool
     */
    public function isPinned()
    {
        return $this->pinned;
    }
    
    
    
    /**
     * @param bool $pinned
     *
     * @return Topic
     */
    public function setPinned(bool $pinned): Topic
    {
        $this->pinned = $pinned;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Topic
     */
    public function setCreationDate(\DateTime $creationDate): Topic
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Topic
     */
    public function setModificationDate(\DateTime $modificationDate): Topic
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return bool
     */
    public function isArchived(): bool
    {
        return $this->archived;
    }
    
    
    
    /**
     * @param bool $archived
     *
     * @return Topic
     */
    public function setArchived(bool $archived): Topic
    {
        $this->archived = $archived;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getLastActivityDate()
    {
        return $this->lastActivityDate;
    }
    
    
    
    /**
     * @param \DateTime $lastActivityDate
     *
     * @return Topic
     */
    public function setLastActivityDate(\DateTime $lastActivityDate): Topic
    {
        $this->lastActivityDate = $lastActivityDate;
        
        return $this;
    }
}