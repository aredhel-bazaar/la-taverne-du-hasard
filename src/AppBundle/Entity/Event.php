<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 19:41
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EventRepository")
 * @ORM\Table(name="event", indexes={})
 * @ORM\Cache()
 */
class Event
{
    const DATE_FORMAT_ICAL   = 'Ymd\THis\Z';
    
    const MAX_PER_ADMIN_PAGE = 20;
    
    const TYPE_OPEN_EVENT    = 'open_event';
    const TYPE_CAKE_EVENT    = 'cake_event';
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_organizer_id", referencedColumnName="id", nullable=true)
     */
    private $fkOrganizer;
    
    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=50, nullable=true)
     */
    private $type;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime")
     */
    private $startDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime")
     */
    private $endDate;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="places", type="integer", length=2)
     */
    private $places = 0;
    
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @ORM\OneToMany(targetEntity="Subscriber", mappedBy="fkEvent", cascade={"remove"}, fetch="EAGER")
     */
    private $subscribers;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    public function __construct()
    {
        $this->subscribers = new ArrayCollection();
    }
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id)
    {
        $this->id = $id;
    
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    
    
    
    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
    
        return $this;
    }
    
    
    
    /**
     * @return User
     */
    public function getFkOrganizer()
    {
        return $this->fkOrganizer;
    }
    
    
    
    /**
     * @param mixed $fkOrganizer
     *
     * @return $this
     */
    public function setFkOrganizer($fkOrganizer)
    {
        $this->fkOrganizer = $fkOrganizer;
    
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
    
    
    
    /**
     * @param \DateTime $startDate
     *
     * @return $this
     */
    public function setStartDate(\DateTime $startDate)
    {
        $this->startDate = $startDate;
    
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getEndDate()
    {
        return $this->endDate;
    }
    
    
    
    /**
     * @param \DateTime $endDate
     *
     * @return $this
     */
    public function setEndDate(\DateTime $endDate)
    {
        $this->endDate = $endDate;
    
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    
    
    /**
     * @param string $type
     *
     * @return Event
     */
    public function setType(string $type): Event
    {
        $this->type = $type;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getPlaces()
    {
        return $this->places;
    }
    
    
    
    /**
     * @param int $places
     *
     * @return $this
     */
    public function setPlaces(int $places)
    {
        $this->places = $places;
    
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    
    
    /**
     * @param string $content
     *
     * @return $this
     */
    public function setContent(string $content)
    {
        $this->content = $content;
    
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return $this
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
    
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return $this
     */
    public function setCreationDate(\DateTime $creationDate)
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return $this
     */
    public function setModificationDate(\DateTime $modificationDate)
    {
        $this->modificationDate = $modificationDate;
    
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getSubscribers()
    {
        return $this->subscribers;
    }
    
    
    
    public function addSubscriber(Subscriber $subscriber)
    {
        $subscriber->setFkEvent($this);
        
        $this->subscribers[] = $subscriber;
        
        return $this;
    }
    
    
    
    public function removeSubscriber(Subscriber $subscriber)
    {
        $this->subscribers->removeElement($subscriber);
    }
    
    
    
    /**
     * @param mixed $subscribers
     *
     * @return Event
     */
    public function setSubscribers($subscribers)
    {
        $this->subscribers = $subscribers;
        
        return $this;
    }
    
    
    
    public function isOpen()
    {
        $subscribers = $this->getSubscribers();
        
        $nb_subscribers = 0;
        /** @var Subscriber $subscriber */
        foreach( $subscribers as $subscriber ) {
            if( $subscriber->isPresent() ) {
                $nb_subscribers++;
            }
        }
        
        return (
            ($this->getPlaces() > $nb_subscribers || $this->getPlaces() == 0)
            &&
            $this->getStartDate() > new \DateTime()
        );
    }
    
    
    
    /**
     * Retourne le flux iCal pour l'évènement
     *
     * @return string
     */
    public function getICalFormat()
    {
        $iCal =  'BEGIN:VCALENDAR'."\n";
        $iCal .= 'VERSION:2.0'."\n";
        $iCal .= 'PRODID:-//lataverneduhasard/calendrier v1.0//FR'."\n";
        $iCal .= 'BEGIN:VEVENT'."\n";
        $iCal .= 'DTSTART:'.$this->getStartDate()->format(self::DATE_FORMAT_ICAL)."\n";
        $iCal .= 'DTEND:'.$this->getEndDate()->format(self::DATE_FORMAT_ICAL)."\n";
        $iCal .= 'SUMMARY:'.$this->getName()."\n";
        $iCal .= 'END:VEVENT'."\n";
        $iCal .= 'END:VCALENDAR'."\n";
        
        return $iCal;
    }
}