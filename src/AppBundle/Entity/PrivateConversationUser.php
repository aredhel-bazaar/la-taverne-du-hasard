<?php
/**
 * Created by PhpStorm.
 * User: Aredhel
 * Date: 18/06/2017
 * Time: 10:49
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrivateConversationUserRepository")
 * @ORM\Table(name="private_conversation_user", indexes={})
 * @ORM\Cache()
 */
class PrivateConversationUser
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @var PrivateConversation
     *
     * @ORM\ManyToOne(targetEntity="PrivateConversation", inversedBy="privateConversationUsers")
     * @ORM\JoinColumn(name="fk_private_conversation_id", referencedColumnName="id")
     */
    private $fkPrivateConversation;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return PrivateConversationUser
     */
    public function setId(int $id): PrivateConversationUser
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return User
     */
    public function getFkUser(): User
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param User $fkUser
     *
     * @return PrivateConversationUser
     */
    public function setFkUser(User $fkUser): PrivateConversationUser
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return PrivateConversation
     */
    public function getFkPrivateConversation(): PrivateConversation
    {
        return $this->fkPrivateConversation;
    }
    
    
    
    /**
     * @param PrivateConversation $fkPrivateConversation
     *
     * @return PrivateConversationUser
     */
    public function setFkPrivateConversation(PrivateConversation $fkPrivateConversation): PrivateConversationUser
    {
        $this->fkPrivateConversation = $fkPrivateConversation;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate(): \DateTime
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return PrivateConversationUser
     */
    public function setCreationDate(\DateTime $creationDate): PrivateConversationUser
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate(): \DateTime
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return PrivateConversationUser
     */
    public function setModificationDate(\DateTime $modificationDate): PrivateConversationUser
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
}