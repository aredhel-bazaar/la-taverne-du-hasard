<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace MigrationBundle\Entity;

use AppBundle\Entity\Comment;

class Commentaire
{
    public $commentaire_id;
    public $commentaire_date_creation;
    public $commentaire_contenu;
    public $lk_event_id;
    public $lk_news_id;
    public $lk_membre_id;
    
    
    
    public function convertToV2(Comment &$comment, array $membres, array $events, array $news)
    {
        $now = new \DateTime();
    
        $comment
            ->setContent($this->commentaire_contenu)
            ->setFkUser($membres[$this->lk_membre_id]['v2'])
            ->setCreationDate(!empty($this->commentaire_date_creation)     ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->commentaire_date_creation)     : $now)
            ->setModificationDate($now)
        ;
        
        if (!is_null($this->lk_event_id)) {
            $comment->setFkEvent($events[$this->lk_event_id]['v2']);
        } else {
            $comment->setFkArticle($news[$this->lk_news_id]['v2']);
        }
    }
}