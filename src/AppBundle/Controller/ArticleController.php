<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Comment;
use AppBundle\Form\CommentForm;
use AppBundle\Service\CustomEditorBlock;
use AppBundle\Service\Notifier;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class NewsController
 *
 * --------------------*
 * @Route("/billet")
 * --------------------*
 */
class ArticleController extends BaseController
{
    /**
     * --------------------*
     * @Route("/")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return Response
     */
    public function indexAction(EntityManager $em, Request $request)
    {
        $page              = $request->query->getInt('page', 1);
        $nb_articles       = $em->getRepository('AppBundle:Article')->countBy();
        $articleList       = $em->getRepository('AppBundle:Article')->findAll(['creationDate' => 'DESC'], Article::MAX_PER_FRONT_PAGE, ($page-1)*Article::MAX_PER_FRONT_PAGE);
        $nb_pages          = (int) ceil($nb_articles / Article::MAX_PER_FRONT_PAGE);
        
        return $this->render('article/list.html.twig', [
            'article_list' => $articleList,
            'nb_pages'     => $nb_pages,
            'current_page' => $page
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/lire/{article}", name="app_article_read")
     * @Method({"GET", "POST"})
     * --------------------*
     *
     * @param Article       $article
     *
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function readAction(Article $article, EntityManager $em, Request $request)
    {
        if( $article->getStatus() !== Article::STATUS_VISIBLE && !$this->isGranted('ROLE_MODERATOR') ) {
            return new RedirectResponse($this->generateUrl('homepage'));
        }
    
        $formView     = null;
        $comment_list = $em->getRepository('AppBundle:Comment')->findBy(['fkArticle' => $article], ['creationDate' => 'DESC']);
    
        if( $this->isGranted('ROLE_MEMBER') ) {
            $comment = new Comment();
            $commentForm = $this->createForm(CommentForm::class, $comment);
    
            $commentForm->handleRequest($request);
    
            if( $commentForm->isSubmitted() ) {
                if( $commentForm->isValid() ) {
                    $comment->setFkArticle($article)
                        ->setFkUser($this->getUser())
                        ->setCreationDate(new \DateTime())
                        ->setModificationDate(new \DateTime());
            
                    $em->persist($comment);
                    $em->flush();
            
                    return new RedirectResponse($this->generateUrl('app_article_read', ['article' => $article->getId()]));
                }
            }
    
            $formView = $commentForm->createView();
        }
    
        return $this->render('article/detail.html.twig', [
            'article'      => $article,
            'comment_list' => $comment_list,
            'form'         => $formView,
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/poster/{news_id}")
     * @Security("is_granted('ROLE_NEWS_WRITER')")
     * --------------------*
     *
     * @param Notifier          $notifierService
     * @param CustomEditorBlock $editorBlock
     * @param LoggerInterface   $logger
     * @param EntityManager     $em
     * @param null              $news_id
     * @param Request           $request
     *
     * @return Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createAction(
        Notifier $notifierService, CustomEditorBlock $editorBlock, LoggerInterface $logger, EntityManager $em,
        $news_id=null, Request $request
    )
    {
        $newsRepository = $em->getRepository('AppBundle:Article');
        
        if (!is_null($news_id)) {
            $news = $newsRepository->find($news_id);
        } else {
            $news = new Article();
        }
        
        $form = $this->createForm('AppBundle\Form\NewsCreateForm', $news);
        
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            if($form->isValid()) {
                $news->setContent($editorBlock->parseCustomBlocks($news->getContent()));
                
                if( is_null($news_id) ) {
                    $news->setStatus(1)
                        ->setCreationDate(new \DateTime())
                        ->setFkUser($this->getUser());
                }
                
                $news->setModificationDate(new \DateTime());
    
                $em->persist($news);
                $em->flush();
    
                try {
                    $notifierService->notify(Notifier::NOTIFY_CASE_NEW_ARTICLE, $news->getId());
                } catch (\Exception $e) {
                    $logger->error("Unable to notify people after creation of an article : ".$e->getMessage());
                }
                
                return new RedirectResponse($this->generateUrl('app_admin_listnews'));
            }
        }
        
        return $this->render('article/create.html.twig', [
            'form' => $form->createView()
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/archiver/{news}")
     * @Security("is_granted('ROLE_NEWS_WRITER')")
     * --------------------*
     *
     * @param Article       $news
     * @param Request       $request
     *
     * @param EntityManager $em
     *
     * @return RedirectResponse|Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function archiveAction(Article $news, Request $request, EntityManager $em)
    {
        $form = $this->createForm('AppBundle\Form\NewsArchiveForm', $news);
    
        $form->handleRequest($request);
    
        if( $form->isSubmitted() ) {
            if ($form->isValid()) {
                $news->setStatus(Article::STATUS_ARCHIVED)
                    ->setModificationDate(new \DateTime());
                
                $em->flush();
                
                return new RedirectResponse($this->generateUrl('app_admin_listnews'));
            }
        }
    
        return $this->render('article/archive.html.twig', [
            'form' => $form->createView()
        ]);
    }
    /**
     * --------------------*
     * @Route("/widget", name="app_article_widget")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $em
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function widgetAction(EntityManager $em)
    {
        /** @var Article[] $articles */
        $articles   = $em->getRepository('AppBundle:Article')->findBy([], ['creationDate' => 'DESC'], 5);
        
        return $this->render('article/widget.html.twig', [
            'articles'   => $articles,
        ]);
    }
}
