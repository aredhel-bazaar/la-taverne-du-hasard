<?php

namespace AppBundle\Controller;

use AppBundle\Service\Sitemap;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Request;

class DefaultController extends BaseController
{
    /**
     * --------------------*
     * @Route("/", name="homepage")
     * --------------------*
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction()
    {
        return $this->render('home/home.html.twig');
    }
    
    
    
    /**
     * --------------------*
     * @Route("/mentions", name="mentions")
     * --------------------*
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function mentionsAction()
    {
        return $this->render('home/mentions.html.twig');
    }
    
    
    
    /**
     * Génère le sitemap du site.
     *
     * @Route("/sitemap.{_format}", name="front_sitemap", Requirements={"_format" = "xml"})
     * @param Sitemap $sitemapService
     *
     * @param Request $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function siteMapAction(Sitemap $sitemapService, Request $request)
    {
        $sitemap = $sitemapService->generate();
        return $this->render('template/sitemap.'.$request->get('_format').'.twig', [
            'sitemap' => $sitemap
        ]);
    }
}
