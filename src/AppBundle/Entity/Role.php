<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\TranslationBundle\Translation\TranslationContainerInterface;
use Symfony\Component\Security\Core\Role\RoleInterface;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RoleRepository")
 * @ORM\Table(name="role", indexes={})
 * @ORM\Cache()
 */
class Role extends \Symfony\Component\Security\Core\Role\Role implements TranslationContainerInterface
{
    const ROLE_VISITOR     = 'ROLE_VISITOR';
    const ROLE_NEWBIE      = 'ROLE_NEWBIE';
    const ROLE_MEMBER      = 'ROLE_MEMBER';
    const ROLE_NEWS_WRITER = 'ROLE_NEWS_WRITER';
    const ROLE_ADHERENT    = 'ROLE_ADHERENT';
    const ROLE_GAME_MASTER = 'ROLE_GAME_MASTER';
    const ROLE_MODERATOR   = 'ROLE_MODERATOR';
    const ROLE_ADMIN       = 'ROLE_ADMIN';
    const ROLE_SUPER_ADMIN = 'ROLE_SUPER_ADMIN';
    
    const ROLES = [
        self::ROLE_VISITOR,
        self::ROLE_NEWBIE,
        self::ROLE_MEMBER,
        self::ROLE_NEWS_WRITER,
        self::ROLE_ADHERENT,
        self::ROLE_GAME_MASTER,
        self::ROLE_MODERATOR,
        self::ROLE_ADMIN,
        self::ROLE_SUPER_ADMIN
    ];
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60)
     */
    private $name;
    
    /**
     * @var string
     *
     * @ORM\Column(name="role", type="string", length=60)
     */
    private $role;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="order", type="integer", length=2)
     */
    private $order;
    
    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="roles")
     * @ORM\JoinTable(name="lk_user_role")
     *
     */
    private $users;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    
    
    
    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
    
    
    
    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name)
    {
        $this->name = $name;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getRole(): string
    {
        return $this->role;
    }
    
    
    
    /**
     * @param string $role
     *
     * @return $this
     */
    public function setRole(string $role)
    {
        $this->role = $role;
        
        return $this;
    }
    
    
    
    /**
     * @return int
     */
    public function getOrder(): int
    {
        return $this->order;
    }
    
    
    
    /**
     * @param int $order
     */
    public function setOrder(int $order)
    {
        $this->order = $order;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getUsers()
    {
        return $this->users;
    }
    
    
    
    /**
     * @param mixed $users
     */
    public function setUsers($users)
    {
        $this->users = $users;
    }
    
    
    
    /**
     * Returns an array of messages.
     *
     * @return array<Message>
     */
    public static function getTranslationMessages()
    {
        $messages = [];
        foreach (self::ROLES as $role) {
            $messages[] = new \JMS\TranslationBundle\Model\Message($role);
        }
        
        return $messages;
    }
}