<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SubscriberRepository")
 * @ORM\Table(name="subscriber", indexes={})
 * @ORM\Cache()
 */
class Subscriber
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id")
     */
    private $fkUser;
    
    /**
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="subscribers")
     * @ORM\JoinColumn(name="fk_event_id", referencedColumnName="id")
     */
    private $fkEvent;
    
    /**
     * @var bool
     *
     * @ORM\Column(name="is_present", type="boolean")
     */
    private $present;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Subscriber
     */
    public function setCreationDate(\DateTime $creationDate): Subscriber
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Subscriber
     */
    public function setModificationDate(\DateTime $modificationDate): Subscriber
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param mixed $fkUser
     *
     * @return Subscriber
     */
    public function setFkUser($fkUser)
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return mixed
     */
    public function getFkEvent()
    {
        return $this->fkEvent;
    }
    
    
    
    /**
     * @param mixed $fkEvent
     *
     * @return Subscriber
     */
    public function setFkEvent($fkEvent)
    {
        $this->fkEvent = $fkEvent;
        
        return $this;
    }
    
    
    
    /**
     * @return bool
     */
    public function isPresent(): bool
    {
        return $this->present;
    }
    
    
    
    /**
     * @param bool $present
     *
     * @return Subscriber
     */
    public function setPresent(bool $present): Subscriber
    {
        $this->present = $present;
        
        return $this;
    }
}