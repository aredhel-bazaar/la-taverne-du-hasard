(function() {
    "use strict";

    var questionPrototype;
    var formIndexes = [];
    var surveyFormElm = $('form.js-survey-form');

    var contentLoaderElm = $('.js-content-loader');

    /**
     * JQuery Extends
     */
    $.fn.extend({
        addQuestion: function () {
            var questionElm = $(questionPrototype.replace(/__question_index__/g, formIndexes.length));
            questionElm.attr('data-index', formIndexes.length).addClass('js-question question').appendTo(questionsElm);

            formIndexes.push([]);

            return questionElm;
        },

        removeQuestion: function () {
            $(this).remove();
        },

        addAnswer: function () {
            var questionIndex = $(this).parents('.js-question').data('index');
            var answerPrototype = $(this).data('prototype');

            $(answerPrototype.replace(/__answer_index__/g, formIndexes[questionIndex].length)).addClass('js-answer answer').appendTo($(this));
            formIndexes[questionIndex].push('');
        },

        removeAnswer: function () {
            $(this).remove();
        }
    });

    $.extend({

    });

    if (surveyFormElm.length) {
        var questionsElm      = $('.js-questions-container');
        questionPrototype     = questionsElm.data('prototype');

        surveyFormElm
            .on('click', '.js-add-questions', function () {
                var questionElm = questionsElm.addQuestion();
                questionElm.first().find('.js-answers-container').addAnswer();
            })
            .on('click', '.js-remove-questions', function () {
                $(this).parents('.js-question[data-index="'+ $(this).data('questions-index') +'"]').removeQuestion();
                $(this).remove();
            })
            .on('click', '.js-add-answers', function () {
                $(this).siblings('.js-answers-container').addAnswer();
            })
            .on('click', '.js-remove-answers', function () {
                $(this).prev().removeAnswer();
                $(this).parents('.js-answer').remove();
            })
        ;
    }

    if (contentLoaderElm.length) {
        contentLoaderElm.each(function(index, elm) {
            $.ajax({
                url: $(elm).data('url')
            })
                .done(function (response) {
                    $(elm).replaceWith(response.data);
                    $.initSemanticComponents();
                })
            ;
        });
    }

    $('.js-main-section')
        .on('submit', '.js-answer-survey', function (e) {
            e.preventDefault();

            var formElm = $(this);

            formElm.find('.js-submit-button').buttonLoader('start');

            $.ajax({
                url: formElm.attr('action'),
                type: 'POST',
                data: formElm.serialize()
            })
                .done(function (response) {
                    toastr.success(response.returnMessage);
                    formElm.parents('.js-loaded-content').replaceWith(response.data);
                })
                .always(function () {
                    formElm.find('.js-submit-button').buttonLoader('stop');
                })
            ;
        })
        .on('click', '.js-survey-edit-answers', function(e) {
            var $button = $(this);

            $.ajax({
                url: $button.attr('data-href'),
                type: 'GET'
            })
                .done(function (response) {
                    $button.parents('.js-loaded-content').replaceWith(response);
                    $.initSemanticComponents();
                })
            ;
        })
    ;
})();
