<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Comment;
use AppBundle\Entity\Event;
use AppBundle\Entity\OpenEvent;
use AppBundle\Entity\Subscriber;
use AppBundle\Form\EventCreateForm;
use AppBundle\Service\Calendar;
use AppBundle\Service\Notifier;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\Translation\TranslatorInterface;

/**
 * Class CalendarController
 * @package AppBundle\Controller
 *
 * --------------------*
 * @Route("/calendrier")
 * --------------------*
 */
class CalendarController extends BaseController
{
    /**
     * --------------------*
     * @Route("/")
     * @Method("GET")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     * @param Calendar      $calendarService
     * @param int           $year
     * @param int           $month
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexAction(EntityManager $em, Request $request, Calendar $calendarService, int $year = null, int $month = null )
    {
        $year         = (is_null($year) ? date('Y') : $year);
        $month        = (is_null($month) ? date('m') : str_pad($month, 2, '0', STR_PAD_LEFT));
        $calendar     = $calendarService
            ->setDuration('1 month')
            ->setFrom($year, $month)
            ->getCalendar();
        $prevDateTime = $calendarService->getPrevMonth($year, $month);
        $nextDateTime = $calendarService->getNextMonth($year, $month);
        $open_events  = $em->getRepository('AppBundle:OpenEvent')->findBetween(reset($calendar), end($calendar));
        
        if( $request->isXmlHttpRequest() ) {
            $view = 'calendar/mini-calendar.html.twig';
        } else {
            $view = 'calendar/calendar.html.twig';
        }
        
        return $this->render($view, [
            'calendar'   => $calendar,
            'open_events'=> $open_events,
            'prev_month' => $prevDateTime->format('m'),
            'prev_year'  => $prevDateTime->format('Y'),
            'month'      => $month,
            'year'       => $year,
            'next_month' => $nextDateTime->format('m'),
            'next_year'  => $nextDateTime->format('Y'),
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/creer/{event}")
     * @Security("is_granted('ROLE_ADMIN')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Notifier      $notifierService
     * @param Request       $request
     * @param null          $event
     *
     * @return RedirectResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function createAction(EntityManager $em, Notifier $notifierService, Request $request, $event=null)
    {
        if( is_null($event) ) {
            $is_new_event = true;
            $event = new Event();
        } else {
            $is_new_event = false;
            $event = $em->getRepository('AppBundle:Event')->find($event);
        }
        
        //-- Paramètres de préremplissage
        $event_date = $request->get('date');
        if( !is_null($event_date) ) {
            $eventDateTime = \DateTime::createFromFormat('Y-m-d', $event_date)->setTime(0, 0);
            $event->setStartDate($eventDateTime)
                ->setEndDate($eventDateTime);
        }
        //--
        
        $form = $this->createForm(EventCreateForm::class, $event);
        $form->handleRequest($request);
    
        if( $form->isSubmitted() ) {
            if( $form->isValid() ) {
                $user = null;
                if( $event->getFkOrganizer() ) {
                    try {
                        $user = $em->getRepository('AppBundle:User')->find($event->getFkOrganizer()->getId());
                    } catch (\Exception $e) {
                        $user = null;
                    }
                }
                
                if( $is_new_event ) {
                    $event->setCreationDate(new \DateTime())
                        ->setFkUser($this->getUser());
                }
    
                $event->setModificationDate(new \DateTime())
                    ->setFkOrganizer($user);
                
                $em->beginTransaction();
                
                $em->persist($event);
                $em->flush();
                
                if ($is_new_event) {
                    try {
                        $notifierService->notify(Notifier::NOTIFY_CASE_NEW_EVENT, $event->getId());
                    } catch (\Exception $e) {
                        $em->rollback();
                        
                        throw $e;
                    }
                }
                $em->commit();
    
                return new RedirectResponse($this->generateUrl('app_admin_listevents'));
            }
        }
        
        return $this->render('calendar/create.html.twig', [
            'form' => $form->createView(),
        ]);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/open-event/personnaliser")
     * @Method({"GET", "POST"})
     * @Security("is_granted('ROLE_ADMIN')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse|\Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function OpenEventCustomizeAction(EntityManager $em, Request $request)
    {
        $date   = $request->get('datetime');
    
        //-- Error handler
        if( is_null($date) ) {
            throw new \Exception("You must specify open datetime.");
        }
    
        $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $date);
    
        if( is_null($dateTime) ) {
            throw new \Exception("Invalid date format specified.");
        }
        //--
    
        $openEvent = $em->getRepository('AppBundle:OpenEvent')->findOpenEventByDay($dateTime);
        
        if( is_null($openEvent) ) {
            $openEvent = new OpenEvent();
            $event     = new Event();
            $event->setStartDate($dateTime)
                ->setEndDate($dateTime);
        } else {
            $event = $openEvent->getFkEvent();
        }
    
        $form = $this->createForm(EventCreateForm::class, $event, [
            'no_places'    => true,
            'action'       => $this->generateUrl($request->get('_route'), ['datetime' => $date]),
        ]);
        $form->handleRequest($request);
    
        if( $form->isSubmitted() ) {
            if( $form->isValid() ) {
                $action = $request->get('action');
                $now    = new \DateTime();
    
                $user = null;
                if( $event->getFkOrganizer() ) {
                    try {
                        $user = $em->getRepository('AppBundle:User')->find($event->getFkOrganizer()->getId());
                    } catch (\Exception $e) {
                        $user = null;
                    }
                }
                
                $event->setCreationDate($now)
                    ->setModificationDate($now)
                    ->setFkOrganizer($user);
                
                $openEvent->setCreationDate($now)
                    ->setModificationDate($now)
                    ->setDate($dateTime)
                    ->setFkEvent($event)
                    ->setFkUser($this->getUser())
                    ->setOpen($action == 'open');
                
                $em->persist($event);
                $em->persist($openEvent);
                
                $em->flush();
                
                return $this->json(null);
            }
        }
        
        return $this->render('calendar/customize.html.twig', [
            'form'  => $form->createView(),
        ]);
    }
    
    
    
    /**
     * @Route("/open-event/ouvrir", options={"expose"=true})
     *
     * @param TranslatorInterface $translator
     * @param EntityManager       $em
     * @param Request             $request
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     * @throws \Exception
     */
    public function OpenEventElementAction(TranslatorInterface $translator, EntityManager $em, Request $request)
    {
        $action = $request->get('action');
        $date   = $request->get('datetime');
        $now    = new \DateTime();
        
        //-- Error handler
        if( is_null($action) ) {
            throw new \Exception("You must specify action.");
        }

        if( is_null($date) ) {
            throw new \Exception("You must specify open datetime.");
        }

        $dateTime = \DateTime::createFromFormat('Y-m-d H:i:s', $date);

        if( is_null($dateTime) ) {
            throw new \Exception("Invalid date format specified.");
        }
        //--
        
        if ($dateTime->format('N') == 7) {
            $dateTimeStart = (clone $dateTime)->setTime(13, 30);
            $dateTimeEnd   = (clone $dateTime)->setTime(18, 30);
        } else {
            $dateTimeStart = (clone $dateTime)->setTime(21, 00);
            $dateTimeEnd   = (clone $dateTime)->setTime(23, 59);
        }
    
        $openEvent = $em->getRepository('AppBundle:OpenEvent')->findOneBy(['date' => $dateTime]);
        $cakeEvent = $em->getRepository('AppBundle:Event')->findOneBy(['type' => Event::TYPE_CAKE_EVENT, 'startDate' => $dateTimeStart]);
        $newOpenEvent = (is_null($openEvent));
        
        if( $newOpenEvent ) {
            $event = new Event();
            $event->setFkUser($this->getUser())
                ->setCreationDate($now)
                ->setModificationDate($now)
                ->setType(Event::TYPE_OPEN_EVENT)
                ->setStartDate($dateTimeStart)
                ->setEndDate($dateTimeEnd)
                ->setPlaces(0);
            $em->persist($event);
            
            $openEvent = new OpenEvent();
            $openEvent->setDate($dateTime)
                ->setFkUser($this->getUser())
                ->setCreationDate($now)
                ->setFkEvent($event);
        }
        
        $openEvent->setModificationDate($now);
        $event = $openEvent->getFkEvent();
    
        if( $action == 'close' ) {
            $openEvent->setOpen(false);
            
            $event->setName($translator->trans('open_event.closed.title.default'))
                ->setContent($translator->trans('open_event.closed.content.default'));
        } else {
            $openEvent->setOpen(true);
    
            if (is_null($cakeEvent)) {
                $cakeEvent = clone $event;
                $cakeEvent->setType(Event::TYPE_CAKE_EVENT)
                    ->setName($translator->trans('cake_event.title.default'))
                    ->setContent($translator->trans('cake_event.content.default'));
                $em->persist($cakeEvent);
            }
            
            $event->setName($translator->trans('open_event.opened.title.default'))
                ->setContent($translator->trans('open_event.opened.content.default'));
        }
    
        $em->persist($openEvent);
        $em->flush();
    
        return $this->json(null);
    }
    
    
    
    /**
     * --------------------*
     * @Route("/event/{id}/archiver")
     * @Security("is_granted('ROLE_ADMIN')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param int           $id
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function archiveAction(EntityManager $em, int $id)
    {
        $event = $em->getRepository('AppBundle:Event')
            ->find($id);
        
        if (!is_null($event)) {
            $em->remove($event);
            $em->flush();
        }
    
        return $this->redirectToRoute('app_admin_listevents');
    }
    
    
    
    /**
     * --------------------*
     * @Route("/event/{event}/{action}")
     * @Method("GET")
     * @Security("is_granted('ROLE_MEMBER')")
     * --------------------*
     *
     * @param EntityManager $em
     * @param Event         $event
     * @param string        $action
     *
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function eventSubscriptionAction(EntityManager $em, Event $event, string $action)
    {
        $subscriber          = $em->getRepository('AppBundle:Subscriber')->findOneBy(['fkUser' => $this->getUser(), 'fkEvent' => $event]);
        $openEvent           = $em->getRepository('AppBundle:OpenEvent')->findOneBy(['date' => $event->getStartDate()]);
        $openEvent           = $openEvent->getFkEvent();
        $openEventSubscriber = $em->getRepository('AppBundle:Subscriber')->findOneBy(['fkUser' => $this->getUser(), 'fkEvent' => $openEvent]);
    
        if( is_null($subscriber) ) {
            $subscriber = new Subscriber();
            $subscriber->setFkEvent($event)
                ->setFkUser($this->getUser())
                ->setCreationDate(new \DateTime())
                ->setModificationDate(new \DateTime());
        }
        
        if( is_null($openEventSubscriber) && $event->getId() !== $openEvent->getId()) {
            $openEventSubscriber = new Subscriber();
            $openEventSubscriber->setFkEvent($openEvent)
                ->setFkUser($this->getUser())
                ->setCreationDate(new \DateTime())
                ->setModificationDate(new \DateTime());
        }
        
        if( $action == 'subscribe' ) {
            $subscriber->setPresent(true);
            if ($event->getId() !== $openEvent->getId()) {
                $openEventSubscriber->setPresent(true);
            }
        } else {
            $subscriber->setPresent(false);
        }
    
        $em->persist($subscriber);
        if ($event->getId() !== $openEvent->getId()) {
            $em->persist($openEventSubscriber);
        }
        $em->flush();
        
        return $this->json([]);
    }
    
    
    
    /**
     * Récupère tous les évènements d'un jour donné
     *
     * --------------------*
     * @Route("/{year}/{month}/{day}")
     * --------------------*
     *
     * @param EntityManager $em
     * @param int           $year
     * @param int           $month
     * @param int           $day
     *
     * @param Request       $request
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dayAction(EntityManager $em, int $year, int $month, int $day, Request $request)
    {
        $date = $year.'/'.$month.'/'.$day;
        $startDateTime = new \DateTime($date.' 00:00:00');
        $endDateTime   = new \DateTime($date.' 23:59:59');
        
        //-- Les dates ne sont pas valides ? On redirige sur la home
        if (!($startDateTime instanceof \DateTime) || !($endDateTime instanceof \DateTime)) {
            return $this->redirectToRoute('homepage');
        }
        //--
        
        $openEvent   = $em->getRepository('AppBundle:OpenEvent')->findOneBy(['date' => $startDateTime]);
        $event_list  = [];
        $subscribers = [];
        
        if (is_null($openEvent)) {
            return $this->redirectToRoute('app_calendar_index');
        }
        
        if( $request->get('comment') && $this->isGranted('ROLE_MEMBER') ) {
            $comment = new Comment();
            $event   = $em->getRepository('AppBundle:Event')->find('event_id');
            
            $comment->setFkEvent($event)
                ->setContent($request->get('comment'))
                ->setFkUser($this->getUser())
                ->setCreationDate(new \DateTime())
                ->setModificationDate(new \DateTime());
    
            $em->persist($comment);
            $em->flush();
            
            return $this->json([]);
        }
        
        if( !is_null($openEvent) && $openEvent->isOpen() == true ) {
            $event_list      = $em->getRepository('AppBundle:Event')->findBetween($startDateTime, $endDateTime);
            if( !empty($event_list) ) {
                $event_list  = $event_list[$startDateTime->format('d/m')];
                $subscribers = $em->getRepository('AppBundle:Subscriber')->findByEvent($event_list);
    
                /** @var Event $event */
                foreach( $event_list as $index => $event ) {
                    if( !isset($subscribers[$event->getId()]) ) {
                        $subscribers[$event->getId()] = [];
                    }
                    
                    if( $event->getId() === $openEvent->getFkEvent()->getId() ) {
                        unset($event_list[$index]);
                    }
                }
            }
        }
        
        return $this->render('calendar/list_events.html.twig', [
            'open_event'  => $openEvent,
            'event_list'  => $event_list,
            'subscribers' => $subscribers,
        ]);
    }
    
    
    
    /**
     * @see indexAction
     *
     * --------------------*
     * @Route("/{year}/{month}")
     * --------------------*
     * @param int           $year
     * @param int           $month
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function monthAction(int $year, int $month)
    {
        return $this->forward('AppBundle:Calendar:index', ['year' => $year, 'month' => $month]);
    }
}
