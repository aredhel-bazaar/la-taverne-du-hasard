<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161009162955 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD recover_password_code VARCHAR(60) DEFAULT NULL, CHANGE code_recover_password account_validation_code VARCHAR(60) DEFAULT NULL, CHANGE timestamp_expiration_code code_expiration_date DATETIME DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE user ADD code_recover_password VARCHAR(60) DEFAULT NULL COLLATE utf8_unicode_ci, DROP account_validation_code, DROP recover_password_code, CHANGE code_expiration_date timestamp_expiration_code DATETIME DEFAULT NULL');
    }
}
