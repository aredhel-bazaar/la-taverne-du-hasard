<?php

namespace AppBundle\Service;

use AppBundle\Entity\Accreditation;
use AppBundle\Entity\Role;
use AppBundle\Extensions\Entity\PermissionInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface;

class AccreditationProvider
{
    private $authorizationChecker;
    /**
     * @var TokenStorage
     */
    private $token;
    /**
     * @var ObjectManager
     */
    private $objectManager;
    
    
    
    /**
     * Accreditation constructor.
     *
     * @param AuthorizationCheckerInterface $authorizationChecker
     * @param TokenStorage                  $token
     * @param ObjectManager                 $objectManager
     */
    public function __construct(AuthorizationCheckerInterface $authorizationChecker, TokenStorage $token, ObjectManager $objectManager)
    {
        $this->authorizationChecker = $authorizationChecker;
        $this->token = $token;
        $this->objectManager = $objectManager;
    }
    
    
    
    public function __call($method, $args = [])
    {
        if (preg_match('/^can([a-zA-Z]+)/', $method, $matches)) {
            return call_user_func([$this, 'can'], strtolower($matches[1]), $args[0]);
        }
        
        throw new \Exception("Method $method not found in ".__CLASS__);
    }
    
    
    
    private function isAccreditationTypeValid($type)
    {
        return (in_array($type, Accreditation::TYPES));
    }
    
    
    
    /**
     * @param string              $type
     * @param PermissionInterface $item
     *
     * @return bool
     * @throws \Exception
     */
    public function can(string $type, PermissionInterface $item)
    {
        if (!$this->isAccreditationTypeValid($type)) {
            throw new \Exception("Accreditation type $type unknown");
        }
        
        //-- Admins can always access the element
        if ($this->authorizationChecker->isGranted(Role::ROLE_ADMIN)) {
            return true;
        }
        //--
    
        $rights = $item->getAccessRights();
        $roles  = $this->token->getToken()->getRoles();
        if (empty($roles)) {
            $roles[] = new \Symfony\Component\Security\Core\Role\Role('ROLE_VISITOR');
        }
    
        /**
         * @var mixed $index
         * @var Role $role
         */
        foreach ($roles as $index => $role) {
            unset($roles[$index]);
            $roles[$role->getRole()] = $role->getRole();
        }
    
        foreach ($rights as $index => $right) {
            if ($right[$type]) {
                $rights[$index] = $rights[$index][$type];
            } else {
                unset($rights[$index]);
            }
        }
    
        return (!empty(array_intersect_key($roles, $rights)));
    }
    
    
    
    public function getRightsHierarchy()
    {
        $roles = $this->objectManager->getRepository('AppBundle:Role')->findBy([], ['order' => 'DESC']);
    
        $right_list   = [];
        foreach (Accreditation::TYPES as $type) {
            $moderator_ms = false;
            $member_ms    = false;
            
            /** @var Role $role */
            foreach ($roles as $role) {
                if (!in_array($role->getRole(), [Role::ROLE_ADMIN, Role::ROLE_SUPER_ADMIN])) {
                    //-- Milestones
                    if ($moderator_ms == false) {
                        $moderator_ms = ($role->getRole() == Role::ROLE_MODERATOR);
                    }
                    
                    if ($member_ms == false) {
                        $member_ms = ($role->getRole() == Role::ROLE_MEMBER);
                    }
                    //--
                    
                    $canDo = false;
                    if (
                        $type == Accreditation::TYPE_READ ||
                        $type == Accreditation::TYPE_WRITE && $member_ms ||
                        $type == Accreditation::TYPE_MODERATE && $moderator_ms
                    ) {
                        $canDo = true;
                    }
                    
                    $right_list[$type][$role->getRole()] = $canDo;
                }
            }
        }
        
        return $right_list;
    }
    
    
    
    /**
     * @param array  $obj_list
     * @param string $type
     */
    public function filter(array &$obj_list, string $type)
    {
        if ($this->isAccreditationTypeValid($type)) {
            /** @var PermissionInterface $item */
            foreach ($obj_list as $index => $item) {
                if (!$this->can($type, $item)) {
                    unset($obj_list[$index]);
                }
            }
        }
    }
}