<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AccreditationRepository")
 * @ORM\Table(name="accreditation", indexes={})
 * @ORM\Cache()
 */
class Accreditation
{
    const TYPE_READ     = "read";
    const TYPE_WRITE    = "write";
    const TYPE_MODERATE = "moderate";
    
    const TYPES = [
        self::TYPE_READ,
        self::TYPE_WRITE,
        self::TYPE_MODERATE
    ];
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Topic
     *
     * @ORM\ManyToOne(targetEntity="Topic", inversedBy="rights")
     * @ORM\JoinColumn(name="fk_topic_id", referencedColumnName="id")
     */
    private $fkTopic;
    
    /**
     * @var Section
     *
     * @ORM\ManyToOne(targetEntity="Section", inversedBy="rights")
     * @ORM\JoinColumn(name="fk_section_id", referencedColumnName="id")
     */
    private $fkSection;
    
    /**
     * @var Role
     *
     * @ORM\ManyToOne(targetEntity="Role")
     * @ORM\JoinColumn(name="fk_role_id", referencedColumnName="id")
     */
    private $fkRole;
    
    /**
     * @var string
     *
     * @ORM\Column(name="right_type", type="string", length=30)
     */
    private $type;
    
    /**
     * @var boolean
     *
     * @ORM\Column(name="access", type="boolean")
     */
    private $access = false;
    
    
    
    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Accreditation
     */
    public function setId(int $id): Accreditation
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return Topic|null
     */
    public function getFkTopic()
    {
        return $this->fkTopic;
    }
    
    
    
    /**
     * @param Topic $fkTopic
     *
     * @return Accreditation
     */
    public function setFkTopic($fkTopic)
    {
        $this->fkTopic = $fkTopic;
        
        return $this;
    }
    
    
    
    /**
     * @return Section|null
     */
    public function getFkSection()
    {
        return $this->fkSection;
    }
    
    
    
    /**
     * @param Section $fkSection
     *
     * @return Accreditation
     */
    public function setFkSection($fkSection)
    {
        $this->fkSection = $fkSection;
        
        return $this;
    }
    
    
    
    /**
     * @return Role
     */
    public function getFkRole()
    {
        return $this->fkRole;
    }
    
    
    
    /**
     * @param Role $fkRole
     *
     * @return Accreditation
     */
    public function setFkRole(Role $fkRole)
    {
        $this->fkRole = $fkRole;
        
        return $this;
    }
    
    
    
    /**
     * @return bool
     */
    public function hasAccess(): bool
    {
        return $this->access;
    }
    
    
    
    /**
     * @param bool $access
     *
     * @return Accreditation
     */
    public function setAccess(bool $access): Accreditation
    {
        $this->access = $access;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }
    
    
    
    /**
     * @param string $type
     *
     * @return Accreditation
     */
    public function setType(string $type): Accreditation
    {
        $this->type = $type;
        
        return $this;
    }
}