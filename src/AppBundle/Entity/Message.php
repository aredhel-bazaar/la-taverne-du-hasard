<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 06/10/2016
 * Time: 19:41
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MessageRepository")
 * @ORM\Table(name="message", indexes={})
 * @ORM\Cache()
 */
class Message
{
    const NB_PER_PAGE = 10;
    
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;
    
    /**
     * @var Section
     *
     * @ORM\ManyToOne(targetEntity="Topic", inversedBy="messages")
     * @ORM\JoinColumn(name="fk_topic_id", referencedColumnName="id", nullable=true)
     */
    private $fkTopic;
    
    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="fk_user_id", referencedColumnName="id", nullable=true)
     */
    private $fkUser;
    
    /**
     * @var string
     *
     * @ORM\Column(name="content", type="text")
     */
    private $content;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="creation_date", type="datetime")
     */
    private $creationDate;
    
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="modification_date", type="datetime")
     */
    private $modificationDate;
    
    
    
    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    
    
    
    /**
     * @param int $id
     *
     * @return Message
     */
    public function setId(int $id): Message
    {
        $this->id = $id;
        
        return $this;
    }
    
    
    
    /**
     * @return Section
     */
    public function getFkTopic()
    {
        return $this->fkTopic;
    }
    
    
    
    /**
     * @param Topic $fkTopic
     *
     * @return Message
     */
    public function setFkTopic(Topic $fkTopic): Message
    {
        $this->fkTopic = $fkTopic;
        
        return $this;
    }
    
    
    
    /**
     * @return User
     */
    public function getFkUser()
    {
        return $this->fkUser;
    }
    
    
    
    /**
     * @param User $fkUser
     *
     * @return Message
     */
    public function setFkUser(User $fkUser): Message
    {
        $this->fkUser = $fkUser;
        
        return $this;
    }
    
    
    
    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
    
    
    
    /**
     * @param string $content
     *
     * @return Message
     */
    public function setContent(string $content): Message
    {
        $this->content = $content;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getCreationDate()
    {
        return $this->creationDate;
    }
    
    
    
    /**
     * @param \DateTime $creationDate
     *
     * @return Message
     */
    public function setCreationDate(\DateTime $creationDate): Message
    {
        $this->creationDate = $creationDate;
        
        return $this;
    }
    
    
    
    /**
     * @return \DateTime
     */
    public function getModificationDate()
    {
        return $this->modificationDate;
    }
    
    
    
    /**
     * @param \DateTime $modificationDate
     *
     * @return Message
     */
    public function setModificationDate(\DateTime $modificationDate): Message
    {
        $this->modificationDate = $modificationDate;
        
        return $this;
    }
}