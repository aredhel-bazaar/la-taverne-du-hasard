<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Role;
use Doctrine\ORM\EntityRepository;

class RoleRepository extends BaseRepository
{
    public function findAll()
    {
        $results = parent::findAll();
        
        /** @var Role $role */
        foreach ($results as $index => $role) {
            unset($results[$index]);
            
            $results[$role->getRole()] = $role;
        }
        
        return $results;
    }
    
    
    
    public function findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
    {
        $results = parent::findBy($criteria, $orderBy, $limit, $offset);
        
        /** @var Role $role */
        foreach ($results as $index => $role) {
            unset($results[$index]);
            
            $results[$role->getRole()] = $role;
        }
        
        return $results;
    }
}