<?php

namespace AppBundle\Twig;

class HexToRGBExtension extends \Twig_Extension
{
    /**
     * @return array
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('hex_to_rgba', array($this, 'hex2rgba'))
        );
    }

    /**
     * @param $colour
     *
     * @return array|bool
     */
    public function hex2rgb_array($colour)
    {
        if ( $colour[0] == '#' ) {
            $colour = substr( $colour, 1 );
        }
        if ( strlen( $colour ) == 6 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[1], $colour[2] . $colour[3], $colour[4] . $colour[5] );
        } elseif ( strlen( $colour ) == 3 ) {
            list( $r, $g, $b ) = array( $colour[0] . $colour[0], $colour[1] . $colour[1], $colour[2] . $colour[2] );
        } else {
            return false;
        }
        $r = hexdec( $r );
        $g = hexdec( $g );
        $b = hexdec( $b );
        return array( 'red' => $r, 'green' => $g, 'blue' => $b );
    }



    /**
     * @param     $colour
     * @param int $opacity
     *
     * @return string
     */
    public function hex2rgba($colour, $opacity = 1)
    {
        $rgb = $this->hex2rgb_array($colour);
        return 'rgba('.$rgb['red'].','.$rgb['green'].','.$rgb['blue'].','.$opacity.')';
    }



    /**
     * @return string
     */
    public function getName()
    {
        return 'hex_to_rgb';
    }
}
