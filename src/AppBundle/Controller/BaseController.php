<?php

namespace AppBundle\Controller;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpKernel\KernelInterface;
use Symfony\Component\Translation\Translator;

abstract class BaseController extends AbstractController
{
    /**
     * Envoie un message au format JSON au navigateur
     *
     * @param mixed $data
     * @param null  $message
     * @param int   $code
     *
     * @internal param string $status
     * @return JsonResponse
     */
    public static function outputJSON($data = null, $message = null, $code = Response::HTTP_OK)
    {
        $return = ['data' => $data, 'returnMessage' => $message];
        
        return new JsonResponse($return, $code);
    }
    
    
    
    /**
     *  Sert un fichier via XSendFile
     *
     * @param string $path
     * @param string $filename
     *
     * @return BinaryFileResponse
     */
    public static function outputFile(string $path, string $filename)
    {
        $full_path = $path.$filename;
        
        // Dans le cas où le fichier a été créé dans la même requête
        clearstatcache(false, $full_path);
        
        $FileSystem = new File($full_path);
        
        $Response = new BinaryFileResponse($full_path);
        $Response::trustXSendfileTypeHeader();
        $Response->headers->set('X-Sendfile', $full_path);
        $Response->headers->set('Content-type', $FileSystem->getMimeType());
        $Response->headers->set('Cache-Control', 'must-revalidate, post-check=0, pre-check=0');
        $Response->headers->set('Content-Transfer-Encoding', 'binary');
        $Response->headers->set('Content-Length', filesize($full_path));
        
        $Response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT, $filename);
        
        return $Response;
    }
}
