<?php

namespace AppBundle\Form;

use AppBundle\Entity\Article;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewsCreateForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => 'news_create_form.label.title'
            ])
            ->add('subtitle', TextType::class, [
                'label' => 'news_create_form.label.subtitle'
            ])
            ->add('content', CKEditorType::class, [
                'label'       => 'news_create_form.label.content',
                'config_name' => 'admin_config'
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => Article::class
        ));
    }
}
