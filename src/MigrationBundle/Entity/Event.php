<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace MigrationBundle\Entity;

class Event
{
    public $event_id;
    public $event_nom;
    public $event_organisateur;
    public $event_date_debut;
    public $event_date_fin;
    public $event_nb_places;
    public $event_contenu;
    public $event_date_creation;
    public $event_date_modification;
    public $event_ouvert;
    public $lk_event_membre;
    
    
    
    public function convertToV2(\AppBundle\Entity\Event &$event, array $membres)
    {
        $now = new \DateTime();
        
        $event
            ->setFkUser($membres[$this->lk_event_membre]['v2'])
            ->setName($this->event_nom)
            ->setStartDate(\DateTime::createFromFormat('Y-m-d H:i:s', $this->event_date_debut))
            ->setEndDate(\DateTime::createFromFormat('Y-m-d H:i:s', $this->event_date_fin))
            ->setPlaces($this->event_nb_places)
            ->setContent($this->event_contenu)
            ->setCreationDate(      !empty($this->event_date_creation)     ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->event_date_creation)     : $now)
            ->setModificationDate(  !empty($this->event_date_modification) ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->event_date_modification) : $now)
        ;
        
        if (!is_null($this->event_organisateur)) {
            $event->setFkOrganizer($membres[$this->event_organisateur]['v2']);
        }
    }
}