<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161007220414 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE activity_log (id INT AUTO_INCREMENT NOT NULL, fk_user_id INT DEFAULT NULL, ip VARCHAR(100) NOT NULL, creation_date DATETIME NOT NULL, browser_name VARCHAR(100) NOT NULL, browser_version VARCHAR(20) NOT NULL, bot_name VARCHAR(100) DEFAULT NULL, route VARCHAR(200) NOT NULL, status_code INT NOT NULL, is_mobile TINYINT(1) NOT NULL, INDEX IDX_FD06F6475741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE commentaire (id INT AUTO_INCREMENT NOT NULL, fk_news_id INT DEFAULT NULL, fk_event_id INT DEFAULT NULL, fk_user_id INT DEFAULT NULL, content LONGTEXT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_67F068BC458B648C (fk_news_id), INDEX IDX_67F068BC43DFAB55 (fk_event_id), INDEX IDX_67F068BC5741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE event (id INT AUTO_INCREMENT NOT NULL, fk_organizer_id INT DEFAULT NULL, fk_user_id INT DEFAULT NULL, name VARCHAR(100) NOT NULL, start_date DATETIME NOT NULL, end_date DATETIME NOT NULL, places INT NOT NULL, content LONGTEXT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_3BAE0AA7A6B14FC3 (fk_organizer_id), INDEX IDX_3BAE0AA75741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE article (id INT AUTO_INCREMENT NOT NULL, fk_tag_id INT DEFAULT NULL, fk_user_id INT DEFAULT NULL, title VARCHAR(150) NOT NULL, subtitle VARCHAR(300) NOT NULL, content LONGTEXT NOT NULL, image_cover VARCHAR(250) NOT NULL, status INT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_23A0E669A7C0B81 (fk_tag_id), INDEX IDX_23A0E665741EEB9 (fk_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE tag (id INT AUTO_INCREMENT NOT NULL, fk_tag_id INT DEFAULT NULL, name VARCHAR(60) NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_389B7839A7C0B81 (fk_tag_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE activity_log ADD CONSTRAINT FK_FD06F6475741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC458B648C FOREIGN KEY (fk_news_id) REFERENCES article (id)');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC43DFAB55 FOREIGN KEY (fk_event_id) REFERENCES event (id)');
        $this->addSql('ALTER TABLE commentaire ADD CONSTRAINT FK_67F068BC5741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7A6B14FC3 FOREIGN KEY (fk_organizer_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA75741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E669A7C0B81 FOREIGN KEY (fk_tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE article ADD CONSTRAINT FK_23A0E665741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE tag ADD CONSTRAINT FK_389B7839A7C0B81 FOREIGN KEY (fk_tag_id) REFERENCES tag (id)');
        $this->addSql('ALTER TABLE user ADD public_name VARCHAR(60) NOT NULL, CHANGE email email VARCHAR(255) NOT NULL, CHANGE theme theme VARCHAR(40) NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC43DFAB55');
        $this->addSql('ALTER TABLE commentaire DROP FOREIGN KEY FK_67F068BC458B648C');
        $this->addSql('ALTER TABLE article DROP FOREIGN KEY FK_23A0E669A7C0B81');
        $this->addSql('ALTER TABLE tag DROP FOREIGN KEY FK_389B7839A7C0B81');
        $this->addSql('DROP TABLE activity_log');
        $this->addSql('DROP TABLE commentaire');
        $this->addSql('DROP TABLE event');
        $this->addSql('DROP TABLE article');
        $this->addSql('DROP TABLE tag');
        $this->addSql('ALTER TABLE user DROP public_name, CHANGE email email VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, CHANGE theme theme VARCHAR(40) DEFAULT NULL COLLATE utf8_unicode_ci');
    }
}
