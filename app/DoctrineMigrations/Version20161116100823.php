<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20161116100823 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('BEGIN');
        $this->addSql('ALTER TABLE open_event ADD date DATE NOT NULL, DROP date_time');
        $this->addSql('ALTER TABLE subscriber ADD modification_date DATETIME NOT NULL, CHANGE date_creation creation_date DATETIME NOT NULL');
        $this->addSql('COMMIT');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE open_event ADD date_time DATETIME NOT NULL, DROP date');
        $this->addSql('ALTER TABLE subscriber ADD date_creation DATETIME NOT NULL, DROP creation_date, DROP modification_date');
    }
}
