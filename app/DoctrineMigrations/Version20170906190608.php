<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170906190608 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE answer (id INT AUTO_INCREMENT NOT NULL, fk_question_id INT DEFAULT NULL, label VARCHAR(150) NOT NULL, `order` INT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_DADD4A2577D15A8B (fk_question_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE question (id INT AUTO_INCREMENT NOT NULL, fk_survey_id INT DEFAULT NULL, label VARCHAR(150) NOT NULL, `order` INT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_B6F7494ED2A7870D (fk_survey_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE survey (id INT AUTO_INCREMENT NOT NULL, status INT NOT NULL, closure_date DATETIME NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lk_user_answer (fk_user_id INT NOT NULL, fk_answer_id INT NOT NULL, creation_date DATETIME NOT NULL, modification_date DATETIME NOT NULL, INDEX IDX_44E27B75741EEB9 (fk_user_id), INDEX IDX_44E27B7CB6A9F97 (fk_answer_id), PRIMARY KEY(fk_user_id, fk_answer_id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE answer ADD CONSTRAINT FK_DADD4A2577D15A8B FOREIGN KEY (fk_question_id) REFERENCES question (id)');
        $this->addSql('ALTER TABLE question ADD CONSTRAINT FK_B6F7494ED2A7870D FOREIGN KEY (fk_survey_id) REFERENCES survey (id)');
        $this->addSql('ALTER TABLE lk_user_answer ADD CONSTRAINT FK_44E27B75741EEB9 FOREIGN KEY (fk_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE lk_user_answer ADD CONSTRAINT FK_44E27B7CB6A9F97 FOREIGN KEY (fk_answer_id) REFERENCES answer (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE lk_user_answer DROP FOREIGN KEY FK_44E27B7CB6A9F97');
        $this->addSql('ALTER TABLE answer DROP FOREIGN KEY FK_DADD4A2577D15A8B');
        $this->addSql('ALTER TABLE question DROP FOREIGN KEY FK_B6F7494ED2A7870D');
        $this->addSql('DROP TABLE answer');
        $this->addSql('DROP TABLE question');
        $this->addSql('DROP TABLE survey');
        $this->addSql('DROP TABLE lk_user_answer');
    }
}
