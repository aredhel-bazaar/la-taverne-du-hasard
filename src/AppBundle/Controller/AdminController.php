<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Article;
use AppBundle\Entity\Event;
use AppBundle\Entity\User;
use AppBundle\Service\Calendar;
use Doctrine\ORM\EntityManager;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;

/**
 * Class AdminController
 *
 * --------------------*
 * @Route("/apc")
 * --------------------*
 *
 * @Security("has_role('ROLE_ADMIN')")
 */
class AdminController extends BaseController
{
    /**
     * @Route("/")
     * @param EntityManager $em
     *
     * @return Response
     */
    public function indexAction(EntityManager $em)
    {
        $surveys = $em->getRepository('AppBundle:Survey')
            ->findBy([], ['creationDate' => 'DESC'], 5)
        ;
        
        $users = $em->getRepository('AppBundle:User')
            ->findBy([], ['creationDate' => 'DESC'], 5);
        
        return $this->render('admin/index.html.twig', [
            'surveys' => $surveys,
            'users'   => $users
        ]);
    }
    
    
    
    /**
     * @Route("/articles/lister")
     *
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return Response
     */
    public function listNewsAction(EntityManager $em, Request $request)
    {
        $page           = $request->query->getInt('page', 1);
        $newsRepository = $em->getRepository('AppBundle:Article');
        $nb_news        = $newsRepository->countBy();
        $newsList       = $newsRepository->findBy([], ['creationDate' => 'DESC'], Article::MAX_PER_ADMIN_PAGE, $page - 1);
        $nb_pages       = ceil($nb_news / Article::MAX_PER_ADMIN_PAGE);
    
        return $this->render('admin/news/list.html.twig', [
            'news_list'     => $newsList,
            'nb_pages'      => $nb_pages,
            'current_pages' => $page,
        ]);
    }
    
    
    
    /**
     * @Route("/utilisateurs/lister")
     *
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return Response
     */
    public function listUsersAction(EntityManager $em, Request $request)
    {
        $page           = $request->query->getInt('page', 1);
        $userRepository = $em->getRepository('AppBundle:User');
        $userList       = $userRepository->findBy([], ['creationDate' => 'DESC'], User::MAX_PER_ADMIN_PAGE, User::MAX_PER_ADMIN_PAGE*($page-1));
        $nb_users       = $userRepository->countBy();
        $nb_pages       = ceil($nb_users / User::MAX_PER_ADMIN_PAGE);
        
        return $this->render('admin/users/list.html.twig', [
            'user_list'    => $userList,
            'nb_pages'     => $nb_pages,
            'current_page' => $page,
        ]);
    }
    
    
    
    /**
     * @Route("/events/lister")
     * @param EntityManager $em
     * @param Request       $request
     *
     * @return Response
     */
    public function listEventsAction(EntityManager $em, Request $request)
    {
        $page            = $request->query->getInt('page', 1);
        $eventRepository = $em->getRepository('AppBundle:Event');
        $eventList       = $em->getRepository('AppBundle:Event')->findEvents(['startDate' => 'DESC'], Event::MAX_PER_ADMIN_PAGE, $page - 1);
        $nb_events       = $eventRepository->countBy();
        $nb_pages        = ceil($nb_events / User::MAX_PER_ADMIN_PAGE);
        
        return $this->render('admin/events/list.html.twig', [
            'event_list'    => $eventList,
            'nb_pages'      => $nb_pages,
            'current_page'  => $page
        ]);
    }
    
    
    
    /**
     * @Route("/events/ouverture", options={"expose"=true})
     *
     * @param EntityManager $em
     * @param Request       $request
     * @param Calendar      $calendarService
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function openEventAction(EntityManager $em, Request $request, Calendar $calendarService)
    {
        //-- Calendar Filters
        $year   = $request->get('year', date('Y'));
        $month  = str_pad($request->get('month', date('m')), 2, '0', STR_PAD_LEFT);
        $day    = (is_null($request->get('day')) ? date('d') : null);
        $filter = (!is_null($request->get('filter')) ? $request->get('filter') : [Calendar::DAY_FRIDAY, Calendar::DAY_SUNDAY]);
        //--
        
        $calendar     = $calendarService
            ->setDuration('4 months')
            ->setGranularity(Calendar::GRANULARITY_DAY)
            ->filterDays($filter)
            ->setFrom($year, $month, $day)
            ->getDates();
        $open_events  = $em->getRepository('AppBundle:OpenEvent')->findBetween(reset($calendar), end($calendar));
        
        if( $request->isXmlHttpRequest() ) {
            $view = 'admin/events/open_date_list.html.twig';
        } else {
            $view = 'admin/events/open_event.html.twig';
        }
    
        return $this->render($view, [
            'calendar'    => $calendar,
            'day_filters' => array_combine($filter, $filter),
            'today'       => new \DateTime(),
            'open_events' => $open_events,
        ]);
    }
}
