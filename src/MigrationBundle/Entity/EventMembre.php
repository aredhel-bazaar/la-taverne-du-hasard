<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace MigrationBundle\Entity;

use AppBundle\Entity\Subscriber;

class EventMembre
{
    public $event_membre_id;
    public $lk_event_id;
    public $lk_membre_id;
    public $event_participe;
    public $date_creation;
    
    
    
    public function convertToV2(Subscriber &$subscriber, array $membres, array $events)
    {
        $now = new \DateTime();
        
        $subscriber
            ->setFkEvent($events[$this->lk_event_id]['v2'])
            ->setFkUser($membres[$this->lk_membre_id]['v2'])
            ->setPresent($this->event_participe == 2)
            ->setCreationDate(!empty($this->date_creation)     ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->date_creation)     : $now)
            ->setModificationDate($now)
        ;
    }
}