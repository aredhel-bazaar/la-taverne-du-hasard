<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * BaseRepository
 */
abstract class BaseRepository extends EntityRepository
{
    public function countBy(array $criteria = [])
    {
        $name = __CLASS__;
        $queryBuilder = $this->createQueryBuilder($name)
            ->select('COUNT('.$name.'.id)');
        
        foreach ($criteria as $criterion => $value) {
            $queryBuilder->andWhere($name.'.'.$criterion.' = :'.$criterion)
                ->setParameter($criterion, $value);
        }
        
        return $queryBuilder->getQuery()->getSingleScalarResult();
    }
}
