<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class UserSignupForm extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName',     TextType::class, [
                'label' => 'user.label.first_name',
                'label_attr'    => array('class' => 'sr-only'),
                'attr'          => array('placeholder' => 'user.label.first_name')
            ])
            ->add('lastName',     TextType::class, [
                'label' => 'user.label.last_name',
                'label_attr'    => array('class' => 'sr-only'),
                'attr'          => array('placeholder' => 'user.label.last_name')
            ])
            ->add('publicName',     TextType::class, [
                'label' => 'user.label.public_name',
                'label_attr'    => array('class' => 'sr-only'),
                'attr'          => array('placeholder' => 'user.label.public_name')
            ])
            ->add('email',          EmailType::class, [
                'label' => 'user.label.email',
                'label_attr'    => array('class' => 'sr-only'),
                'attr'          => array('placeholder' => 'user.label.email')
            ])
            ->add('plainPassword',  RepeatedType::class, [
                'type'           => PasswordType::class,

                'first_options'  => array(
                    'label' => 'user.label.password',
                    'label_attr'    => array('class' => 'sr-only'),
                    'attr'          => array('placeholder' => 'user.label.password')
                ),
                'second_options' => array(
                    'label' => 'user.label.repeat_password',
                    'label_attr'    => array('class' => 'sr-only'),
                    'attr'          => array('placeholder' => 'user.label.repeat_password')
                )
            ])
        ;
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class'        => User::class,
            'validation_groups' => ['Default', 'Registration']
        ));
    }
}
