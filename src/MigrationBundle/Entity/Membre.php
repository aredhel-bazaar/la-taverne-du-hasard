<?php
/**
 * Created by PhpStorm.
 * User: Silvesius
 * Date: 05/10/2016
 * Time: 22:32
 */

namespace MigrationBundle\Entity;

use AppBundle\Entity\Preference;
use AppBundle\Entity\Role;
use AppBundle\Entity\User;
use Doctrine\Common\Collections\ArrayCollection;

class Membre
{
    public $membre_id;
    public $membre_ip;
    public $membre_pseudo;
    public $membre_url;
    public $membre_password;
    public $membre_email;
    public $membre_avatar;
    public $membre_groupe;
    public $membre_prenom;
    public $membre_nom;
    public $membre_age;
    public $membre_sexe;
    public $membre_localite;
    public $membre_presentation;
    public $membre_theme;
    public $membre_etat;
    public $membre_est_admin;
    public $membre_pref_email_event;
    public $membre_pref_email_private;
    public $membre_pref_email_event_commentaire;
    public $membre_date_creation;
    public $membre_date_modification;
    
    
    
    public function convertToV2(User &$user, array $roles)
    {
        $now = new \DateTime();
        
        $preferences = new ArrayCollection([
            (new Preference())
                ->setFkUser($user)
                ->setName(Preference::PREFERENCE_NEWSLETTER)
                ->setValue(Preference::PREFERENCES_NAME[Preference::PREFERENCE_NEWSLETTER])
                ->setCreationDate($now)
                ->setModificationDate($now),
    
            (new Preference())
                ->setFkUser($user)
                ->setName(Preference::PREFERENCE_ALERT_COMMENT)
                ->setValue(Preference::PREFERENCES_NAME[Preference::PREFERENCE_ALERT_COMMENT])
                ->setCreationDate($now)
                ->setModificationDate($now),
    
            (new Preference())
                ->setFkUser($user)
                ->setName(Preference::PREFERENCE_ALERT_EVENT)
                ->setValue(Preference::PREFERENCES_NAME[Preference::PREFERENCE_ALERT_EVENT])
                ->setCreationDate($now)
                ->setModificationDate($now),
    
            (new Preference())
                ->setFkUser($user)
                ->setName(Preference::PREFERENCE_ALERT_ARTICLE)
                ->setValue(Preference::PREFERENCES_NAME[Preference::PREFERENCE_ALERT_ARTICLE])
                ->setCreationDate($now)
                ->setModificationDate($now),
    
            (new Preference())
                ->setFkUser($user)
                ->setName(Preference::PREFERENCE_ALERT_PRIVATE_MESSAGE)
                ->setValue(Preference::PREFERENCES_NAME[Preference::PREFERENCE_ALERT_PRIVATE_MESSAGE])
                ->setCreationDate($now)
                ->setModificationDate($now)
        ]);
        
        $user
            ->setPublicName(        $this->membre_pseudo)
            ->setNickname(          $this->membre_url)
            ->setPassword(          $this->membre_password)
            ->setEmail(             $this->membre_email)
            ->setAvatar(            !is_null($this->membre_avatar)       ? $this->membre_avatar       : $user->getAvatar())
            ->setFirstName(         !is_null($this->membre_prenom)       ? $this->membre_prenom       : '')
            ->setLastName(          !is_null($this->membre_nom)          ? $this->membre_nom          : '')
            ->setGender(            !is_null($this->membre_sexe)         ? $this->membre_sexe         : '')
            ->setLocality(          !is_null($this->membre_localite)     ? $this->membre_localite     : '')
            ->setPresentation(      !is_null($this->membre_presentation) ? $this->membre_presentation : '')
            ->setTheme(             User::DEFAULT_THEME)
            ->setStatus(            $this->membre_etat + 1)
            ->setSuperAdmin(        $this->membre_est_admin == true)
            ->setCreationDate(      !empty($this->membre_date_creation)     ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->membre_date_creation)     : $now)
            ->setModificationDate(  !empty($this->membre_date_modification) ? \DateTime::createFromFormat('Y-m-d H:i:s', $this->membre_date_modification) : $now)
            ->setPreferences(       $preferences)
            ->addRole(              $roles[Role::ROLE_MEMBER])
        ;
        
        if (!is_null($this->membre_age)) {
            $user->setBirthdate(\DateTime::createFromFormat('Y-m-d', $this->membre_age));
        }
        
        if ($user->isSuperAdmin()) {
            $user->addRole($roles[Role::ROLE_SUPER_ADMIN]);
        }
    }
}