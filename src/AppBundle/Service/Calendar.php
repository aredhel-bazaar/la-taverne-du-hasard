<?php

namespace AppBundle\Service;

/**
 * Class Calendar
 * @package AppBundle\Service
 */
class Calendar
{
    const MINUTE_SECONDS = 60;
    const HOUR_SECONDS   = self::MINUTE_SECONDS * 60;
    const DAY_SECONDS    = self::HOUR_SECONDS * 24;
    const WEEK_SECONDS   = self::DAY_SECONDS * 7;
    
    const DAY_MONDAY    = 1;
    const DAY_TUESDAY   = 2;
    const DAY_WEDNESDAY = 3;
    const DAY_THURSDAY  = 4;
    const DAY_FRIDAY    = 5;
    const DAY_SATURDAY  = 6;
    const DAY_SUNDAY    = 7;
    
    const DURATION_UNIT_HOUR  = ['hour', 'hours'];
    const DURATION_UNIT_DAY   = ['day', 'days'];
    const DURATION_UNIT_WEEK  = ['week', 'weeks'];
    const DURATION_UNIT_MONTH = ['month', 'months'];
    const DURATION_UNIT_YEAR  = ['year', 'years'];
    
    const GRANULARITY_HOUR  = 'hourly';
    const GRANULARITY_DAY   = 'daily';
    const GRANULARITY_WEEK  = 'weekly';
    const GRANULARITY_MONTH = 'monthly';
    const GRANULARITY_YEAR  = 'yearly';
    
    const DATE_FORMAT_MONTH = 'Y/m';
    const DATE_FORMAT_DAY   = 'Y/m/d';
    const DATE_FORMAT_MYSQL = 'Y-m-d H:i:s';
    /**
     * @var string
     */
    private $duration;
    private $duration_amount;
    private $duration_unit;
    /**
     * @var array
     */
    private $day_filter;
    /**
     * @var \DateTime
     */
    private $startDateTime;
    /**
     * @var string
     */
    private $granularity;
    /**
     * @var \DateTime
     */
    private $endDateTime;
    
    
    
    /**
     * Calendar constructor.
     */
    public function __construct()
    {
    }
    
    
    
    /**
     * @param string $duration
     *
     * @return $this
     */
    public function setDuration(string $duration)
    {
        list($amount, $unit) = explode(' ', $duration);
        
        $this->duration_amount = $amount;
        $this->duration_unit   = $unit;
        $this->duration        = $duration;
        
        return $this;
    }
    
    
    
    /**
     * Spécifie quels jours de la semaines doivent être retournés par le calendrier
     *
     * @param int|array $days
     *
     * @return $this
     */
    public function filterDays($days)
    {
        if( !is_array($days) ) {
            $this->day_filter = [$days];
        }
        
        $this->day_filter = $days;
        
        return $this;
    }
    
    
    
    /**
     * Spécifie la date de départ du calendrier
     *
     * @param $year
     * @param $month
     * @param $day
     * @param $hour
     * @param $minute
     * @param $second
     *
     * @return $this
     */
    public function setFrom(int $year=null, int $month=null, int $day=null, int $hour=null, int $minute=null, int $second=null)
    {
        $year   = (is_null($year)   ? date('Y') : $year);
        $month  = (is_null($month)  ? date('m') : $month);
        $day    = (is_null($day)    ? '01'      : $day);
        $hour   = (is_null($hour)   ? '00'      : $hour);
        $minute = (is_null($minute) ? '00'      : $minute);
        $second = (is_null($second) ? '00'      : $second);
        
        $dateTime = new \DateTime();
        $dateTime->setDate($year, $month, $day);
        $dateTime->setTime($hour, $minute, $second);
        
        $this->startDateTime = $dateTime;
        
        return $this;
    }
    
    
    
    /**
     * @param $granularity
     *
     * @return $this
     * @throws \Exception
     */
    public function setGranularity($granularity)
    {
        switch( $granularity ) {
            case self::GRANULARITY_HOUR:
                $this->granularity = '1 '.self::DURATION_UNIT_HOUR[0];
                break;
            
            case self::GRANULARITY_DAY:
                $this->granularity = '1 '.self::DURATION_UNIT_DAY[0];
                break;
            
            case self::GRANULARITY_WEEK:
                $this->granularity = '1 '.self::DURATION_UNIT_WEEK[0];
                break;
            
            case self::GRANULARITY_MONTH:
                $this->granularity = '1 '.self::DURATION_UNIT_MONTH[0];
                break;
            
            case self::GRANULARITY_YEAR:
                $this->granularity = '1 '.self::DURATION_UNIT_YEAR[0];
                break;
            
            default:
                throw new \Exception("Granularity [$granularity] not supported.");
        }
        
        return $this;
    }
    
    
    
    public function getDates()
    {
        if( is_null($this->duration) ) {
            throw new \Exception("No duration set.");
        }
        
        if( is_null($this->granularity) ) {
            throw new \Exception("No granularity set.");
        }
    
        $dateTimeCursor    = clone $this->startDateTime;
        $this->endDateTime = (clone $this->startDateTime)->modify('+'.$this->duration);
        $list              = [];
    
        //-- On construit le tableau de date pour le mois ciblé
        while( $dateTimeCursor < $this->endDateTime ) {
            if( is_null($this->day_filter) || in_array($dateTimeCursor->format('N'), $this->day_filter) ) {
                $list[] = clone $dateTimeCursor;
            }
            
            $dateTimeCursor->modify('+'.$this->granularity);
        }
        //--
        
        return $list;
    }
    
    
    
    public function getCalendar()
    {
        $this->setGranularity(Calendar::GRANULARITY_DAY);
        
        $list = $this->getDates();
        
        //-- On regarde si le début du mois est bien un Lundi. Sinon on complète le tableau pour avoir un lundi
        $first_month_day       = reset($list)->format('N');
        if( $first_month_day > self::DAY_MONDAY ) {
            $currentDateTime = $this->startDateTime;
            for( $i=$first_month_day; $i>self::DAY_MONDAY; $i-- ) {
                $currentDateTime->modify('-'.$this->granularity);
                
                array_unshift($list, clone $currentDateTime);
            }
        }
        //--
    
        //-- On regarde si la fin du mois est bien un Dimanche. Sinon on complète le tableau pour avoir un dimanche à la fin
        $last_month_day       = end($list)->format('N');
        if( $last_month_day < self::DAY_SUNDAY ) {
            $currentDateTime = $this->endDateTime;
            for( $i=$last_month_day; $i<self::DAY_SUNDAY; $i++ ) {
                array_push($list, clone $currentDateTime);
    
                $currentDateTime->modify('+'.$this->granularity);
            }
        }
        //--
        
        return $list;
    }
    
    
    
    public function getNextMonth($year, $month)
    {
        $month_start = strtotime("01-".$month."-".$year);
        $dateTime    = new \DateTime();
        $dateTime->setTimestamp($month_start);
        $dateTime->modify('+1 month');
        
        return $dateTime;
    }
    
    
    
    public function getPrevMonth($year, $month)
    {
        $month_start = strtotime("01-".$month."-".$year);
        $dateTime    = new \DateTime();
        $dateTime->setTimestamp($month_start);
        $dateTime->modify('-1 month');
        
        return $dateTime;
    }
}